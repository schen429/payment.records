﻿using AutoFixture;
using AutoFixture.AutoMoq;
using iHerb.Payment.Records.Metrics;
using Xunit;

namespace iHerb.Payment.Records.UnitTests.Metrics
{
    public class RecordsMetricsCollectorTests
    {
        private readonly IFixture _fixture;

        public RecordsMetricsCollectorTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
        }

        [Fact]
        public void ObserveServiceError_Success()
        {
            var collector = _fixture.Create<RecordsMetricsCollector>();

            collector.ObserveServiceError(
                RecordsMetricsCollector.Service_Records,
                RecordsMetricsCollector.Reason_DBFailure);

            Assert.True(true);
        }
    }
}
