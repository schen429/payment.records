﻿using AutoFixture;
using AutoFixture.AutoMoq;
using iHerb.Payment.Records.Metrics;
using iHerb.Payment.Records.UnitTests.TestHelpers;
using Moq;
using Prometheus;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Xunit;

namespace iHerb.Payment.Records.UnitTests.Metrics
{
    public class SqlMetricsCollectorTests
    {
        private readonly IFixture _fixture;
        private readonly Mock<IRecordsMetricsCollector> _dataMartMetricsCollector;
        private readonly Counter _timeoutCounter;

        public SqlMetricsCollectorTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _dataMartMetricsCollector = _fixture.Freeze<Mock<IRecordsMetricsCollector>>();
            _timeoutCounter = CreateTimeoutCounter();
        }

        [Fact]
        public void ObserveDuration_Success()
        {
            var collector = _fixture.Create<SqlMetricsCollector>();

            collector.ObserveDuration("datamart", "Payment", "spx_abc", SqlMetricsCollector.Status_Success, 0.01);

            Assert.True(true);
        }

        [Fact]
        public void ObserveTimeout_Success()
        {
            var collector = _fixture.Create<SqlMetricsCollector>();

            collector.ObserveTimeout("datamart", "Payment", "spx_abc", SqlMetricsCollector.Status_Failure);

            Assert.True(true);
        }

        [Fact]
        public async Task ObserveExecuteAsync_Success()
        {
            var collector = _fixture.Create<SqlMetricsCollector>();

            await collector.ObserveExecuteAsync("datamart", "Payment", "spx_abc", () => Task.CompletedTask);

            Assert.True(true);
        }

        [Fact]
        public async Task ObserveExecuteAsync_Error()
        {
            var collector = _fixture.Create<SqlMetricsCollector>();

            await Assert.ThrowsAsync<Exception>(async () =>
                {
                    await collector.ObserveExecuteAsync("datamart", "Payment", "spx_abc", async () => { throw new Exception(); });
                }
            );

            _dataMartMetricsCollector.Verify(m => m.ObserveServiceError(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task ObserveExecuteAsync_ExceptionIsSqlTimeoutException_ObservesTimeout()
        {
            const string database = "datamart";
            const string schema = "Payment";
            const string command = "spx_abc";
            const string status = "failure";
            var collector = _fixture.Create<SqlMetricsCollector>();

            await Assert.ThrowsAsync<SqlException>(() =>
                collector.ObserveExecuteAsync(
                    database,
                    schema,
                    command,
                    () => throw PaymentTestUtilities.CreateSqlException(-2)));
            Assert.Equal(1, _timeoutCounter.WithLabels(database, schema, command, status).Value);
        }

        [Fact]
        public async Task ObserveExecuteAsyncT_Success()
        {
            var collector = _fixture.Create<SqlMetricsCollector>();

            await collector.ObserveExecuteAsync<int>("datamart", "Payment", "spx_abc", async () => 100);

            Assert.True(true);
        }

        [Fact]
        public async Task ObserveExecuteAsyncT_Error()
        {
            var collector = _fixture.Create<SqlMetricsCollector>();

            await Assert.ThrowsAsync<Exception>(async () =>
                {
                    await collector.ObserveExecuteAsync<int>("datamart", "Payment", "spx_abc", async () => { throw new Exception(); });
                }
            );

            _dataMartMetricsCollector.Verify(m => m.ObserveServiceError(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task ObserveExecuteAsyncT_ExceptionIsSqlTimeoutException_ObservesTimeout()
        {
            const string database = "datamart";
            const string schema = "Payment";
            const string command = "spx_abc";
            const string status = "failure";
            var collector = _fixture.Create<SqlMetricsCollector>();

            await Assert.ThrowsAsync<SqlException>(() =>
                collector.ObserveExecuteAsync<int>(
                    database,
                    schema,
                    command,
                    () => throw PaymentTestUtilities.CreateSqlException(-2)));
            Assert.Equal(1, _timeoutCounter.WithLabels(database, schema, command, status).Value);
        }

        #region Helpers

        private static Counter CreateTimeoutCounter()
        {
            var counter = Prometheus.Metrics.CreateCounter(
                "payment_api_sql_request_timeout_count",
                "SQL request timeout count for datamart",
                new CounterConfiguration
                {
                    LabelNames = new []
                    {
                        "database",
                        "schema",
                        "command",
                        "status"
                    }
                });

            foreach (var labelValues in counter.GetAllLabelValues())
            {
                counter.RemoveLabelled(labelValues);
            }

            return counter;
        }

        #endregion
    }
}
