﻿using AutoFixture;
using AutoFixture.AutoMoq;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models.PaymentLogs;
using iHerb.Payment.Records.Repositories;
using iHerb.Payment.Records.Utils;
using Moq;
using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using Xunit;

namespace iHerb.Payment.Records.UnitTests.Utils
{
    public class StringExtensionsTests
    {
        private readonly IFixture _fixture;

        public StringExtensionsTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
        }

        [Theory]
        [InlineData("123", 123)]
        [InlineData("abc123", null)]
        [InlineData("", null)]
        [InlineData("123.9", null)]
        [InlineData("123abc", null)]
        public void ToInt_Success(string s, int? expectedResult)
        {
            var result = StringUtil.ToInt(s);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData("123", 0, 123)]
        [InlineData("abc123", 0, 0)]
        [InlineData("", -1, -1)]
        [InlineData("123.9", 0, 0)]
        [InlineData("123abc", 0, 0)]
        public void ToIntOrDefault_Success(string s, int defaultValue, int expectedResult)
        {
            var result = StringUtil.ToIntOrDefault(s, defaultValue);
            Assert.Equal(expectedResult, result);
        }
    }
}
