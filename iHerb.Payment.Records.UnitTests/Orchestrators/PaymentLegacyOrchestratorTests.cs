﻿using AutoFixture.Xunit2;
using FluentAssertions;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Factories;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Models.Consumed.Authorization;
using iHerb.Payment.Records.Models.Messages;
using iHerb.Payment.Records.Models.PaymentLogs;
using iHerb.Payment.Records.Orchestrators;
using iHerb.Payment.Records.Repositories;
using LazyCache;
using LazyCache.Testing.Moq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace iHerb.Payment.Records.UnitTests.Orchestrators
{
    public class PaymentLegacyOrchestratorTests
    {
        private const string CacheEntryKey = "LocalCurrencyPaymentTypes_0_False";
        private readonly IAppCache _appCache = Create.MockedCachingService();
        private readonly Mock<IConfiguration> _configuration = new();

        private readonly Mock<ILogger<QueueMessageProcessingOrchestrator>> _logger =
            new();

        private readonly Mock<IPaymentLogsFactory> _paymentLogsFactory = new();
        private readonly Mock<IPaymentLegacyRepository> _paymentRepository = new();
        private readonly Mock<IShopRepository> _shopRepository = new();


        private readonly PaymentLegacyOrchestrator _sut;

        public PaymentLegacyOrchestratorTests()
        {
            var configSection = new Mock<IConfigurationSection>();
            configSection.Setup(a => a.Value).Returns("100");
            _configuration.Setup(a => a.GetSection("CacheSettings:CacheExpirationTimeHours"))
                .Returns(configSection.Object);

            var services = new ServiceCollection()
                .AddLogging()
                .AddLazyCache()
                .AddTransient(_ => _logger.Object)
                .AddTransient(_ => _paymentLogsFactory.Object)
                .AddTransient(_ => _paymentRepository.Object)
                .AddTransient(_ => _shopRepository.Object)
                .AddTransient(_ => _appCache)
                .AddTransient(_ => _configuration.Object)
                .BuildServiceProvider();

            _sut = ActivatorUtilities.CreateInstance<PaymentLegacyOrchestrator>(services);
        }


        [Theory]
        [AutoData]
        public async Task ProcessCaptureMessage_Success(TransactionMessage message)
        {
            message.ReferenceId = "1234567890";
            message.TransactionType = TransactionType.AUTHORISATION;
            message.TransactionStatus = TransactionStatus.CAPTURED;
            await _sut.ProcessCaptureMessage(message);

            _paymentRepository.Verify(m => m.InsertPaymentLog(It.IsAny<PaymentLog>()), Times.Once);
            _paymentRepository.Verify(
                m => m.InsertOrderPayment(message.Amount, It.IsAny<string>(), It.IsAny<int>(), false, false),
                Times.Once);
        }

        [Theory]
        [AutoData]
        public async Task ProcessRefundMessage_Success(TransactionMessage message)
        {
            message.ReferenceId = "1234567890";
            message.TransactionType = TransactionType.REFUND;
            message.TransactionStatus = TransactionStatus.REFUNDED;
            await _sut.ProcessRefundMessage(message);

            _paymentRepository.Verify(m => m.InsertPaymentLog(It.IsAny<PaymentLog>()), Times.Once);
            _paymentRepository.Verify(
                m => m.InsertOrderPayment(message.Amount, It.IsAny<string>(), It.IsAny<int>(), true, false),
                Times.Once);
        }
        
        [Theory]
        [AutoData]
        public async Task ProcessChargebackMessage_Success(TransactionMessage message)
        {
            message.ReferenceId = "1234567890";
            message.TransactionType = TransactionType.CHARGEBACK;
            message.TransactionStatus = TransactionStatus.REFUNDED;
            await _sut.ProcessChargebackMessage(message);

            _paymentRepository.Verify(m => m.InsertPaymentLog(It.IsAny<PaymentLog>()), Times.Once);
            _paymentRepository.Verify(
                m => m.InsertOrderPayment(message.Amount, It.IsAny<string>(), It.IsAny<int>(), true, false),
                Times.Once);
        }

        [Theory]
        [AutoData]
        public async Task ProcessAuthorisationMessage_Success(TransactionMessage message)
        {
            message.ReferenceId = "1234567890";
            await _sut.ProcessAuthorisationMessage(message);

            _paymentRepository.Verify(m => m.InsertPaymentLog(It.IsAny<PaymentLog>()), Times.Once);
        }

        [Theory]
        [AutoData]
        public async Task ProcessVoidMessage_Success(TransactionMessage message)
        {
            message.ReferenceId = "1234567890";
            message.TransactionType = TransactionType.VOID;
            message.TransactionStatus = TransactionStatus.VOIDED;
            await _sut.ProcessVoidMessage(message);

            _paymentRepository.Verify(m => m.InsertPaymentLog(It.IsAny<PaymentLog>()), Times.Once);
            _paymentRepository.Verify(
                m => m.InsertOrderPayment(message.Amount, It.IsAny<string>(), It.IsAny<int>(), false, true),
                Times.Once);
        }

        [Fact]
        public async Task ProcessPaymentMessage_Success()
        {
            var message = new PaymentMessage<AuthorizationExternalRequest>
            {
                Source = MessageSource.TRANSACTION,
                Subject = MessageSubject.AUTHORISED,
                Body = new AuthorizationExternalRequest
                {
                    TransactionId = Guid.NewGuid(),
                    PaymentType = PaymentType.CreditCard,
                    PaymentSubType = PaymentSubType.VISA,
                    ReferenceId = "1234568990",
                    UserId = Guid.NewGuid(),
                    Amount = 101M,
                    Currency = "USD",
                    RawResponse = new AuthorizationExternalResponse
                    {
                        additionalData = new AdyenAdditionalData()
                    }
                }
            };
            var json = JsonConvert.SerializeObject(message);
            var paymentMessage = new PaymentMessage
            {
                Source = MessageSource.TRANSACTION,
                Subject = MessageSubject.AUTHORISED
            };
            _paymentLogsFactory.Setup(x =>
                    x.GetPaymentLog(paymentMessage.Source, paymentMessage.Subject, It.IsAny<string>()))
                .Returns(new PaymentLog());
            await _sut.ProcessPaymentMessage(paymentMessage, json);

            _paymentRepository.Verify(m => m.InsertPaymentLog(It.IsAny<PaymentLog>()), Times.Once);
        }

        [Fact]
        public async Task ProcessPaymentMessage_Failure()
        {
            var message = new PaymentMessage<AuthorizationExternalRequest>
            {
                Source = MessageSource.TRANSACTION,
                Subject = MessageSubject.AUTHORISED,
                Body = new AuthorizationExternalRequest
                {
                    TransactionId = Guid.NewGuid(),
                    PaymentType = PaymentType.CreditCard,
                    PaymentSubType = PaymentSubType.VISA,
                    ReferenceId = "1234568990",
                    UserId = Guid.NewGuid(),
                    Amount = 101M,
                    Currency = "USD",
                    RawResponse = new AuthorizationExternalResponse
                    {
                        additionalData = new AdyenAdditionalData()
                    }
                }
            };
            var json = JsonConvert.SerializeObject(message);
            var paymentMessage = new PaymentMessage
            {
                Source = MessageSource.TRANSACTION,
                Subject = MessageSubject.UNKNOWN
            };

            /*_fixture.Register<IPaymentLogsFactory>(() =>
            {
                var paymentLogWrapper = _fixture.Create<PaymentLogMapper>();
                return new PaymentLogsFactory(new Mock<ILogger<PaymentLogsFactory>>().Object, paymentLogWrapper);
            });*/

            await _sut.ProcessPaymentMessage(paymentMessage, json);

            _paymentRepository.Verify(m => m.InsertPaymentLog(It.IsAny<PaymentLog>()), Times.Never);
        }

        [Theory]
        [InlineAutoData("{\"additionalData\": {\"alias\":\"213554\", \"expiryDate\": \"3/2030\"}}", "213554", 3, 2030)]
        [InlineAutoData("{\"additionalData\": {\"alias\": null, \"expiryDate\": null}}", "", 0, 0)]
        [InlineAutoData(null, null, null, null)]
        public async Task GetTransactionLogs_ReturnPaymentLogWithCorrectTxType(string additionalInfo, string expectedAlias, int? expectedMonth, int? expectedYear)
        {
            _paymentRepository.Setup(c => c.GetTransactionLogs(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new List<RawPaymentLog>
                {
                    new()
                    {
                        TxType = 'N',
                        AdditionalInfo = additionalInfo
                    }
                });
            var logs = await _sut.GetTransactionLogs(23, null);
            logs.PaymentLogs.Count.Should().Be(1);
            logs.PaymentLogs[0].TxType.Should().HaveSameValueAs(PaymentTxTypePayment.Notification);
            logs.PaymentLogs[0].ExpirationMonth.Should().Be(expectedMonth);
            logs.PaymentLogs[0].ExpirationYear.Should().Be(expectedYear);
            logs.PaymentLogs[0].CreditCardAliasId.Should().Be(expectedAlias);
        }

        [Fact]
        public async Task GetTransactionLogs_NoRecordReturned_PaymentLogsNotNull()
        {
            _paymentRepository.Setup(c => c.GetTransactionLogs(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new List<RawPaymentLog>());

            var logs = await _sut.GetTransactionLogs(23, null);
            logs.PaymentLogs.Count.Should().Be(0);
        }


        [Fact]
        public async Task GetTransactionLogs_RepositoryThrowException_ThrowException()
        {
            _paymentRepository.Setup(c => c.GetTransactionLogs(It.IsAny<int>(), It.IsAny<string>()))
                .ThrowsAsync(new Exception());

            await Assert.ThrowsAnyAsync<Exception>(() => _sut.GetTransactionLogs(23, null));
        }

        [Theory]
        [AutoData]
        public async Task GetPaymentTransactionInfo_RepositoryThrowException_ReturnNull(byte deviceStore,
            bool includeDisabled, List<RawPaymentLog> paymentLogs,
            List<LocalCurrencyPaymentType> localCurrencyPaymentTypes)
        {
            _appCache.Add(CacheEntryKey, localCurrencyPaymentTypes);
            _paymentRepository.Setup(c => c.GetTransactionLogs(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(paymentLogs);
            _shopRepository.Setup(x => x.GetLocalCurrencyPaymentTypesAsync(deviceStore, includeDisabled))
                .ThrowsAsync(new Exception());

            var logs = await _sut.GetPaymentTransactionInfo(23, null);
            logs.Should().BeNull();
        }

        [Theory]
        [AutoData]
        public async Task GetPaymentTransactionInfo_NoRecord_ReturnNull(byte deviceStore, bool includeDisabled,
            List<RawPaymentLog> paymentLogs, List<LocalCurrencyPaymentType> localCurrencyPaymentTypes)
        {
            _appCache.Add(CacheEntryKey, localCurrencyPaymentTypes);
            _paymentRepository.Setup(c => c.GetTransactionLogs(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(paymentLogs);
            _shopRepository.Setup(c => c.GetLocalCurrencyPaymentTypesAsync(deviceStore, includeDisabled))
                .ReturnsAsync(localCurrencyPaymentTypes);


            var logs = await _sut.GetPaymentTransactionInfo(23, null);
            logs.Should().BeNull();
        }

        [Theory]
        [AutoData]
        public async Task GetPaymentTransactionInfo_RequireCapture_Success(byte deviceStore, bool includeDisabled,
            List<RawPaymentLog> paymentLogs, List<LocalCurrencyPaymentType> localCurrencyPaymentTypes)
        {
            paymentLogs.First().AccountType = "MC";
            paymentLogs.First().CurrencyCode = "USD";
            paymentLogs.First().TxType = (char) PaymentTxTypePayment.DelayedCapture;
            paymentLogs.First().Approved = true;

            localCurrencyPaymentTypes.First().CreditCardType = "MC";
            localCurrencyPaymentTypes.First().ExternalPaymentType = ExternalPaymentType.None;
            _appCache.Add(CacheEntryKey, localCurrencyPaymentTypes);
            _paymentRepository.Setup(c => c.GetTransactionLogs(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(paymentLogs);

            _shopRepository.Setup(c => c.GetLocalCurrencyPaymentTypesAsync(deviceStore, includeDisabled))
                .ReturnsAsync(localCurrencyPaymentTypes);


            var logs = await _sut.GetPaymentTransactionInfo(23, null);
            logs.TransactionDate.Should().Be(paymentLogs.First().TxDate);
        }

        [Theory]
        [AutoData]
        public async Task GetPaymentTransactionInfo_DoesNotRequireCapture_Success(List<RawPaymentLog> paymentLogs,
            List<LocalCurrencyPaymentType> localCurrencyPaymentTypes)
        {
            paymentLogs.First().AccountType = "WECHAT";
            paymentLogs.First().CurrencyCode = "CNY";
            paymentLogs.First().TxType = (char) PaymentTxTypePayment.AuthorizationRequest;
            paymentLogs.First().Approved = true;

            localCurrencyPaymentTypes.First().CreditCardType = "WECHAT";
            localCurrencyPaymentTypes.First().ExternalPaymentType = ExternalPaymentType.WeChat;
            localCurrencyPaymentTypes.First().CurrencyCode = "CNY";

            _appCache.Add(CacheEntryKey, localCurrencyPaymentTypes);
            _paymentRepository.Setup(c => c.GetTransactionLogs(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(paymentLogs);

            _shopRepository.Setup(c => c.GetLocalCurrencyPaymentTypesAsync(0, false))
                .ReturnsAsync(localCurrencyPaymentTypes);

            var logs = await _sut.GetPaymentTransactionInfo(23, null);
            logs.TransactionDate.Should().Be(paymentLogs.First().TxDate);
        }

        [Fact]
        public async Task GetTransactionsByUser_Success()
        {
            var userId = Guid.NewGuid();

            _paymentRepository.Setup(m => m.GetTransactionsByUser(It.IsAny<Guid>(), It.IsAny<PaymentType?>(),
                    It.IsAny<string>(), It.IsAny<DateTime?>(), It.IsAny<DateTime?>()))
                .ReturnsAsync(new List<RawPaymentLog>
                {
                    new RawPaymentLog
                    {
                        CustomerId = userId,
                        OrderNumber = 123456,
                        OrderExt = 0,
                        TxType = (char) PaymentTxTypePayment.Authorization
                    }
                });

            var result = await _sut.GetTransactionsByUser(userId);

            Assert.Single(result.PaymentLogs,
                paymentLog =>
                    paymentLog.CustomerId == userId && paymentLog.TxType == PaymentTxTypePayment.Authorization);
        }

        [Theory]
        [AutoData]
        public async Task ProcessAuthorisationMessage_NonCreditCard_InsertUpsert(TransactionMessage message)
        {
            message.TransactionId = Guid.NewGuid();
            message.PaymentType = PaymentType.Adyen_ApplePay;
            message.ReferenceId = "1234567890";
            message.BillingAddress = new BillingAddress();
            message.TransactionStatus = TransactionStatus.AUTHORISED;
            message.ActionType = ActionType.NONE;

            await _sut.ProcessAuthorisationMessage(message);

            _paymentRepository.Verify(m => m.InsertPaymentLog(It.IsAny<PaymentLog>()), Times.Once);
            _paymentRepository.Verify(m => m.UpsertPaymentProperties(It.IsAny<PaymentProperties>()), Times.Once);
        }

        [Theory]
        [AutoData]
        public async Task ProcessAuthorisationMessage_CreditCard_Upsert(TransactionMessage message)
        {
            message.TransactionId = Guid.NewGuid();
            message.PaymentType = PaymentType.CreditCard;
            message.ReferenceId = "1234567890";
            message.BillingAddress = new BillingAddress();
            message.TransactionStatus = TransactionStatus.AUTHORISED;
            message.ActionType = ActionType.NONE;
            message.PaymentAccountId = "1234";

            await _sut.ProcessAuthorisationMessage(message);

            _paymentRepository.Verify(m => m.InsertPaymentLog(It.IsAny<PaymentLog>()), Times.Never);
            _paymentRepository.Verify(m => m.UpsertPaymentProperties(It.IsAny<PaymentProperties>()), Times.Once);
        }

        [Theory]
        [AutoData]
        public async Task ProcessAuthorisationMessage_Captured_NoInsertUpsert(TransactionMessage message)
        {
            message.TransactionId = Guid.NewGuid();
            message.PaymentType = PaymentType.Adyen_ApplePay;
            message.ReferenceId = "1234567890";
            message.BillingAddress = new BillingAddress();
            message.TransactionStatus = TransactionStatus.CAPTURED;
            message.ActionType = ActionType.NONE;
            message.PaymentAccountId = "1234";

            await _sut.ProcessAuthorisationMessage(message);

            _paymentRepository.Verify(m => m.InsertPaymentLog(It.IsAny<PaymentLog>()), Times.Never);
            _paymentRepository.Verify(m => m.UpsertPaymentProperties(It.IsAny<PaymentProperties>()), Times.Never);
        }

        [Theory]
        [InlineAutoData("{\"Target\": {\"Id\":\"213554\", \"ShippingAddress\": {\"FirstName\": \"iHerb\", \"LastName\": \"Tester\"}}}")]
        [InlineAutoData("{\"resultCode\":\"Authorised\", \"additionalData\": {\"cardHolderName\": \"iHerb Tester\"}}")]
        public async Task GetBillingAddress_success(string additionalInfo, int orderNumber, BillingAddress billingAddress,
            List<PaymentProperties> orderAuthorizationProperty)
        {
            orderAuthorizationProperty.ForEach(x => x.BillingAddress = JsonConvert.SerializeObject(billingAddress));
            orderAuthorizationProperty.ForEach(x => x.AdditionalInfo = additionalInfo);
            _paymentRepository.Setup(x => x.GetBillingAddressByOrderNumber(orderNumber))
                .ReturnsAsync(orderAuthorizationProperty);

            var result = await _sut.GetBillingAddress(orderNumber);

            result.Should().BeOfType<BillingAddressResponse>();
            result.BillingAddresses.FirstOrDefault()?.BillingAddress.City.Should().Be(billingAddress.City);
            result.BillingAddresses.FirstOrDefault()?.BillingAddress.AccountHolderName.Should().Be("iHerb Tester");
        }

        [Theory, AutoData]
        public async Task GetBillingAddress_RepositoryTrowException(int orderNumber)
        {
            _paymentRepository.Setup(x => x.GetBillingAddressByOrderNumber(orderNumber))
                .ThrowsAsync(new Exception());
            
            var result = await Record.ExceptionAsync( () => _sut.GetBillingAddress(orderNumber));

            result.Should().NotBeNull()
                .And.Subject.Should().BeOfType<Exception>();
            
        }
        
        [Theory, AutoData]
        public async Task GetBillingAddress_RepositoryReturnRows_Success(int orderNumber)
        {
            _paymentRepository.Setup(x => x.GetBillingAddressByOrderNumber(orderNumber))
                .ReturnsAsync(new List<PaymentProperties>
                {
                    new PaymentProperties
                    {
                        TransactionId = Guid.NewGuid(),
                        CustomerId = Guid.NewGuid(),
                        OrderNumber = 1,
                        TransactionDate = DateTime.Now,
                        TransactionStatus = TransactionStatus.AUTHORISED.ToString(),
                        BillingAddress = JsonConvert.SerializeObject(new BillingAddress
                        {
                            Country = "USA",
                            Street = "street",
                            City = "city",
                            HouseNumberOrName = "1234",
                            StateOrProvince = "CA",
                            PostalCode = "12345"
                        })
                    }
                });
            
            var result = await  _sut.GetBillingAddress(orderNumber);

            Assert.Single(result.BillingAddresses);
        }

        [Theory, AutoData]
        public async Task GetBillingAddress_RepositoryReturnNull(int orderNumber)
        {
            _paymentRepository.Setup(x => x.GetBillingAddressByOrderNumber(orderNumber))
                .ReturnsAsync((List<PaymentProperties>)null);

            var result = await _sut.GetBillingAddress(orderNumber);

            result.Should().BeNull();
        }

        [Theory, AutoData]
        public async Task GetBillingAddress_RepositoryReturnNullBillingAddress(int orderNumber,
            List<PaymentProperties> orderAuthorizationProperty)
        {
            orderAuthorizationProperty.ForEach(x => x.BillingAddress = null);

            _paymentRepository.Setup(x => x.GetBillingAddressByOrderNumber(orderNumber))
                .ReturnsAsync(orderAuthorizationProperty);

            var result = await _sut.GetBillingAddress(orderNumber);

            result.Should().BeNull();
        }
        
        [Theory, AutoData]
        public async Task GetOrderTotalPaid_RepositoryThrowException(int orderNumber,
           byte? ext)
        {
            _paymentRepository.Setup(x => x.GetOrderTotalPaid(orderNumber, ext))
                .ThrowsAsync(new Exception());

            var result = await Record.ExceptionAsync( () => _sut.GetOrderTotalPaid(orderNumber, ext));

            result.Should().NotBeNull()
                .And.Subject.Should().BeOfType<Exception>();
        }
        
        [Theory, AutoData]
        public async Task GetOrderTotalPaid_Success(int orderNumber,
            byte? ext, decimal amount)
        {
            _paymentRepository.Setup(x => x.GetOrderTotalPaid(orderNumber, ext))
                .ReturnsAsync(amount);

            var result = await _sut.GetOrderTotalPaid(orderNumber, ext);

            result.AmountTotalPaid.Should().Be(amount);
        }
    }
}