using AutoFixture;
using AutoFixture.AutoMoq;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Models.Fraud;
using iHerb.Payment.Records.Models.PaymentLogs;
using iHerb.Payment.Records.Orchestrators;
using iHerb.Payment.Records.Repositories;
using Microsoft.Extensions.Configuration;
using Moq;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace iHerb.Payment.Records.UnitTests.Orchestrators
{
    public class FraudOrchestratorTests
    {
        private readonly IFixture _fixture;
        private readonly Mock<IPaymentLegacyRepository> _paymentLegacyRepository;
        private readonly Mock<IConfiguration> _configuration;

        public FraudOrchestratorTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _paymentLegacyRepository = Mock.Get(_fixture.Freeze<IPaymentLegacyRepository>());
            _configuration = Mock.Get(_fixture.Freeze<IConfiguration>());
        }

        [Fact]
        public async Task GetTransactionLogsAsync_SkipIsGreaterThan0_SkipsEarlierLogsFromDb()
        {
            var request = new GetTransactionLogsRequest
            {
                OrderNumber = 1,
                Skip = 1,
                Top = 1
            };
            var paymentLogs = new[]
            {
                new RawPaymentLog { OrderNumber = 1 },
                new RawPaymentLog { OrderNumber = 2 }
            };
            _paymentLegacyRepository.Setup(r => r.GetTransactionLogs(It.IsAny<int?>(), It.IsAny<string>()))
                .ReturnsAsync(paymentLogs);
            var orchestrator = _fixture.Create<FraudOrchestrator>();

            var result = await orchestrator.GetTransactionLogsAsync(request);

            var paymentLog = Assert.Single(result.PaymentLogs);
            Assert.Equal(paymentLog.OrderNumber, paymentLogs[1].OrderNumber);
        }

        [Fact]
        public async Task GetTransactionLogsAsync_TopIsGreaterThan0_LimitsLogsReturnedFromDb()
        {
            var request = new GetTransactionLogsRequest
            {
                OrderNumber = 1,
                Skip = 0,
                Top = 1
            };
            var paymentLogs = new[]
            {
                new RawPaymentLog { OrderNumber = 1 },
                new RawPaymentLog { OrderNumber = 2 }
            };
            _paymentLegacyRepository.Setup(r => r.GetTransactionLogs(It.IsAny<int?>(), It.IsAny<string>()))
                .ReturnsAsync(paymentLogs);
            var orchestrator = _fixture.Create<FraudOrchestrator>();

            var result = await orchestrator.GetTransactionLogsAsync(request);

            var paymentLog = Assert.Single(result.PaymentLogs);
            Assert.Equal(paymentLog.OrderNumber, paymentLogs[0].OrderNumber);
        }

        [Fact]
        public async Task GetTransactionLogsAsync_TopIsNull_SetsTopAccordingToDefaultPageSizeConfig()
        {
            var request = new GetTransactionLogsRequest
            {
                OrderNumber = 1,
                Skip = 0,
                Top = null
            };
            var paymentLogs = new[]
            {
                new RawPaymentLog { OrderNumber = 1 },
                new RawPaymentLog { OrderNumber = 2 }
            };
            var section = new Mock<IConfigurationSection>();
            _paymentLegacyRepository.Setup(r => r.GetTransactionLogs(It.IsAny<int?>(), It.IsAny<string>()))
                .ReturnsAsync(paymentLogs);
            section.Setup(s => s.Value).Returns("1");
            _configuration.Setup(c => c.GetSection("ApiSettings:DefaultPageSize")).Returns(section.Object);
            var orchestrator = _fixture.Create<FraudOrchestrator>();

            var result = await orchestrator.GetTransactionLogsAsync(request);

            var paymentLog = Assert.Single(result.PaymentLogs);
            Assert.Equal(paymentLog.OrderNumber, paymentLogs[0].OrderNumber);
        }

        [Fact]
        public async Task GetTransactionLogsAsync_TxTypeIsNotAuthorization_SetsFraudResultToNull()
        {
            var request = new GetTransactionLogsRequest
            {
                OrderNumber = 1,
                Skip = 0,
                Top = 1
            };
            var paymentLogs = new[]
            {
                new RawPaymentLog
                {
                    OrderNumber = 1,
                    TxType = (char) PaymentTxTypePayment.Void
                }
            };
            _paymentLegacyRepository.Setup(r => r.GetTransactionLogs(It.IsAny<int?>(), It.IsAny<string>()))
                .ReturnsAsync(paymentLogs);
            var orchestrator = _fixture.Create<FraudOrchestrator>();

            var result = await orchestrator.GetTransactionLogsAsync(request);

            var paymentLog = Assert.Single(result.PaymentLogs);
            Assert.Null(paymentLog.FraudResult);
        }

        [Fact]
        public async Task GetTransactionLogsAsync_AdditionalInfoIsNotJson_SetsFraudResultToNull()
        {
            var request = new GetTransactionLogsRequest
            {
                OrderNumber = 1,
                Skip = 0,
                Top = 1
            };
            var paymentLogs = new[]
            {
                new RawPaymentLog
                {
                    AdditionalInfo = "not JSON",
                    OrderNumber = 1,
                    TxType = (char) PaymentTxTypePayment.Authorization
                }
            };
            _paymentLegacyRepository.Setup(r => r.GetTransactionLogs(It.IsAny<int?>(), It.IsAny<string>()))
                .ReturnsAsync(paymentLogs);
            var orchestrator = _fixture.Create<FraudOrchestrator>();

            var result = await orchestrator.GetTransactionLogsAsync(request);

            var paymentLog = Assert.Single(result.PaymentLogs);
            Assert.Null(paymentLog.FraudResult);
        }

        [Theory]
        [InlineData("null")]
        [InlineData("0")]
        [InlineData("[]")]
        [InlineData("false")]
        [InlineData("\"valid JSON but not an object\"")]
        public async Task GetTransactionLogsAsync_AdditionalInfoIsNotAJsonObject_SetsFraudResultToNull(
            string additionalInfo)
        {
            var request = new GetTransactionLogsRequest
            {
                OrderNumber = 1,
                Skip = 0,
                Top = 1
            };
            var paymentLogs = new[]
            {
                new RawPaymentLog
                {
                    AdditionalInfo = additionalInfo,
                    OrderNumber = 1,
                    TxType = (char) PaymentTxTypePayment.Authorization
                }
            };
            _paymentLegacyRepository.Setup(r => r.GetTransactionLogs(It.IsAny<int?>(), It.IsAny<string>()))
                .ReturnsAsync(paymentLogs);
            var orchestrator = _fixture.Create<FraudOrchestrator>();

            var result = await orchestrator.GetTransactionLogsAsync(request);

            var paymentLog = Assert.Single(result.PaymentLogs);
            Assert.Null(paymentLog.FraudResult);
        }

        [Theory]
        [InlineData("null")]
        [InlineData("0")]
        [InlineData("[]")]
        [InlineData("false")]
        [InlineData("\"valid JSON but not an object\"")]
        public async Task GetTransactionLogsAsync_FraudResultIsNotAJsonObject_SetsFraudResultToNull(string fraudResult)
        {
            var request = new GetTransactionLogsRequest
            {
                OrderNumber = 1,
                Skip = 0,
                Top = 1
            };
            var paymentLogs = new[]
            {
                new RawPaymentLog
                {
                    AdditionalInfo = $"{{\"fraudResult\":{fraudResult}}}",
                    OrderNumber = 1,
                    TxType = (char) PaymentTxTypePayment.Authorization
                }
            };
            _paymentLegacyRepository.Setup(r => r.GetTransactionLogs(It.IsAny<int?>(), It.IsAny<string>()))
                .ReturnsAsync(paymentLogs);
            var orchestrator = _fixture.Create<FraudOrchestrator>();

            var result = await orchestrator.GetTransactionLogsAsync(request);

            var paymentLog = Assert.Single(result.PaymentLogs);
            Assert.Null(paymentLog.FraudResult);
        }

        [Fact]
        public async Task GetTransactionLogsAsync_FraudCheckDataAccountScoresAreNot0_ReturnsFraudCheckResult()
        {
            var request = new GetTransactionLogsRequest
            {
                OrderNumber = 1,
                Skip = 0,
                Top = 1
            };
            var paymentLogs = new[]
            {
                new RawPaymentLog
                {
                    AdditionalInfo = JsonSerializer.Serialize(new
                    {
                        fraudResult = new AdyenFraudResult
                        {
                            accountScore = 1,
                            results = new[]
                            {
                                new AdyenFraudCheckResultContainer
                                {
                                    FraudCheckResult = new AdyenFraudCheckResult
                                    {
                                        accountScore = -1,
                                        checkId = 1,
                                        name = "should be returned 1"
                                    }
                                },
                                new AdyenFraudCheckResultContainer
                                {
                                    FraudCheckResult = new AdyenFraudCheckResult
                                    {
                                        accountScore = 0,
                                        checkId = 2,
                                        name = "should not be returned 1"
                                    }
                                },
                                new AdyenFraudCheckResultContainer
                                {
                                    FraudCheckResult = new AdyenFraudCheckResult
                                    {
                                        accountScore = 1,
                                        checkId = 3,
                                        name = "should be returned 2"
                                    }
                                }
                            }
                        }
                    }),
                    OrderNumber = 1,
                    TxType = (char) PaymentTxTypePayment.Authorization
                }
            };
            _paymentLegacyRepository.Setup(r => r.GetTransactionLogs(It.IsAny<int?>(), It.IsAny<string>()))
                .ReturnsAsync(paymentLogs);
            var orchestrator = _fixture.Create<FraudOrchestrator>();

            var result = await orchestrator.GetTransactionLogsAsync(request);

            var paymentLog = Assert.Single(result.PaymentLogs);
            Assert.Equal(1, paymentLog.FraudResult.AccountScore);
            Assert.Equal(2, paymentLog.FraudResult.Results.Count);
            Assert.All(paymentLog.FraudResult.Results, r => Assert.NotEqual(0, r.AccountScore));
        }
    }
}
