﻿using AutoFixture;
using AutoFixture.AutoMoq;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Factories;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Models.Consumed.Authorization;
using iHerb.Payment.Records.Models.Messages;
using iHerb.Payment.Records.Orchestrators;
using iHerb.Payment.Records.Utils;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Xunit;

namespace iHerb.Payment.Records.UnitTests.Orchestrators
{
    public class QueueMessageProcessingOrchestratorTests
    {
        private readonly IFixture _fixture;
        private readonly Mock<IPaymentLegacyOrchestrator> _aymentLegacyOrchestrator;

        public QueueMessageProcessingOrchestratorTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());

            _aymentLegacyOrchestrator = _fixture.Freeze<Mock<IPaymentLegacyOrchestrator>>();
        }


        [Fact]
        public async Task ProcessPaymentMessage_Success()
        {
            var message = new PaymentMessage<AuthorizationExternalRequest>
            {
                Source = MessageSource.TRANSACTION,
                Subject = MessageSubject.AUTHORISED,
                Body = new AuthorizationExternalRequest
                {
                    TransactionId = Guid.NewGuid(),
                    PaymentType = PaymentType.CreditCard,
                    PaymentSubType = PaymentSubType.VISA,
                    ReferenceId = "1234568990",
                    UserId = Guid.NewGuid(),
                    Amount = 101M,
                    Currency = "USD",
                    RawResponse = new AuthorizationExternalResponse
                    {
                        additionalData = new AdyenAdditionalData()
                    }
                }
            };
            var json = JsonConvert.SerializeObject(message);

            _fixture.Register<IPaymentLogsFactory>(() =>
            {
                var paymentLogWrapper = _fixture.Create<PaymentLogMapper>();
                return new PaymentLogsFactory(new Mock<ILogger<PaymentLogsFactory>>().Object, paymentLogWrapper);
            });
            var orchestrator = _fixture.Create<QueueMessageProcessingOrchestrator>();

            await orchestrator.TransferPaymentMessage(json);

            _aymentLegacyOrchestrator.Verify(m => m.ProcessPaymentMessage(It.IsAny<PaymentMessage>(), json), Times.Once);
        }

    }
}
