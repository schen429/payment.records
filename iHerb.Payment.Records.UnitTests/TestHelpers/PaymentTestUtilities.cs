using System;
using System.Data.SqlClient;
using System.Reflection;

namespace iHerb.Payment.Records.UnitTests.TestHelpers
{
    public static class PaymentTestUtilities
    {
        public static SqlException CreateSqlException(int number = 0)
        {
            var sqlErrorCollection = (SqlErrorCollection) typeof(SqlErrorCollection).GetConstructor(
                    BindingFlags.Instance | BindingFlags.NonPublic, null, CallingConventions.Any, Array.Empty<Type>(),
                    null)
                ?.Invoke(null);

            var sqlError = (SqlError) typeof(SqlError).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic,
                    null, CallingConventions.Any,
                    new[]
                    {
                        typeof(int), typeof(byte), typeof(byte), typeof(string), typeof(string), typeof(string),
                        typeof(int), typeof(Exception)
                    }, null)
                ?.Invoke(new object[]
                    {number, (byte) 2, (byte) 3, "server name", "error message", "proc", 1, new Exception()});

            typeof(SqlErrorCollection)
                .GetMethod("Add", BindingFlags.NonPublic | BindingFlags.Instance)
                ?.Invoke(sqlErrorCollection, new object[] { sqlError });

            var exception = (SqlException) typeof(SqlException).GetConstructor(
                    BindingFlags.Instance | BindingFlags.NonPublic, null, CallingConventions.Any,
                    new[] {typeof(string), typeof(SqlErrorCollection), typeof(Exception), typeof(Guid)}, null)
                ?.Invoke(new object[] {string.Empty, sqlErrorCollection, new Exception(), Guid.Empty});

            return exception;
        }
    }
}
