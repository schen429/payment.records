﻿using iHerb.Payment.Records.Services;
using iHerb.Platform.Constants;
using iHerb.Platform.PreferencesParser.DependencyInjections;
using iHerb.Platform.PreferencesParser.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Moq;
using System;
using Xunit;

namespace iHerb.Payment.Records.UnitTests.Services
{
    public class ContextAccessorServiceTests
    {
        private readonly Mock<HttpRequest> _httpRequest;
        private readonly Mock<HttpContext> _httpContext;
        private readonly Mock<IHttpContextAccessor> _httpContextAccessor;

        public ContextAccessorServiceTests()
        {
            _httpRequest = new Mock<HttpRequest>();
            _httpContext = CreateMockHttpContext(_httpRequest.Object);
            _httpContextAccessor = CreateMockHttpContextAccessor(_httpContext.Object);
        }

        [Fact]
        public void UserId_HttpRequestHeadersContainUserId_ReturnsUserIdHeaderFromHttpRequest()
        {
            var expectedUserId = Guid.NewGuid();
            var service = CreateContextAccessorService();
            _httpRequest.Setup(request => request.Headers).Returns(new HeaderDictionary
            {
                { "UserId", new StringValues(expectedUserId.ToString()) }
            });

            var userId = service.UserId;

            Assert.Equal(expectedUserId, userId);
        }

        [Fact]
        public void UserId_HttpRequestHeadersDoNotContainUserId_ReturnsNull()
        {
            var service = CreateContextAccessorService();
            _httpRequest.Setup(request => request.Headers).Returns(new HeaderDictionary());

            var userId = service.UserId;

            Assert.Null(userId);
        }

        #region Helpers

        private static Mock<IHttpContextAccessor> CreateMockHttpContextAccessor(HttpContext httpContext)
        {
            var mock = new Mock<IHttpContextAccessor>();

            mock.Setup(accessor => accessor.HttpContext).Returns(httpContext);

            return mock;
        }

        private static Mock<HttpContext> CreateMockHttpContext(HttpRequest httpRequest)
        {
            var mock = new Mock<HttpContext>();

            mock.Setup(context => context.Request).Returns(httpRequest);

            return mock;
        }

        private ContextAccessorService CreateContextAccessorService()
        {
            return new ContextAccessorService(_httpContextAccessor.Object);
        }

        private ContextAccessorService CreateContextAccessorService(byte prefDeviceStoreValue, string applicationId)
        {
            // request
            var headers = new Mock<IHeaderDictionary>();
            headers.Setup(m => m.ContainsKey("x-application-id"))
                .Returns(true);

            StringValues appIdStringValues = applicationId;
            headers.Setup(x => x.TryGetValue("x-application-id", out appIdStringValues)).Returns(true);
            headers.SetupGet(m => m["x-application-id"])
                .Returns(new StringValues(applicationId));
            var request = new Mock<HttpRequest>();
            request.Setup(m => m.Headers)
                .Returns(headers.Object);

            // HttpContext
            var httpContext = new Mock<HttpContext>();
            httpContext.Setup(m => m.Request)
                .Returns(request.Object);

            // Http context accessor
            var httpContextAccessor = new Mock<IHttpContextAccessor>();
            httpContextAccessor.Setup(m => m.HttpContext)
                .Returns(httpContext.Object);

            return new ContextAccessorService(httpContextAccessor.Object);
        }

        #endregion
    }
}
