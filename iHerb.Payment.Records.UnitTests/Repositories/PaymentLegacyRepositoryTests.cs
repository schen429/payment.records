﻿using AutoFixture;
using AutoFixture.AutoMoq;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Metrics;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Models.PaymentLogs;
using iHerb.Payment.Records.Repositories;
using Moq;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Xunit;

namespace iHerb.Payment.Records.UnitTests.Repositories
{
    public class PaymentLegacyRepositoryTests
    {
        private readonly IFixture _fixture;
        private readonly Mock<ISqlExecutor> _sqlExecutor;
        private readonly Mock<ISqlMetricsCollector> _sqlMetricsCollector;

        public PaymentLegacyRepositoryTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _sqlExecutor = _fixture.Freeze<Mock<ISqlExecutor>>();
            _fixture.Freeze<ISqlMetricsCollector>();
            _fixture.Inject<ISqlMetricsCollector>(_fixture.Create<SqlMetricsCollector>());
        }

        [Fact]
        public async Task InsertPaymentLog_MissingParams_Success()
        {
            var repo = _fixture.Create<PaymentLegacyRepository>();

            await repo.InsertPaymentLog(new PaymentLog
            {
            });

            _sqlExecutor.Verify(m => m.ExecuteNonQueryAsync(It.IsAny<DbCommand>()), Times.Once);
        }

        [Fact]
        public async Task InsertPaymentLog_AllParams_Success()
        {
            var repo = _fixture.Create<PaymentLegacyRepository>();

            await repo.InsertPaymentLog(new PaymentLog
            {
                OrderNumber = 1,
                OrderExt = 0,
                CustomerId = Guid.NewGuid(),
                AccountNo = "AcctNum",
                AccountType = "AccType",
                Amount = 100,
                AuthCode = "S",
                ReferenceId = "RefId",
                TxType = PaymentTxTypePayment.Authorization,
                ResponseMessage = "RespMessage",
                ResponseCode = "RespCode",
                Approved = true,
                Comment = "Comment",
                Username = "UserName",
                Voided = false,
                Captured = false,
                CountryCode = "US",
                BankName = "BankName",
                AdditionalInfo = "AdditionalInfo",
                PaymentEngine = 0,
                ReconciliationId = "ReconciliationId",
                DetailCardIndicator = "Indicator",
                ProductId = "ProductId",
                CardClass = "CardClass",
                ProductSubType = "ProdSubType"
            });

            _sqlExecutor.Verify(m => m.ExecuteNonQueryAsync(It.IsAny<DbCommand>()), Times.Once);
        }

        [Fact]
        public async Task InsertOrderPayment_Success()
        {
            var repo = _fixture.Create<PaymentLegacyRepository>();

            await repo.InsertOrderPayment(100, "1234", 0);

            _sqlExecutor.Verify(m => m.ExecuteAsync(
                It.IsAny<IDbConnection>(),
                It.IsAny<string>(),
                It.IsAny<object>(),
                It.IsAny<IDbTransaction>(),
                It.IsAny<int?>(),
                It.IsAny<CommandType?>()), 
                Times.Once);
        }

        [Fact]
        public async Task UpsertPaymentProperties_Success()
        {
            var paymentProperties = new PaymentProperties
            {
                TransactionId = Guid.NewGuid(),
                CustomerId = Guid.NewGuid(),
                OrderNumber = 1,
                TransactionDate = DateTime.Now,
                TransactionStatus = TransactionStatus.AUTHORISED.ToString(),
                BillingAddress = JsonConvert.SerializeObject(new BillingAddress()),
                LastUpdatedBy = "PaymentApi"
            };

            var repo = _fixture.Create<PaymentLegacyRepository>();

            await repo.UpsertPaymentProperties(paymentProperties);

            _sqlExecutor.Verify(m => m.ExecuteAsync(
                It.IsAny<IDbConnection>(),
                It.IsAny<string>(),
                It.IsAny<object>(),
                It.IsAny<IDbTransaction>(),
                It.IsAny<int?>(),
                It.IsAny<CommandType?>()),
                Times.Once);
        }

        [Fact]
        public async Task GetBillingAddressByOrderNumber_Success()
        {
            var repo = _fixture.Create<PaymentLegacyRepository>();

            var result = repo.GetBillingAddressByOrderNumber(1);

            Assert.True(true);
        }

        [Fact]
        public async Task GetTransactionLog_Success()
        {
            var repo = _fixture.Create<PaymentLegacyRepository>();

            var result = repo.GetTransactionLog(1);

            Assert.True(true);
        }

        [Fact]
        public async Task GetTransactionLogs_Success()
        {
            var repo = _fixture.Create<PaymentLegacyRepository>();

            var result = repo.GetTransactionLogs(1, null);

            Assert.True(true);
        }

        [Fact]
        public async Task GetTransactionsByUser_Success()
        {
            var repo = _fixture.Create<PaymentLegacyRepository>();

            var result = repo.GetTransactionsByUser(Guid.Empty);

            Assert.True(true);
        }
    }
}
