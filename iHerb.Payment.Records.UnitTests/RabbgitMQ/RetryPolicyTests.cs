﻿using AutoFixture;
using AutoFixture.AutoMoq;
using iHerb.Payment.Records.RabbgitMQ;
using Microsoft.Extensions.Configuration;
using Moq;
using RabbitMQ.Client;
using System.Threading.Tasks;
using Xunit;

namespace iHerb.Payment.Records.UnitTests.RabbgitMQ
{
    public class RetryPolicyTests
    {
        private readonly IFixture _fixture;
        private readonly Mock<IConfiguration> _configuration;
        private readonly Mock<IModel> _channel;
        private readonly Mock<IConnection> _rabbitConnection;
        private readonly Mock<IRabbitConnectionProvider> _rabbitConnectionProvider;

        public RetryPolicyTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());

            _configuration = _fixture.Freeze<Mock<IConfiguration>>();
            var maxRetryValue = new Mock<IConfigurationSection>();
            maxRetryValue.Setup(m => m.Value).Returns("5");
            _configuration.Setup(m => m.GetSection("Queues:MaxRetry")).Returns(maxRetryValue.Object);
            var RetryIntervalSecondsValue = new Mock<IConfigurationSection>();
            RetryIntervalSecondsValue.Setup(m => m.Value).Returns("1");
            _configuration.Setup(m => m.GetSection("Queues:RetryIntervalSeconds")).Returns(RetryIntervalSecondsValue.Object);
            _configuration.Setup(m => m["Queues:RetryExchange"]).Returns("payment.exchange.retry");

            _channel = _fixture.Freeze<Mock<IModel>>();
            _rabbitConnection = _fixture.Freeze<Mock<IConnection>>();
            _rabbitConnection.Setup(m => m.CreateModel())
                .Returns(_channel.Object);
            _rabbitConnectionProvider = _fixture.Freeze<Mock<IRabbitConnectionProvider>>();
            _rabbitConnectionProvider.Setup(m => m.GetConnection())
                .Returns(_rabbitConnection.Object);
        }

        [Fact]
        public async Task RetryAsync_FirstRetry()
        {
            var retryPolicy = _fixture.Create<RetryPolicy>();

            var retried = await retryPolicy.RetryAsync("payment.exchange", null, "payment.authorisation.creditcard.authorised", new byte[0]);

            Assert.True(retried);
        }

        [Fact]
        public async Task RetryAsync_SecondRetry()
        {
            var retryPolicy = _fixture.Create<RetryPolicy>();

            var retried = await retryPolicy.RetryAsync("payment.exchange.retry", "1", "payment.authorisation.creditcard.authorised", new byte[0]);

            Assert.True(retried);
        }

        [Fact]
        public async Task RetryAsync_NoRetry()
        {
            var retryPolicy = _fixture.Create<RetryPolicy>();

            var retried = await retryPolicy.RetryAsync("payment.exchange.retry", "6", "payment.authorisation.creditcard.authorised", new byte[0]);

            Assert.False(retried);
        }
    }
}
