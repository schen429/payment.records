﻿using AutoFixture;
using AutoFixture.AutoMoq;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Models.Consumed.Authorization;
using iHerb.Payment.Records.Models.Messages;
using iHerb.Payment.Records.Orchestrators;
using iHerb.Payment.Records.RabbgitMQ;
using Microsoft.Extensions.Configuration;
using Moq;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Xunit;

namespace iHerb.Payment.Records.UnitTests.RabbgitMQ
{
    public class SecuredMessageProcessorTests
    {
        private readonly IFixture _fixture;
        private readonly Mock<IConfiguration> _configuration;
        private readonly Mock<IQueueMessageProcessingOrchestrator> _orchestrator;

        public SecuredMessageProcessorTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());

            _configuration = _fixture.Freeze<Mock<IConfiguration>>();
            var maxRetryValue = new Mock<IConfigurationSection>();
            maxRetryValue.Setup(m => m.Value).Returns("5");
            _configuration.Setup(m => m.GetSection("Queues:MaxRetry")).Returns(maxRetryValue.Object);
            var RetryIntervalSecondsValue = new Mock<IConfigurationSection>();
            RetryIntervalSecondsValue.Setup(m => m.Value).Returns("1");
            _configuration.Setup(m => m.GetSection("Queues:RetryIntervalSeconds")).Returns(RetryIntervalSecondsValue.Object);
            _configuration.Setup(m => m["Queues:RetryExchange"]).Returns("payment.exchange.retry");
            _configuration.Setup(m => m["Queues:Exchange"]).Returns("payment.exchange");
            _configuration.Setup(m => m["Queues:SecuredMessageQueue"]).Returns("payment.Secured.reporting");
            _configuration.Setup(m => m["Queues:SecuredMessageQueueRoutingKey"]).Returns("payment.Secured.*.*");

            _orchestrator = _fixture.Freeze<Mock<IQueueMessageProcessingOrchestrator>>();
        }

        [Fact]
        public async Task ProcessMessage_Success()
        {
            var message = new PaymentMessage<AuthorizationExternalRequest>
            {
                Source = MessageSource.TRANSACTION,
                Subject = MessageSubject.AUTHORISED,
                Body = new AuthorizationExternalRequest
                {
                    TransactionId = Guid.NewGuid(),
                    PaymentType = PaymentType.CreditCard,
                    PaymentSubType = PaymentSubType.VISA,
                    ReferenceId = "1234568990",
                    UserId = Guid.NewGuid(),
                    Amount = 101M,
                    Currency = "USD",
                    RawResponse = new AuthorizationExternalResponse
                    {
                        additionalData = new AdyenAdditionalData()
                    }
                }
            };
            var json = JsonConvert.SerializeObject(message);


            var processor = _fixture.Create<SecuredMessageProcessor>();

            await processor.ProcessMessage(json);

            _orchestrator.Verify(
                m => m.TransferPaymentMessage(It.IsAny<string>()),
                Times.Once);
        }
    }
}
