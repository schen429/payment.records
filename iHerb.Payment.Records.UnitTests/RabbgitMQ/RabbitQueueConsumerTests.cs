﻿using AutoFixture;
using AutoFixture.AutoMoq;
using iHerb.Payment.Records.RabbgitMQ;
using Microsoft.Extensions.Configuration;
using Moq;
using RabbitMQ.Client;
using System.Threading.Tasks;
using Xunit;

namespace iHerb.Payment.Records.UnitTests.RabbgitMQ
{
    public class RabbitQueueConsumerTests
    {
        private readonly IFixture _fixture;
        private readonly Mock<IConfiguration> _configuration;
        private readonly Mock<IModel> _channel;
        private readonly Mock<IConnection> _rabbitConnection;
        private readonly Mock<IRabbitConnectionProvider> _rabbitConnectionProvider;
        private readonly Mock<IMessageProcessor> _messageProcessor;

        public RabbitQueueConsumerTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());

            _configuration = _fixture.Freeze<Mock<IConfiguration>>();
            var maxRetryValue = new Mock<IConfigurationSection>();
            maxRetryValue.Setup(m => m.Value).Returns("5");
            _configuration.Setup(m => m.GetSection("Queues:MaxRetry")).Returns(maxRetryValue.Object);
            var RetryIntervalSecondsValue = new Mock<IConfigurationSection>();
            RetryIntervalSecondsValue.Setup(m => m.Value).Returns("1");
            _configuration.Setup(m => m.GetSection("Queues:RetryIntervalSeconds")).Returns(RetryIntervalSecondsValue.Object);
            _configuration.Setup(m => m["Queues:RetryExchange"]).Returns("payment.exchange.retry");
            _configuration.Setup(m => m["Queues:Exchange"]).Returns("payment.exchange");
            _configuration.Setup(m => m["Queues:AuthorisationMessageQueue"]).Returns("payment.authorisation.reporting");
            _configuration.Setup(m => m["Queues:AuthorisationMessageQueueRoutingKey"]).Returns("payment.authorisation.*.*");

            _channel = _fixture.Freeze<Mock<IModel>>();
            _channel.Setup(m => m.QueueDeclare(It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>(), null))
                .Returns(_fixture.Create<QueueDeclareOk>());
            _rabbitConnection = _fixture.Freeze<Mock<IConnection>>();
            _rabbitConnection.Setup(m => m.CreateModel())
                .Returns(_channel.Object);
            _rabbitConnectionProvider = _fixture.Freeze<Mock<IRabbitConnectionProvider>>();
            _rabbitConnectionProvider.Setup(m => m.GetConnection())
                .Returns(_rabbitConnection.Object);

            _messageProcessor = _fixture.Freeze<Mock<IMessageProcessor>>();
            _messageProcessor.Setup(m => m.Name).Returns("AuthorisationMessageProcessor");
            _messageProcessor.Setup(m => m.ExchangeName).Returns("payment.exchange");
            _messageProcessor.Setup(m => m.QueueName).Returns("payment.authorisation.reporting");
            _messageProcessor.Setup(m => m.RoutingKeys).Returns(new string[] { "payment.authorisation.*.*" });
        }

        [Fact]
        public async Task RegisterMessageProcessor_Success()
        {
            var consumer = _fixture.Create<RabbitQueueConsumer>();

            consumer.RegisterMessageProcessor(_messageProcessor.Object);

            _rabbitConnection.Verify(c => c.CreateModel(), Times.Once);
        }

    }
}
