using System;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Orchestrators;
using iHerb.Payment.Records.RabbgitMQ;
using Microsoft.Extensions.Configuration;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace iHerb.Payment.Records.UnitTests.RabbgitMQ
{
    public class ChargebackMessageProcessorTests
    {
        private readonly IFixture _fixture;
        private readonly Mock<IConfiguration> _configuration;
        private readonly Mock<IPaymentLegacyOrchestrator> _orchestrator;

        public ChargebackMessageProcessorTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());

            _configuration = _fixture.Freeze<Mock<IConfiguration>>();
            var maxRetryValue = new Mock<IConfigurationSection>();
            maxRetryValue.Setup(m => m.Value).Returns("5");
            _configuration.Setup(m => m.GetSection("Queues:MaxRetry")).Returns(maxRetryValue.Object);
            var RetryIntervalSecondsValue = new Mock<IConfigurationSection>();
            RetryIntervalSecondsValue.Setup(m => m.Value).Returns("1");
            _configuration.Setup(m => m.GetSection("Queues:RetryIntervalSeconds")).Returns(RetryIntervalSecondsValue.Object);
            _configuration.Setup(m => m["Queues:RetryExchange"]).Returns("payment.exchange.retry");
            _configuration.Setup(m => m["Queues:Exchange"]).Returns("payment.exchange");
            _configuration.Setup(m => m["Queues:ChargebackMessageQueue"]).Returns("payment.chargeback.reporting");
            _configuration.Setup(m => m["Queues:ChargebackMessageQueueRoutingKey"]).Returns("payment.chargeback.*.*");

            _orchestrator = _fixture.Freeze<Mock<IPaymentLegacyOrchestrator>>();
        }
        
        [Fact]
        public async Task ProcessMessage_Success()
        {
            var message = new TransactionMessage()
            {
                Alias = "test alias",
                Amount = 100M,
                BillingAddress = new BillingAddress
                {
                    Country = "US",
                    Street = "Sand Canyon Ave",
                    City = "Irvine",
                    StateOrProvince = "CA",
                    HouseNumberOrName = "15535",
                    PostalCode = "92618"
                },
                Country = "US",
                Currency = "USD",
                PaymentType = PaymentType.CreditCard,
                ReferenceId = "1234567890",
                TransactionId = Guid.NewGuid(),
                UserId = Guid.NewGuid(),
                RawResponse = "test raw response",
                PaymentSubType = "VISA",
                PaymentEngine = TokenEngineTransactions.ADYEN3DS2,
                ActionType = ActionType.NONE,
                ActionUrl = string.Empty,
                TransactionType = TransactionType.CHARGEBACK,
                TransactionStatus = TransactionStatus.REFUNDED
            };
            var json = JsonConvert.SerializeObject(message);

            var processor = _fixture.Create<ChargebackMessageProcessor>();

            await processor.ProcessMessage(json);

            _orchestrator.Verify(
                m => m.ProcessChargebackMessage(It.Is<TransactionMessage>(message => message.ReferenceId == "1234567890")),
                Times.Once);
        }
    }
}