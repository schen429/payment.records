using AutoFixture;
using AutoFixture.AutoMoq;
using iHerb.Payment.Records.Controllers;
using iHerb.Payment.Records.Models.Fraud;
using iHerb.Payment.Records.Orchestrators;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace iHerb.Payment.Records.UnitTests.Controllers
{
    public class FraudControllerTests
    {
        private readonly IFixture _fixture;
        private readonly Mock<IFraudOrchestrator> _fraudOrchestrator;

        public FraudControllerTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());

            _fixture.Customize<BindingInfo>(c => c.OmitAutoProperties());

            _fraudOrchestrator = Mock.Get(_fixture.Freeze<IFraudOrchestrator>());
        }

        [Fact]
        public async Task GetTransactionLogs_ResponseTotalIsNot0_ReturnsOrchestratorResponse()
        {
            var request = new GetTransactionLogsRequest();
            var orchestratorResponse = new GetTransactionLogsResponse { Total = 1 };
            _fraudOrchestrator.Setup(o => o.GetTransactionLogsAsync(It.IsAny<GetTransactionLogsRequest>()))
                .ReturnsAsync(orchestratorResponse);
            var controller = _fixture.Create<FraudController>();

            var response = await controller.GetTransactionLogs(request);

            var okObjectResult = Assert.IsType<OkObjectResult>(response.Result);
            Assert.Equal(orchestratorResponse, okObjectResult.Value);
        }

        [Fact]
        public async Task GetTransactionLogs_ResponseTotalIs0_ReturnsNotFound()
        {
            var request = new GetTransactionLogsRequest();
            var orchestratorResponse = new GetTransactionLogsResponse { Total = 0 };
            _fraudOrchestrator.Setup(o => o.GetTransactionLogsAsync(It.IsAny<GetTransactionLogsRequest>()))
                .ReturnsAsync(orchestratorResponse);
            var controller = _fixture.Create<FraudController>();

            var response = await controller.GetTransactionLogs(request);

            Assert.IsType<NotFoundResult>(response.Result);
        }
    }
}
