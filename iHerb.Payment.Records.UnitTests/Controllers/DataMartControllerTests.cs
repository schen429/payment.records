﻿using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;
using FluentAssertions;
using iHerb.Payment.Records.Controllers;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Models.PaymentLogs;
using iHerb.Payment.Records.Orchestrators;
using iHerb.Payment.Records.Services;
using iHerb.Platform.Authentication.DependencyInjections;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace iHerb.Payment.Records.UnitTests.Controllers
{
    public class DataMartControllerTests
    {
        private readonly IFixture _fixture;
        private readonly Mock<IPaymentLegacyOrchestrator> _paymentLegacyOrchestrator;
        private readonly Mock<IAuthenticationService> _authenticationService;
        private readonly Mock<IContextAccessorService> _contextAccessorService;
        private readonly RecordsController _controller;

        public DataMartControllerTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _paymentLegacyOrchestrator = Mock.Get(_fixture.Freeze<IPaymentLegacyOrchestrator>());
            Mock.Get(_fixture.Freeze<ILogger<RecordsController>>());
            _authenticationService = Mock.Get(_fixture.Freeze<IAuthenticationService>());
            _contextAccessorService = Mock.Get(_fixture.Freeze<IContextAccessorService>());
            _controller = new RecordsController(_paymentLegacyOrchestrator.Object, _authenticationService.Object, _contextAccessorService.Object);
        }

        [Fact]
        public async Task GetTransactionLogs_NoLogsReturned_ReturnNotFound()
        {
            _paymentLegacyOrchestrator.Setup(c => c.GetTransactionLogs(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new TransactionLogResponse() { PaymentLogs = new List<PaymentLog>() });
            var response = await _controller.GetTransactionLogs("1", "abc");
            var statusCode = (response.Result as IStatusCodeActionResult)?.StatusCode;
            statusCode.Should().Be(StatusCodes.Status404NotFound);
        }

        [Fact]
        public async Task GetTransactionLogs_OrchestratorThrowException_ReturnNotFound()
        {
            _paymentLegacyOrchestrator.Setup(c => c.GetTransactionLogs(It.IsAny<int>(), It.IsAny<string>()))
                .ThrowsAsync(new Exception());

            await Assert.ThrowsAnyAsync<Exception>(() => _controller.GetTransactionLogs("1", null));
        }

        [Fact]
        public async Task GetTransactionLogs_EmptyOrderNumber_ReturnNotFound()
        {
            _paymentLegacyOrchestrator.Setup(c => c.GetTransactionLogs(It.IsAny<int>(), It.IsAny<string>()))
                .ThrowsAsync(new Exception());

            var response = await _controller.GetTransactionLogs(string.Empty, "abc");
            var statusCode = (response.Result as IStatusCodeActionResult)?.StatusCode;
            statusCode.Should().Be(StatusCodes.Status404NotFound);
        }

        [Theory]
        [AutoData]
        public async Task GetTransactionLogs_ReturnPaymentLogs(List<PaymentLog> paymentLogs)
        {
            _paymentLegacyOrchestrator.Setup(c => c.GetTransactionLogs(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new TransactionLogResponse() { PaymentLogs = paymentLogs });
            var response = await _controller.GetTransactionLogs("1", null);
            var statusCode = (response.Result as IStatusCodeActionResult)?.StatusCode;
            statusCode.Should().NotBeNull();
        }
        
        [Theory]
        [AutoData]
        public async Task GetTransactionLogs_InvalidSortExpression(List<PaymentLog> paymentLogs)
        {
            var sortExpression = ";Drop Table";
            _paymentLegacyOrchestrator.Setup(c => c.GetTransactionLogs(It.IsAny<int>(), It.IsAny<string>()))
                .ReturnsAsync(new TransactionLogResponse() { PaymentLogs = paymentLogs });
            var response = await _controller.GetTransactionLogs("1", sortExpression);
            var statusCode = (response.Result as IStatusCodeActionResult)?.StatusCode;
            statusCode.Should().Be(400);
        }
        
        [Theory]
        [AutoData]
        public async Task GetPaymentTransactionInfo_ReturnTransactionInfo(int orderNumber, byte? orderExt, PaymentTransactionInfoResponse paymentTransactionInfoResponse)
        {
            _paymentLegacyOrchestrator.Setup(c => c.GetPaymentTransactionInfo(orderNumber, orderExt))
                .ReturnsAsync(paymentTransactionInfoResponse);
            var response = await _controller.GetPaymentTransactionInfo(orderNumber, orderExt);
            var statusCode = (response.Result as IStatusCodeActionResult)?.StatusCode;
            statusCode.Should().NotBeNull();
        }
        
        [Theory]
        [AutoData]
        public async Task GetPaymentTransactionInfo_ReturnEmpty(int orderNumber, byte? orderExt)
        {
            _paymentLegacyOrchestrator.Setup(c => c.GetPaymentTransactionInfo(orderNumber, orderExt))
                .ReturnsAsync((PaymentTransactionInfoResponse)null);
            var response = await _controller.GetPaymentTransactionInfo(orderNumber, orderExt);
            var statusCode = (response.Result as IStatusCodeActionResult)?.StatusCode;
            statusCode.Should().Be(StatusCodes.Status404NotFound);
        }
        
        [Theory]
        [AutoData]
        public async Task GetPaymentTransactionInfo_OrchestratorThrowException_ReturnNotFound(int orderNumber, byte? orderExt)
        {
            _paymentLegacyOrchestrator.Setup(c => c.GetPaymentTransactionInfo(orderNumber, orderExt))
                .ThrowsAsync(new Exception());

            await Assert.ThrowsAnyAsync<Exception>(() => _controller.GetPaymentTransactionInfo(orderNumber, orderExt));
        }

        [Fact]
        public async Task GetUserTransactionLogs_Success()
        {
            var userId = Guid.NewGuid();

            _contextAccessorService.Setup(m => m.UserId)
                .Returns(userId);

            _paymentLegacyOrchestrator.Setup(m => m.GetTransactionsByUser(It.IsAny<Guid>(), It.IsAny<PaymentType?>(), It.IsAny<string>(), It.IsAny<DateTime?>(), It.IsAny<DateTime?>()))
                .ReturnsAsync(new TransactionLogResponse
                {
                    PaymentLogs = new List<PaymentLog>
                    {
                        new PaymentLog
                        {
                            CustomerId = userId,
                            OrderNumber = 123456,
                            OrderExt = 0
                        }
                    }
                });

            var result = await _controller.GetUserTransactionLogs(userId);
            var objectResult = Assert.IsType<OkObjectResult>(result.Result);
            var transactionLogResponse = Assert.IsType<TransactionLogResponse>(objectResult.Value);
            Assert.Single(transactionLogResponse.PaymentLogs, paymentLog => paymentLog.CustomerId == userId);
        }

        [Fact]
        public async Task GetUserTransactionLogs_UnmatchedUserId_Forbidden()
        {
            var userId = Guid.Empty;

            _contextAccessorService.Setup(m => m.UserId)
                .Returns(Guid.Empty);

            var result = await _controller.GetUserTransactionLogs(userId);
            Assert.IsType<ForbidResult>(result.Result);
        }

        [Theory, AutoData]
        public async Task GetBillingAddress_NoRecordFound(int orderNumber)
        {
            _paymentLegacyOrchestrator.Setup(x=>x.GetBillingAddress(orderNumber))
                .ReturnsAsync ((BillingAddressResponse) null);

            var response = await _controller.GetBillingAddresses(orderNumber);
            
            var statusCode = (response.Result as IStatusCodeActionResult)?.StatusCode;
            statusCode.Should().Be(404);
        }
        
        [Theory, AutoData]
        public async Task GetBillingAddress_Successful(int orderNumber, BillingAddressResponse billingAddress)
        {
            _paymentLegacyOrchestrator.Setup(x=>x.GetBillingAddress(orderNumber))
                .ReturnsAsync (billingAddress);

            var response = await _controller.GetBillingAddresses(orderNumber);

            (response.Result as OkObjectResult)?.Value.Should().BeOfType<BillingAddressResponse>();
        }
        
        [Theory, AutoData]
        public async Task GetOrderTotalPaid_Successful(int orderNumber, byte? ext, decimal amount)
        {
            _paymentLegacyOrchestrator.Setup(x=>x.GetOrderTotalPaid(orderNumber, ext))
                .ReturnsAsync (new OrderPaidAmountResponse{ AmountTotalPaid = amount});

            var response = await _controller.GetOrderAmount(orderNumber, ext);

            (response.Result as OkObjectResult)?.Value.Should().BeOfType<OrderPaidAmountResponse>();
        }
        
        [Theory, AutoData]
        public async Task GetOrderTotalPaid_NotFound(int orderNumber, byte? ext)
        {
            _paymentLegacyOrchestrator.Setup(x=>x.GetOrderTotalPaid(orderNumber, ext))
                .ReturnsAsync ((OrderPaidAmountResponse)null);

            var response = await _controller.GetOrderAmount(orderNumber, ext);

            var statusCode = (response.Result as IStatusCodeActionResult)?.StatusCode;
            statusCode.Should().Be(404);
        }
    }
}
