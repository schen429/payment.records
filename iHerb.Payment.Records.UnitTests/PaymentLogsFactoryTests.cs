﻿using AutoFixture;
using AutoFixture.AutoMoq;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Factories;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Models.Consumed.Authorization;
using iHerb.Payment.Records.Models.Consumed.Tokenization;
using iHerb.Payment.Records.Models.Messages;
using Newtonsoft.Json;
using System;
using Xunit;

namespace iHerb.Payment.Records.UnitTests
{
    public class PaymentLogsFactoryTests
    {
        private readonly IFixture _fixture;

        public PaymentLogsFactoryTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
        }

        [Fact]
        public void GetPaymentLog_TokenizationSuccessExternalResponse()
        {
            var factory = _fixture.Create<PaymentLogsFactory>();

            var externalRequest = new TokenizationExternalRequest
            {
                UserId = Guid.NewGuid(),
                CurrencyCode = "USD",
                PaymentAccountId = "test-account-id",
                AccountData = new CreditCardRawAccountDataRequest
                {
                    AccountHolderName = "holderName",
                    NationalIdentificationNumber = "Id",
                    CardData = new CreditCardDataRequest
                    {
                        BillingAddress = new CreditCardPaymentBillingAddressRequest
                        {
                        }
                    }
                }
            };

            var externalResponse = new TokenizationSuccessExternalResponse
            {
                additionalData = new AdyenAdditionalData
                {
                },
                resultCode = "Authorised"
            };

            var creditCardData = new CreditCardBinData
            {
            };

            var message = new TokenizationMessage
            {
                Source = Enums.MessageSource.ACCOUNT,
                Subject = Enums.MessageSubject.AUTHORISED,
                Body = new PaymentMessageBody
                {
                    ExternalRequest = JsonConvert.SerializeObject(externalRequest),
                    ExternalResponse = JsonConvert.SerializeObject(externalResponse),
                    CreditCardBinData = JsonConvert.SerializeObject(creditCardData)
                }
            };

            var log = factory.GetPaymentLog(MessageSource.ACCOUNT, MessageSubject.AUTHORISED, JsonConvert.SerializeObject(message));

            Assert.Equal(externalRequest.AccountData.AccountHolderName, log.AccountName);
        }

        [Fact]
        public void GetTokenizationPaymentLogFailed_TokenizationErrorExternalResponse()
        {
            var factory = _fixture.Create<PaymentLogsFactory>();

            var externalRequest = new TokenizationExternalRequest
            {
                UserId = Guid.NewGuid(),
                CurrencyCode = "USD",
                PaymentAccountId = "test-account-id",
                AccountData = new CreditCardRawAccountDataRequest
                {
                    AccountHolderName = "holderName",
                    NationalIdentificationNumber = "Id",
                    CardData = new CreditCardDataRequest
                    {
                        BillingAddress = new CreditCardPaymentBillingAddressRequest
                        {
                        }
                    }
                }
            };

            var externalResponse = new TokenizationErrorExternalResponse
            {
            };

            var creditCardData = new CreditCardBinData
            {
            };

            var message = new TokenizationMessage
            {
                Source = Enums.MessageSource.ACCOUNT,
                Subject = Enums.MessageSubject.FAILED,
                Body = new PaymentMessageBody
                {
                    ExternalRequest = JsonConvert.SerializeObject(externalRequest),
                    ExternalResponse = JsonConvert.SerializeObject(externalResponse),
                    CreditCardBinData = JsonConvert.SerializeObject(creditCardData)
                }
            };

            var log = factory.GetPaymentLog(MessageSource.ACCOUNT, MessageSubject.FAILED, JsonConvert.SerializeObject(message));

            Assert.Equal(externalRequest.AccountData.AccountHolderName, log.AccountName);
        }

        [Fact]
        public void GetAuthorizationPaymentLogSuccess_TokenizationSuccessExternalResponse()
        {
            var factory = _fixture.Create<PaymentLogsFactory>();

            var message = new AuthorizationMessage
            {
                Source = Enums.MessageSource.TRANSACTION,
                Subject = Enums.MessageSubject.AUTHORISED,
                Body = new AuthorizationExternalRequest
                {
                    TransactionId = Guid.NewGuid(),
                    PaymentType = Enums.PaymentType.Adyen_Ideal,
                    RawResponse = new AuthorizationExternalResponse
                    {
                        resultCode = AdyenResultCode.Authorised,
                        additionalData = new AdyenAdditionalData
                        {
                        }
                    }
                }
            };

            var log = factory.GetPaymentLog(MessageSource.TRANSACTION, MessageSubject.AUTHORISED, JsonConvert.SerializeObject(message));

            Assert.Equal(PaymentTxTypePayment.Authorization, log.TxType);
        }

        [Fact]
        public void GetAuthorizationRequestPaymentLog_TokenizationSuccessExternalResponse()
        {
            var factory = _fixture.Create<PaymentLogsFactory>();

            var message = new AuthorizationMessage
            {
                Source = Enums.MessageSource.TRANSACTION,
                Subject = Enums.MessageSubject.AUTHORISED,
                Body = new AuthorizationExternalRequest
                {
                    TransactionId = Guid.NewGuid(),
                    PaymentType = PaymentType.Adyen_Ideal,
                    RawResponse = new AuthorizationExternalResponse
                    {
                        resultCode = AdyenResultCode.Authorised,
                        additionalData = new AdyenAdditionalData
                        {
                        }
                    },
                    ActionType = ActionType.ADYEN_REDIRECT
                }
            };

            var log = factory.GetPaymentLog(MessageSource.TRANSACTION, MessageSubject.AUTHORISED, JsonConvert.SerializeObject(message));

            Assert.Equal(PaymentType.Adyen_Ideal.ToString(), log.AccountType);
        }
    }
}
