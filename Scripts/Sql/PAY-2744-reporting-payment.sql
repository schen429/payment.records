USE [iHerb_Reporting_Payment]
GO

-- [payment].[TBL_OrderPayments]

IF (EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE SPECIFIC_SCHEMA = 'payment' AND  SPECIFIC_NAME = 'spx_Payment_AddOrderPayment2'))
	DROP PROCEDURE [payment].[spx_Payment_AddOrderPayment2]
GO

IF (EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'payment' AND  TABLE_NAME = 'TBL_OrderPayments'))
	DROP TABLE [payment].[TBL_OrderPayments]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [payment].[TBL_OrderPayments](
	[PaymentID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[OrderNumber] [int] NOT NULL,
	[OrderExt] [tinyint] NOT NULL,
	[ReferenceID] [varchar](100) NOT NULL,
	[Amount] [money] NOT NULL,
	[AuthCode] [char](6) NULL,
	[PaymentDate] [datetime] NOT NULL,
	[OrderPaymentType] [tinyint] NULL,
 CONSTRAINT [PK_payment_TBL_OrderPayments] PRIMARY KEY CLUSTERED 
(
	[PaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [payment].[TBL_OrderPayments] ADD  CONSTRAINT [DF_payment_TBL_OrderPayments_OrderExt_0]  DEFAULT ((0)) FOR [OrderExt]
GO

CREATE NONCLUSTERED INDEX [IX_TBL_OrderPayments_OrderNumber_Amount] ON [payment].[TBL_OrderPayments]
(
	[OrderNumber] ASC,
	[Amount] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_TBL_OrderPayments_PaymentDate] ON [payment].[TBL_OrderPayments]
(
	[PaymentDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE PROCEDURE [payment].[spx_Payment_AddOrderPayment2]
	(
		@OrderNumber INT,
		@OrderExt TINYINT = 0,
		@ReferenceID VARCHAR(100),
		@Amount MONEY,
		@AuthCode CHAR(6),
		@PaymentDate DATETIME,
		@PaymentType TINYINT,
		@PerformDupeCheck BIT = 0,
		@RecordAdded BIT OUT
	)
AS
BEGIN
	SET @RecordAdded = 0
	SET NOCOUNT OFF;
	SET XACT_ABORT ON;
    SET DEADLOCK_PRIORITY HIGH;
	BEGIN TRANSACTION;

	DECLARE @pid INT = NULL;

	IF (@PerformDupeCheck = 1)
	BEGIN
		SET @pid =
		(
			SELECT	TOP 1 PaymentID
			FROM	[payment].[TBL_OrderPayments] WITH (TABLOCKX)
			WHERE	OrderNumber = @OrderNumber
			AND		ReferenceID = @ReferenceID
			AND		Amount = @Amount
		);
	END

	IF (@pid IS NULL)
	BEGIN
		INSERT INTO [payment].[TBL_OrderPayments]
			(
				[OrderNumber],
				[OrderExt],
				[ReferenceID],
				[Amount],
				[AuthCode],
				[PaymentDate],
				[OrderPaymentType]
			)
		VALUES
			(@OrderNumber,
			@OrderExt,
			@ReferenceID,
			@Amount,
			@AuthCode,
			@PaymentDate,
			@PaymentType
			);

        SET @RecordAdded = 1;
		SET @pid = @@IDENTITY;

	END;

	COMMIT TRANSACTION;
	SET XACT_ABORT OFF;

	SELECT @pid;
END;
-- Amin 3/20/2014 New Proc
-- Mehrak 12/30/2014 Adding OrderExt for Payment Per Ext Project
-- Brad O 11/3/2016 Added dupe check
-- Brad O 2/24/2017 Added TABLOCKX for exclusive tape lock for dupe check
-- 4/6/2021 Richard Si remove extra columns
GO

--- [payment].[TBL_PaymentLogs]

USE [iHerb_Reporting_Payment]
GO

IF (EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE SPECIFIC_SCHEMA = 'payment' AND  SPECIFIC_NAME = 'spx_Payment_AddTransactionLogSOA'))
	DROP PROCEDURE [payment].[spx_Payment_AddTransactionLogSOA]
GO

IF (EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'payment' AND  TABLE_NAME = 'TBL_PaymentLogs'))
	DROP TABLE [payment].[TBL_PaymentLogs]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [payment].[TBL_PaymentLogs](
	[TxID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CustomerID] [uniqueidentifier] NOT NULL,
	[OrderNumber] [int] NULL,
	[AccountNo] [char](4) NULL,
	[AccountType] [varchar](30) NULL,
	[Amount] [money] NOT NULL,
	[CurrencyCode] [char](3) NOT NULL,
	[AuthCode] [char](6) NULL,
	[ReferenceID] [varchar](100) NULL,
	[TxType] [char](1) NOT NULL,
	[TxDate] [datetime] NOT NULL,
	[ResponseMessage] [nvarchar](max) NOT NULL,
	[ResponseCode] [int] NOT NULL,
	[Approved] [bit] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[Username] [varchar](100) NOT NULL,
	[Voided] [bit] NOT NULL,
	[Captured] [bit] NOT NULL,
	[CountryCode] [char](3) NULL,
	[BankName] [varchar](100) NULL,
	[AdditionalInfo] [nvarchar](max) NULL,
	[PaymentEngine] [tinyint] NULL,
	[ReconciliationID] [varchar](16) NULL,
	[OrderExt] [tinyint] NULL,
	[DetailCardIndicator] [char](2) NULL,
	[ProductID] [varchar](3) NULL,
	[CardClass] [char](1) NULL,
	[ProductSubType] [varchar](2) NULL,
	[CountryCodeWF] [char](3) NULL,
 CONSTRAINT [PK_payment_TBL_PaymentLogs] PRIMARY KEY CLUSTERED 
(
	[TxID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 75) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_TBL_PaymentLogs_TxDate] ON [payment].[TBL_PaymentLogs]
(
	[TxDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX [IX_TBL_PaymentLogs_OrderNumber] ON [payment].[TBL_PaymentLogs]
(
	[OrderNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE PROCEDURE [payment].[spx_Payment_AddTransactionLogSOA]
    (
        @OrderNumber         INT,
        @OrderExt            TINYINT      = NULL,
        @CustomerID          UNIQUEIDENTIFIER,
        @AccountNo           CHAR(4),
        @AccountType         VARCHAR(30),
        @Amount              MONEY,
        @CurrencyCode        CHAR(3),
        @AuthCode            CHAR(6),
        @ReferenceID         VARCHAR(100),
        @TxType              CHAR(1),
        @TxDate              DATETIME,
        @ResponseMessage     NVARCHAR(MAX),
        @ResponseCode        INT,
        @Approved            BIT,
        @Comment             NVARCHAR(MAX),
        @Username            VARCHAR(100),
        @Voided              BIT,
        @Captured            BIT,
        @CountryCode         CHAR(2),
        @BankName            VARCHAR(100),
        @AdditionalInfo      NVARCHAR(MAX),
        @PaymentEngine       TINYINT,
        @ReconciliationID    VARCHAR(16)  = NULL,
        @DetailCardIndicator CHAR(2)      = NULL,
        @ProductID           VARCHAR(3)   = NULL,
        @CardClass           CHAR(1)      = NULL,
        @ProductSubType      VARCHAR(2)   = NULL
    )
AS
    BEGIN
        SET DEADLOCK_PRIORITY HIGH;
        INSERT INTO [payment].[TBL_PaymentLogs]
            (
                [OrderNumber],
                [OrderExt],
                [CustomerID],
                [AccountNo],
                [AccountType],
                [Amount],
                [CurrencyCode],
                [AuthCode],
                [ReferenceID],
                [TxType],
                [TxDate],
                [ResponseMessage],
                [ResponseCode],
                [Approved],
                [Comment],
                [Username],
                [Voided],
                [Captured],
                [CountryCode],
                [BankName],
                [AdditionalInfo],
                [PaymentEngine],
                [ReconciliationID],
                [DetailCardIndicator],
                [ProductID],
                [CardClass],
                [ProductSubType]
            )
        VALUES
            (
                @OrderNumber, @OrderExt, @CustomerID, @AccountNo, @AccountType, @Amount, @CurrencyCode,
                @AuthCode, @ReferenceID, @TxType, @TxDate, @ResponseMessage, @ResponseCode, @Approved, @Comment,
                @Username, @Voided, @Captured, @CountryCode, @BankName, @AdditionalInfo,
                @PaymentEngine, @ReconciliationID, @DetailCardIndicator, @ProductID, @CardClass, @ProductSubType
            );

    END;
-- 8/29/2013 Amin New Proc
-- 10/15/2014 Brad O. Added TokenEngine, TokenID, and ReconciliationID
-- 7/9/2015 Brad O. Added DetailCardIndicator, ProductID, CardClass, and ProductSubType
-- 7/22/2015 Amin Add [CountryCodeWF]
-- 9/10/2015 Brad O. Fix for copying BIN information
-- 9/23/2015 Jeff Modified If Clause per BT's request
-- 2/15/2016 Brad O. Add SingleMerchantID field
-- 03/07/2016 Brad O. Add AltReferenceID field for PayPal use
-- 07/15/2016 Jeff Change AccountType variable from 10 to 30, modify insert to insert into accounttype instead of AccountType
-- 04/10/2017 Mehrak Add BillingCountryCode for Adyen Payment Gateway
-- 9/22/2018 Amin OPTION (OPTIMIZE FOR UNKNOWN, QUERYTRACEON 4136);
-- 12/17/2019 Azret Add CreditCardAliasId
-- Mani N. 1/17/2020 Removed AccountHashValue
-- 4/6/2021 Richard Si remove extra columns
GO

