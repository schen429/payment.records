USE [iHerb_Reporting_Payment]
GO

-- note: test env ran this script on 5/10/2021. Side to side data check for after 5/10/2021.

DECLARE @startDate DATETIME
SET @startDate = '2020-05-01'

--DELETE [iHerb_Reporting_Payment].[payment].[TBL_OrderPayments]
--FROM [iHerb_Reporting_Payment].[payment].[TBL_OrderPayments]
--WHERE PaymentDate > @startDate

-- TBL_OrderPayments
SELECT [PaymentID]
      ,[OrderNumber]
      ,[OrderExt]
      ,[ReferenceID]
      ,[Amount]
      ,[AuthCode]
      ,[PaymentDate]
      ,[Voided]
      ,[Reason]
      ,[Username]
      ,[OrderPaymentType]
INTO #tmpPayments
FROM [iHerb_Secure].[payment].[TBL_OrderPayments]
WHERE PaymentDate > @startDate

DELETE #tmpPayments
FROM #tmpPayments TP
JOIN [iHerb_Reporting_Payment].[payment].[TBL_OrderPayments] P
	ON TP.OrderNumber = P.OrderNumber AND TP.OrderExt = TP.OrderExt AND TP.PaymentDate = P.PaymentDate


INSERT INTO [iHerb_Reporting_Payment].[payment].[TBL_OrderPayments]
           ([OrderNumber]
           ,[OrderExt]
           ,[ReferenceID]
           ,[Amount]
           ,[AuthCode]
           ,[PaymentDate]
           ,[OrderPaymentType])
	SELECT
			[OrderNumber]
           ,[OrderExt]
           ,[ReferenceID]
           ,[Amount]
           ,[AuthCode]
           ,[PaymentDate]
           ,[OrderPaymentType]
	FROM #tmpPayments

DROP TABLE #tmpPayments

--DECLARE @startDate DATETIME
--SET @startDate = '2020-05-01'

--DELETE [iHerb_Reporting_Payment].[payment].[TBL_PaymentLogs]
--FROM [iHerb_Reporting_Payment].[payment].[TBL_PaymentLogs]
--WHERE TxDate > @startDate

-- TBL_PaymentLogs
SELECT [TxID]
      ,[CustomerID]
      ,[OrderNumber]
      ,[AccountNo]
      ,[AccountName]
      ,[AccountType]
      ,[Amount]
      ,[CurrencyCode]
      ,[AuthCode]
      ,[ReferenceID]
      ,[TxType]
      ,[TxDate]
      ,[ResponseMessage]
      ,[ResponseCode]
      ,[Approved]
      ,[Comment]
      ,[Username]
      ,[PaymentID]
      ,[Voided]
      ,[Captured]
      ,[CVCMatch]
      ,[AddressMatch]
      ,[ZipCodeMatch]
      ,[International]
      ,[RequestToken]
      ,[ExpirationYear]
      ,[ExpirationMonth]
      ,[CountryCode]
      ,[BankName]
      ,[AdditionalInfo]
      ,[PaymentEngine]
      ,[TokenEngine]
      ,[TokenID]
      ,[ReconciliationID]
      ,[OrderExt]
      ,[DetailCardIndicator]
      ,[ProductID]
      ,[CardClass]
      ,[ProductSubType]
      ,[CountryCodeWF]
      ,[AltReferenceID]
      ,[BillingCountryCode]
      ,[SingleMerchantID]
      ,[AccountTypeNew]
      ,[CreditCardAliasId]
INTO #tmpLogs
FROM [iHerb_Secure].[payment].[TBL_PaymentLogs]
WHERE TxDate > @startDate

DELETE #tmpLogs
FROM #tmpLogs TL
JOIN [iHerb_Reporting_Payment].[payment].[TBL_PaymentLogs] L
	ON TL.CustomerID = L.CustomerID AND TL.TxType = L.TxType AND TL.TxDate = L.TxDate

INSERT INTO [iHerb_Reporting_Payment].[payment].[TBL_PaymentLogs]
           ([CustomerID]
           ,[OrderNumber]
           ,[AccountNo]
           ,[AccountType]
           ,[Amount]
           ,[CurrencyCode]
           ,[AuthCode]
           ,[ReferenceID]
           ,[TxType]
           ,[TxDate]
           ,[ResponseMessage]
           ,[ResponseCode]
           ,[Approved]
           ,[Comment]
           ,[Username]
           ,[Voided]
           ,[Captured]
           ,[CountryCode]
           ,[BankName]
           ,[AdditionalInfo]
           ,[PaymentEngine]
           ,[ReconciliationID]
           ,[OrderExt]
           ,[DetailCardIndicator]
           ,[ProductID]
           ,[CardClass]
           ,[ProductSubType]
           ,[CountryCodeWF])
	SELECT 
			[CustomerID]
           ,[OrderNumber]
           ,[AccountNo]
           ,[AccountType]
           ,[Amount]
           ,[CurrencyCode]
           ,[AuthCode]
           ,[ReferenceID]
           ,[TxType]
           ,[TxDate]
           ,[ResponseMessage]
           ,[ResponseCode]
           ,[Approved]
           ,[Comment]
           ,[Username]
           ,[Voided]
           ,[Captured]
           ,[CountryCode]
           ,[BankName]
           ,[AdditionalInfo]
           ,[PaymentEngine]
           ,[ReconciliationID]
           ,[OrderExt]
           ,[DetailCardIndicator]
           ,[ProductID]
           ,[CardClass]
           ,[ProductSubType]
           ,[CountryCodeWF]
	FROM #tmpLogs

DROP TABLE #tmpLogs
GO
