USE [iHerb_Reporting_Payment]
GO

--DROP PROCEDURE [payment].[spx_Payment_GetTransactionBillingAddress]
--GO
--DROP PROCEDURE [payment].[spx_Payment_GetOrderBillingAddress]
--GO
--DROP PROCEDURE [payment].[spx_Payment_UpsertPaymentProperties]
--GO
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[payment].[TBL_PaymentProperties]') AND type in (N'U'))
--DROP TABLE [payment].[TBL_PaymentProperties]
--GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [payment].[TBL_PaymentProperties](
	[TransactionId] [uniqueidentifier] NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[OrderNumber] [int] NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[TransactionStatus] [varchar](50) NOT NULL,
	[BillingAddress] [nvarchar](max) NULL,
	[LastUpdatedAt] [datetime] NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NULL,
	[Deleted] bit NOT NULL
CONSTRAINT [PK_TBL_PaymentProperties] PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [payment].[TBL_PaymentProperties] ADD  CONSTRAINT [DF_TBL_PaymentProperties_Deleted_0]  DEFAULT ((0)) FOR [Deleted]
GO

CREATE NONCLUSTERED INDEX [IX_TBL_PaymentProperties_OrderNumber] ON [payment].[TBL_PaymentProperties]
(
	[OrderNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE OR ALTER   PROCEDURE [payment].[spx_Payment_GetOrderBillingAddress]
    @orderNumber [int],
	@includeDeleted bit = 0
AS
BEGIN
    SET NOCOUNT ON;

	SELECT [TransactionId]
		  ,[CustomerId]
		  ,[OrderNumber]
		  ,[TransactionDate]
		  ,[TransactionStatus]
		  ,[BillingAddress]
		  ,[LastUpdatedAt]
		  ,[LastUpdatedBy]
	FROM [payment].[TBL_PaymentProperties]
	WHERE ([OrderNumber] = @orderNumber)
		AND ([Deleted] = 0 OR @includeDeleted = 1)
	ORDER BY [TransactionDate]
END;

-- Richard Si - create SP - 10/18/2021
GO

CREATE OR ALTER   PROCEDURE [payment].[spx_Payment_GetTransactionBillingAddress]
    @transactionId [uniqueidentifier],
	@includeDeleted bit = 0
AS
BEGIN
    SET NOCOUNT ON;

	SELECT [TransactionId]
		  ,[CustomerId]
		  ,[OrderNumber]
		  ,[TransactionDate]
		  ,[TransactionStatus]
		  ,[BillingAddress]
		  ,[LastUpdatedAt]
		  ,[LastUpdatedBy]
	FROM [payment].[TBL_PaymentProperties]
	WHERE ([TransactionId] = @transactionId)
		AND ([Deleted] = 0 OR @includeDeleted = 1)
END;

-- Richard Si - create SP - 10/18/2021
GO

CREATE OR ALTER   PROCEDURE [payment].[spx_Payment_UpsertPaymentProperties]
	@transactionId [uniqueidentifier],
	@customerId [uniqueidentifier],
    @orderNumber [int],
	@transactionDate [datetime],
	@transactionStatus [varchar](50),
	@billingAddress [nvarchar](max),
	@lastUpdatedBy [nvarchar](50)
AS
BEGIN
	MERGE INTO [payment].[TBL_PaymentProperties] AS Target  
	USING (VALUES (@transactionId)) AS Source (TransactionId)  
	ON Target.[TransactionId] = Source.TransactionId
	WHEN MATCHED THEN  
	UPDATE SET 
		[TransactionStatus] = COALESCE(@transactionStatus, Target.TransactionStatus),
		[BillingAddress] = COALESCE(@billingAddress, Target.BillingAddress),
		[LastUpdatedAt] = GETDATE(),
		[LastUpdatedBy] = @lastUpdatedBy
	WHEN NOT MATCHED BY TARGET THEN  
	INSERT ([TransactionId]
           ,[CustomerId]
		   ,[OrderNumber]
           ,[TransactionDate]
		   ,[TransactionStatus]
           ,[BillingAddress]
		   ,[LastUpdatedAt]
		   ,[LastUpdatedBy])
		VALUES
           (@transactionId
           ,@customerId
		   ,@orderNumber
           ,@transactionDate
		   ,@transactionStatus
           ,@billingAddress
		   ,GETDATE()
		   ,@lastUpdatedBy);
END;

-- Richard Si - create SP - 10/18/2021
GO

