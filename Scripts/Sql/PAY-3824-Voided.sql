USE [iHerb_Reporting_Payment]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE payment.TBL_OrderPayments ADD
	Voided bit NULL
GO

CREATE OR ALTER   PROCEDURE [payment].[spx_Payment_AddOrderPayment2]
	(
		@OrderNumber INT,
		@OrderExt TINYINT = 0,
		@ReferenceID VARCHAR(100),
		@Amount MONEY,
		@AuthCode CHAR(6),
		@PaymentDate DATETIME,
		@PaymentType TINYINT,
		@Voided BIT
	)
AS
BEGIN
    SET DEADLOCK_PRIORITY HIGH;

	DECLARE @pid INT = NULL;

	INSERT INTO [payment].[TBL_OrderPayments]
		(
			[OrderNumber],
			[OrderExt],
			[ReferenceID],
			[Amount],
			[AuthCode],
			[PaymentDate],
			[OrderPaymentType],
			[Voided]
		)
	VALUES
		(@OrderNumber,
		@OrderExt,
		@ReferenceID,
		@Amount,
		@AuthCode,
		@PaymentDate,
		@PaymentType,
		@Voided
		);

	SET @pid = @@IDENTITY;

	SELECT @pid;
END;
-- Amin 3/20/2014 New Proc
-- Mehrak 12/30/2014 Adding OrderExt for Payment Per Ext Project
-- Brad O 11/3/2016 Added dupe check
-- Brad O 2/24/2017 Added TABLOCKX for exclusive tape lock for dupe check
-- 4/6/2021 Richard Si remove extra columns (DBA-14074)
-- 7/26/2021 Richard Si remove @PerformDupeCheck
-- 10/20/2021 Richard Si Add Voided column
GO

