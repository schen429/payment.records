USE [iHerb_Reporting_Payment]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [payment].[spx_get_transactions_by_user]
    @userId UNIQUEIDENTIFIER,
	@paymentType [varchar](30) = NULL,
	@paymentSubType [varchar](30) = NULL,
	@beginTxDate [datetime] = NULL,
	@endTxDate [datetime] = NULL
AS
BEGIN
    SET NOCOUNT ON;

	SELECT [TxID]
		  ,[CustomerID]
		  ,[OrderNumber]
		  ,[AccountNo]
		  ,[AccountType]
		  ,[Amount]
		  ,[CurrencyCode]
		  ,[AuthCode]
		  ,[ReferenceID]
		  ,[TxType]
		  ,[TxDate]
		  ,[ResponseMessage]
		  ,[ResponseCode]
		  ,[Approved]
		  ,[Comment]
		  ,[Username]
		  ,[Voided]
		  ,[Captured]
		  ,[CountryCode]
		  ,[BankName]
		  ,[AdditionalInfo]
		  ,[PaymentEngine]
		  ,[ReconciliationID]
		  ,[OrderExt]
		  ,[DetailCardIndicator]
		  ,[ProductID]
		  ,[CardClass]
		  ,[ProductSubType]
		  ,[CountryCodeWF]
	FROM [payment].[TBL_PaymentLogs]
	WHERE [CustomerID] = @userId
		AND ((@paymentType IS NULL AND @paymentSubType IS NULL) OR @paymentType = [AccountType] OR @paymentSubType = [AccountType])
		AND (@beginTxDate IS NULL OR @beginTxDate <= [TxDate])
		AND (@endTxDate IS NULL OR @endTxDate >= [TxDate])
	ORDER BY [TxDate] DESC
END;

-- Richard Si - create SP - 08/17/2021
GO

SET ANSI_PADDING ON
GO

CREATE NONCLUSTERED INDEX [IX_TBL_PaymentLogs_CustomerID_AccountType_TxDate] ON [payment].[TBL_PaymentLogs]
(
	[CustomerID] ASC,
	[AccountType] ASC,
	[TxDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
