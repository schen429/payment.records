﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using System;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace iHerb.Payment.Records.Data
{
    public class XApplicationAuthenticationOptions : AuthenticationSchemeOptions
    {
        public IConfiguration configuration { get; set; }
    }

    public class XApplicationAuthenticationHandler : AuthenticationHandler<XApplicationAuthenticationOptions>
    {
        private const string _XApplicationHeader = "x-application-x";
        private const string _XApplicationConfigurationList = "x-application-x-list";

        public XApplicationAuthenticationHandler(IOptionsMonitor<XApplicationAuthenticationOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey(_XApplicationHeader))
            {
                //Authorization header not in request
                return AuthenticateResult.NoResult();
            }
            if (!Request.Headers.TryGetValue(_XApplicationHeader, out StringValues headerValue))
            {
                Logger.LogWarning("[XApplicationAuthenticationHandler][HandleAuthenticateAsync] User unauthorized through x-application-x header with empty token.");
                return AuthenticateResult.NoResult();
            }

            var whitelistedTokens = Options.configuration[_XApplicationConfigurationList];
            if (string.IsNullOrWhiteSpace(whitelistedTokens))
            {
                Logger.LogWarning("[XApplicationAuthenticationHandler][HandleAuthenticateAsync] User unauthorized through x-application-x header with token.");
                return AuthenticateResult.NoResult();
            }

            var xApplicationXToken = headerValue.ToString();
            var list = whitelistedTokens.Split(',');
            if (!list.Contains(xApplicationXToken))
            {
                Logger.LogWarning($"[XApplicationAuthenticationHandler][HandleAuthenticateAsync] User unauthorized through x-application-x header with token {headerValue}");
                return AuthenticateResult.NoResult();
            }

            // Only log the substring rather than the whole token so we can at least identify which token it is.
            Logger.LogInformation($"[XApplicationAuthenticationHandler][HandleAuthenticateAsync] User validated through x-application-x header with token {xApplicationXToken.Substring(0, Math.Min(4, xApplicationXToken.Length))}");

            var claims = new[] { new Claim(ClaimTypes.Name, "internal") };
            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);

            return AuthenticateResult.Success(ticket);
        }
    }
}
