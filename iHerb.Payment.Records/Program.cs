﻿using iHerb.Platform.Logging.Extensions;
using iHerb.Platform.Metrics.Extensions;
using iHerb.Platform.WebApi.ApiConventions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using System;

[assembly: ApiConventionType(conventionType: typeof(iHerbApiConventions))]

namespace iHerb.Payment.Records
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                CreateHostBuilder(args).Build().RegisterLifetimes().Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseiHerbLogging();
                webBuilder.UseStartup<Startup>();
            });
    }
}
