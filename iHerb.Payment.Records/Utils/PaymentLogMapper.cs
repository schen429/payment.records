﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Models.Consumed.Tokenization;
using iHerb.Payment.Records.Models.Messages;
using iHerb.Payment.Records.Models.PaymentLogs;

namespace iHerb.Payment.Records.Utils
{
    public class PaymentLogMapper
    {
        private readonly ILogger<PaymentLogMapper> _logger;

        public PaymentLogMapper(ILogger<PaymentLogMapper> logger)
        {
            _logger = logger;
        }

        public PaymentLog GetTokenizationPaymentLogSuccess(TokenizationMessage tokenizationMessage)
        {
            TokenizationSuccessExternalResponse tokenizationExternalResponse;
            TokenizationExternalRequest tokenizationExternalRequest;
            CreditCardBinData creditCardBinData = new CreditCardBinData();

            try
            {
                tokenizationExternalRequest = JsonConvert.DeserializeObject<TokenizationExternalRequest>(tokenizationMessage.Body.ExternalRequest);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, $"[PaymentLogMapper][GetTokenizationPaymentLogSuccess] Unable to deserialize TokenizationExternalRequest TokenizationMessage from the queue: {exception}");
                throw;
            }

            try
            {
                tokenizationExternalResponse = JsonConvert.DeserializeObject<TokenizationSuccessExternalResponse>(tokenizationMessage.Body.ExternalResponse);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, $"[PaymentLogMapper][GetTokenizationPaymentLogSuccess] Unable to deserialize TokenizationSuccessExternalResponse TokenizationMessage from the queue: {exception}");
                throw;
            }

            try
            {
                if (!string.IsNullOrEmpty(tokenizationMessage.Body.CreditCardBinData))
                {
                    creditCardBinData = JsonConvert.DeserializeObject<CreditCardBinData>(tokenizationMessage.Body.CreditCardBinData);
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, $"[PaymentLogMapper][GetTokenizationPaymentLogSuccessS] Unable to deserialize CreditCardBinData TokenizationMessage from the queue: {exception}");
                throw;
            }

            PaymentLog paymentLog = new CreditCardTokenizationPaymentLog(tokenizationExternalRequest, tokenizationExternalResponse, creditCardBinData);

            return paymentLog;
        }

        public PaymentLog GetTokenizationPaymentLogFailed(TokenizationMessage tokenizationMessage)
        {
            TokenizationErrorExternalResponse tokenizationExternalResponse;
            TokenizationExternalRequest tokenizationExternalRequest;
            CreditCardBinData creditCardBinData = new CreditCardBinData();

            try
            {
                tokenizationExternalRequest = JsonConvert.DeserializeObject<TokenizationExternalRequest>(tokenizationMessage.Body.ExternalRequest);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, $"[PaymentLogMapper][GetTokenizationPaymentLogFailed] Unable to deserialize TokenizationExternalRequest TokenizationMessage from the queue: {exception}");
                throw;
            }

            try
            {
                tokenizationExternalResponse = JsonConvert.DeserializeObject<TokenizationErrorExternalResponse>(tokenizationMessage.Body.ExternalResponse);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, $"[PaymentLogMapper][GetTokenizationPaymentLogFailed] Unable to deserialize TokenizationErrorExternalResponse TokenizationMessage from the queue: {exception}");
                throw;
            }

            try
            {
                if (!string.IsNullOrEmpty(tokenizationMessage.Body.CreditCardBinData))
                {
                    creditCardBinData = JsonConvert.DeserializeObject<CreditCardBinData>(tokenizationMessage.Body.CreditCardBinData);
                }

            }
            catch (Exception exception)
            {
                _logger.LogError(exception, $"[PaymentLogMapper][GetTokenizationPaymentLogFailed] Unable to deserialize CreditCardBinData TokenizationMessage from the queue: {exception}");
                throw;
            }

            PaymentLog paymentLog = new CreditCardTokenizationPaymentLog(tokenizationExternalRequest, tokenizationExternalResponse, creditCardBinData);

            return paymentLog;
        }

        public PaymentLog GetAuthorizationPaymentLogSuccess(AuthorizationMessage authorizationMessage)
        {
            _logger.LogInformation($"[PaymentLogMapper][GetAuthorizationPaymentLogSuccess] Creating PaymentLog for orderNumber {authorizationMessage.Body.ReferenceId} UserId: {authorizationMessage.Body.UserId} ExternalTokenId: {authorizationMessage.Body.ExternalPaymentAccountId}");

            if (authorizationMessage.Body == null)
            {
                throw new ArgumentNullException(nameof(authorizationMessage.Body));
            }

            if (string.IsNullOrWhiteSpace(authorizationMessage.Body.Last4))
            {
                _logger.LogWarning($"[PaymentLogMapper][GetAuthorizationPaymentLogSuccess] Last4 was null/whitespace for orderNumber {authorizationMessage.Body.ReferenceId} UserId: {authorizationMessage.Body.UserId} ExternalTokenId: {authorizationMessage.Body.ExternalPaymentAccountId}");
            }
            if (string.IsNullOrWhiteSpace(authorizationMessage.Body.AccountHolderName))
            {
                _logger.LogWarning($"[PaymentLogMapper][GetAuthorizationPaymentLogSuccess] AccountHolderName was null/whitespace for orderNumber {authorizationMessage.Body.ReferenceId} UserId: {authorizationMessage.Body.UserId} ExternalTokenId: {authorizationMessage.Body.ExternalPaymentAccountId}");
            }
            if (string.IsNullOrWhiteSpace(authorizationMessage.Body.Country))
            {
                _logger.LogWarning($"[PaymentLogMapper][GetAuthorizationPaymentLogSuccess] Country was null/whitespace for orderNumber {authorizationMessage.Body.ReferenceId} UserId: {authorizationMessage.Body.UserId} ExternalTokenId: {authorizationMessage.Body.ExternalPaymentAccountId}");
            }
            if (string.IsNullOrWhiteSpace(authorizationMessage.Body.ExpirationMonth))
            {
                _logger.LogWarning($"[PaymentLogMapper][GetAuthorizationPaymentLogSuccess] ExpirationMonth was null/whitespace for orderNumber {authorizationMessage.Body.ReferenceId} UserId: {authorizationMessage.Body.UserId} ExternalTokenId: {authorizationMessage.Body.ExternalPaymentAccountId}");
            }
            if (string.IsNullOrWhiteSpace(authorizationMessage.Body.ExpirationYear))
            {
                _logger.LogWarning($"[PaymentLogMapper][GetAuthorizationPaymentLogSuccess] ExpirationYear was null/whitespace for orderNumber {authorizationMessage.Body.ReferenceId} UserId: {authorizationMessage.Body.UserId} ExternalTokenId: {authorizationMessage.Body.ExternalPaymentAccountId}");
            }
            if (authorizationMessage.Body.PaymentSubType == PaymentSubType.NONE)
            {
                _logger.LogWarning($"[PaymentLogMapper][GetAuthorizationPaymentLogSuccess] PaymentSubType was NONE for orderNumber {authorizationMessage.Body.ReferenceId} UserId: {authorizationMessage.Body.UserId} ExternalTokenId: {authorizationMessage.Body.ExternalPaymentAccountId}");
            }

            PaymentLog paymentLog = new CreditCardAuthorizationPaymentLog(authorizationMessage.Body);

            return paymentLog;
        }


        public PaymentLog GetAuthorizationRequestPaymentLog(AuthorizationMessage authorizationMessage)
        {
            _logger.LogInformation($"[PaymentLogMapper][GetAuthorizationRequestPaymentLog] Creating PaymentLog for orderNumber {authorizationMessage.Body.ReferenceId} UserId: {authorizationMessage.Body.UserId}");

            if (authorizationMessage.Body == null)
            {
                throw new ArgumentNullException(nameof(authorizationMessage.Body));
            }

            PaymentLog paymentLog = new AlternativePaymentAuthorizationRequestPaymentLog(authorizationMessage.Body);

            return paymentLog;
        }
    }

}
