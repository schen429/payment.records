﻿namespace iHerb.Payment.Records.Utils
{
    public static class StringUtil
    {
        public static int? ToInt(this string s)
        {
            if (int.TryParse(s, out int n))
            {
                return n;
            }

            return null;
        }

        public static int ToIntOrDefault(this string s, int defaultValue = 0)
        {
            if (int.TryParse(s, out int n))
            {
                return n;
            }

            return defaultValue;
        }
    }
}
