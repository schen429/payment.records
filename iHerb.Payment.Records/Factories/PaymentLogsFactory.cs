﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models.Messages;
using iHerb.Payment.Records.Models.PaymentLogs;
using iHerb.Payment.Records.Utils;

namespace iHerb.Payment.Records.Factories
{
    public class PaymentLogsFactory : IPaymentLogsFactory
    {
        private readonly PaymentLogMapper _paymentLogMapper;
        private readonly ILogger<PaymentLogsFactory> _logger;

        public PaymentLogsFactory(ILogger<PaymentLogsFactory> logger, PaymentLogMapper paymentLogMapper)
        {
            _logger = logger;
            _paymentLogMapper = paymentLogMapper;
        }

        public PaymentLog GetPaymentLog(MessageSource source, MessageSubject subject, string body)
        {
            switch (source)
            {
                case MessageSource.ACCOUNT:

                    TokenizationMessage tokenizationMessage;
                    try
                    {
                        tokenizationMessage = JsonConvert.DeserializeObject<TokenizationMessage>(body);
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, $"[PaymentLogsFactory][GetPaymentLog] Unable to deserialize TokenizationMessage: {exception}");
                        throw;
                    }

                    switch (subject)
                    {
                        case MessageSubject.AUTHORISED:
                        case MessageSubject.REFUSED:
                            return _paymentLogMapper.GetTokenizationPaymentLogSuccess(tokenizationMessage);                             

                        case MessageSubject.FAILED:
                            return _paymentLogMapper.GetTokenizationPaymentLogFailed(tokenizationMessage);

                        default:
                        {
                            _logger.LogError($"[PaymentLogsFactory][GetPaymentLog] Unsupported Payment Mesage Subject {subject}, source = {source}");
                            return null;
                        }
                    }

                case MessageSource.TRANSACTION:
                {
                    switch (subject)
                    {
                        case MessageSubject.AUTHORISED:
                        case MessageSubject.FAILED:
                        case MessageSubject.REFUSED:
                        case MessageSubject.ADDITIONAL_ACTION_REQUIRED:
                                {
                                AuthorizationMessage authorizationMessage;
                                try
                                {
                                    authorizationMessage = JsonConvert.DeserializeObject<AuthorizationMessage>(body);
                                }
                                catch (Exception exception)
                                {
                                    _logger.LogError(exception, $"[PaymentLogsFactory][GetPaymentLog] Unable to deserialize AuthorizationMessage: {exception}");
                                        throw;
                                }

                                switch (authorizationMessage.Body.ActionType)
                                {
                                    case ActionType.ADYEN_REDIRECT:
                                    case ActionType.SHOW_AUTH_UI:
                                        return _paymentLogMapper.GetAuthorizationRequestPaymentLog(authorizationMessage);
                                    default:
                                        return _paymentLogMapper.GetAuthorizationPaymentLogSuccess(authorizationMessage);
                                }
                            }
                        default:
                            {
                                _logger.LogError($"[PaymentLogsFactory][GetPaymentLog] Unsupported Payment Mesage Subject {subject}, source = {source}");
                                return null;
                            }
                    }
                }
                default:
                {
                    _logger.LogError($"[PaymentLogsFactory][GetPaymentLog] Unsupported Payment Mesage Source {source}");
                    return null;
                }
            }
        }
    }
}
