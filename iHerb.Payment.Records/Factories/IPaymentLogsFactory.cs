﻿using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models.PaymentLogs;

namespace iHerb.Payment.Records.Factories
{
    public interface IPaymentLogsFactory
    {
        PaymentLog GetPaymentLog(MessageSource source, MessageSubject subject, string body);
    }
}
