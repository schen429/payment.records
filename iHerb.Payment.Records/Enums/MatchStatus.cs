﻿using System.Runtime.Serialization;

namespace iHerb.Payment.Records.Enums
{
    public enum MatchStatusPayment : byte
    {
        [EnumMember]
        Unknown = (byte)'U',
        [EnumMember]
        Matched = (byte)'Y',
        [EnumMember]
        NotMatched = (byte)'N',
        [EnumMember]
        NotSupported = (byte)'X',
        [EnumMember]
        Error = (byte)'E',
        [EnumMember]
        Suspicious = (byte)'S'
    }
}
