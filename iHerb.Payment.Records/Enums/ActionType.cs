﻿namespace iHerb.Payment.Records.Enums
{
    public enum ActionType
    {
        NONE,
        ADYEN_FINGERPRINT,
        ADYEN_CHALLENGE,
        ADYEN_REDIRECT,
        SELECT_NEW_PAYMENT_ACCOUNT,
        CREATE_TRANSACTION,
        REDIRECT,
        INPUT_ONE_TIME_PASSWORD,
        APPLEPAY_VERIFICATION,
        COLLECT_ADDITIONAL_INFO_DOKUWALLET,
        CREATE_NONCE,
        SHOW_AUTH_UI
    }
}
