﻿namespace iHerb.Payment.Records.Enums
{
    public class ResultCodes
    {
        public const string Authorized = "4001";
        public const string Pending = "4002";
        public const string Refused = "4003";
        public const string Cancelled = "4004";
        public const string AuthenticationFinished = "4005";
        public const string ChallengeShopper = "4006";
        public const string Error = "4007";
        public const string IdentifyShopper = "4008";
        public const string Received = "4009";
        public const string RedirectShopper = "4010";
        public const string UnknownError = "-1";
    }


    public class ResultCodeMessages
    {
        public const string Authorized = "Authorised";
        public const string Refused = "Refused";
    }

    public class UserName
    {
        public const string PaymentSecured = "PaymentSecured";
        public const string PaymentApi = "PaymentApi";
    }
}
