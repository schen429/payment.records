﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace iHerb.Payment.Records.Enums
{
    [JsonConverter(typeof(PaymentTypeEnumConverter))]
    public enum PaymentType
    {
        None = 0,
        AliPay = 3,
        PayPal = 4,
        CreditCard = 5,
        KCP = 6,
        Adyen = 7,
        WeChat_WeChatPay = 9,
        Yandex = 10,
        NoPayment = 16,
        WeChatNew = 17,
        WeChatMini = 18,
        COD = 20,
        NOPAY = 21,
        Adyen_MomoWallet = 22,
        Adyen_DokuWallet = 23,
        Adyen_Ideal = 24,
        Adyen_ApplePay = 25,
        Adyen_QiwiWallet = 26,
        Adyen_Sofort = 27,
        Adyen_GiroPay = 28,
        Adyen_Klarna = 29,
        Adyen_KonbiniATM = 30,
        Adyen_KonbiniStores = 31,
        Adyen_Boleto = 32,
        Rewards = 33,
        AliPay_TrueMoney = 34,
        AliPay_HK = 35,
        AliPay_CN = 36,
        AliPay_TNG = 37,
        AliPay_Dana = 39,
        KakaoPay = 40,
        AliPay_Connect_Wallet = 41,
        AliPay_Easypaisa = 42,
        AliPay_BKash = 43,
        AliPay_PayTM = 44,
        Braintree_PayPal = 46,
        Yandex_Money = 47,
        Yandex_MIR = 48,
        Adyen_Interac = 49,
        Adyen_PayCo = 50,
        Adyen_KoreanOnlineBanking = 51,
        Adyen_Bancontact_Card = 52,
        NihaoPay_UnionPay = 54,
        Marketplace = 55,
        AliPayCashier_AliPay_CN = 56,
        AliPayCashier_Dana = 57,
        AliPayCashier_Touch_N_Go = 58,
        AliPayCashier_GCash = 59,
        AliPayCashier_TrueMoney = 60,
        AliPayCashier_BKash = 61,
        AliPayCashier_Easypaisa = 62,
        AliPayCashier_KakaoPay = 63,
        AliPayCashier_AliPay_HK = 64
    }

    public class PaymentTypeEnumConverter : StringEnumConverter
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            try
            {
                return base.ReadJson(reader, objectType, existingValue, serializer);
            }
            catch
            {
                return null;
            }
        }
    }
}
