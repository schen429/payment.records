﻿namespace iHerb.Payment.Records.Enums
{
    public enum PaymentSubType
    {
        NONE,
        AMEX,
        BCG,
        CB,
        DCI,
        DSC,
        ELO,
        JCB,
        MC,
        UPAY,
        VISA,
        KCP_CC,
        KCP_BT,
        KCP_PC,
        DALIPAY,
        ALIPAY,
        MAESTRO,
        BCMC,
        korean_local_card
    }
}
