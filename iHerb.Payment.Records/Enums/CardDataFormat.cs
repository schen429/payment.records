﻿namespace iHerb.Payment.Records.Enums
{
    public enum CardDataFormat
    {
        RAW,
        ADYEN_ENCRYPTED
    }
}
