﻿namespace iHerb.Payment.Records.Enums
{
    public enum TransactionStatus
    {
        ERROR,
        RECEIVED,
        REFUSED,
        ADDITIONAL_ACTION_REQUIRED,
        FAILED,
        FRAUD,
        AUTHORISED,
        CAPTURED,
        CANCELLED,
        PENDING,
        REFUNDED,
        UNKNOWN,
        VOIDED
    }
}
