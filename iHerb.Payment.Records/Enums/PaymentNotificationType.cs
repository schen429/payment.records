﻿namespace iHerb.Payment.Records.Enums
{
    public enum PaymentNotificationType : byte
    {
        PaymentSuccess = 1,
        CreditCardAuthorizationFail = 2
    }
}
