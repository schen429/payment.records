﻿namespace iHerb.Payment.Records.Enums
{
    public enum TransactionType
    {
        AUTHORISATION,
        CAPTURE,
        REFUND,
        ACCOUNT,
        VOID,
        CHARGEBACK
    }
}
