﻿namespace iHerb.Payment.Records.Enums
{
    public enum ExternalPaymentType : byte
    {
        None = 0,
        WeChat = 1,
        TenPay = 2,
        AliPay = 3,
        PayPal = 4,
        CreditCard = 5,
        KCP = 6,
        Adyen = 7,
        UPAY = 8,
        WeChatMobile = 9,
        Yandex = 10, // 0x0A
        Rakuten = 15, // 0x0F
        NoPayment = 16, // 0x10
        WeChatNew = 17, // 0x11
    }
}