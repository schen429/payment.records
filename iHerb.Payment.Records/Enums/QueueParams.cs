﻿namespace iHerb.Payment.Records.Enums
{
    public enum PrefetchSize : uint
    {
        Default = 0
    }

    public enum PrefetchCount : ushort
    {
        OneMessage = 1
    }
}
