﻿using System.Runtime.Serialization;

namespace iHerb.Payment.Records.Enums
{
    public enum PaymentTxTypePayment : byte
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        Authorization = (byte)'A',
        [EnumMember]
        DelayedCapture = (byte)'D',
        [EnumMember]
        Credit = (byte)'C',
        [EnumMember]
        Sale = (byte)'S',
        [EnumMember]
        Void = (byte)'V',
        [EnumMember]
        VoiceAuthorization = (byte)'F',
        [EnumMember]
        Inquiry = (byte)'I',
        [EnumMember]
        RiskInfo = (byte)'R',
        [EnumMember]
        ChargeBack = (byte)'B',
        [EnumMember]
        CaptureRequest = (byte)'Q',
        [EnumMember]
        CaptureConfirmation = (byte)'P',
        [EnumMember]
        RefundRequest = (byte)'U',
        [EnumMember]
        QueuedRequest = (byte)'?',
        [EnumMember]
        AuthorizationRequest = (byte)'Z',
        [EnumMember]
        Notification = (byte)'N',
        [EnumMember]
        CancelRequest = (byte)'X',
        [EnumMember]
        CreateSubscription = (byte)'+',
        [EnumMember]
        DeleteSubscription = (byte)'-',
        [EnumMember]
        UpdateSubscription = (byte)'E',
    }
}
