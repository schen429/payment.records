﻿namespace iHerb.Payment.Records.Enums
{
    public enum PaymentEngine
    {
        Unknown = 0,
        NoPayment = 1,
        CS = 2,
        PayPal = 3,
        WFPG = 6,
        AP = 8,
        Tenpay = 9,
        Yandex = 10,
        KCP = 11,
        Adyen = 12,
        AYPG = 13,
        WeChatMobile = 14,
        Rakuten = 15,
        iHerb = 16,
        WeChat = 17,
        WeChatMini = 18
    }
}
