﻿namespace iHerb.Payment.Records.Enums
{
    public class RepositoryConstants
    {
        private const string SchemaName = "[payment]";

        public class StoredProcedure
        {
            public const string AddOrderPayment = SchemaName + ".[spx_Payment_AddOrderPayment2]";
            public const string AddTransactionLog = SchemaName + ".[spx_Payment_AddTransactionLogSOA]";
            public const string UpsertPaymentProperties = SchemaName + ".[spx_Payment_UpsertPaymentProperties]";
        }
    }
}
