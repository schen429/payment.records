﻿namespace iHerb.Payment.Records.Enums
{
    public enum PaymentOrderStatus : byte
    {
        NoPaymentInfoAvailable = 0,
        WaitingAuthorization = 1,
        WaitingExternalPayment = 2,
        WaitingExternalPaymentAuthorization = 3,
        Cancelled = 4,
        Paid = 5,
        WaitingCapture = 6,
        WaitingCaptureInitiation = 7,
        PaymentFailed = 8,
        WaitingCancel = 9,
        WaitingCCAuthorization = 10,
        WaitingOrderDueAmount = 11,
        WaitingRewardsAppliedFix = 12,
        WaitingLCAppliedFix = 13,
        Waiting3DSCCAuthorization = 14
    }
}
