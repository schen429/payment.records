﻿namespace iHerb.Payment.Records.Enums
{
    public enum AdyenResultCode
    {
        AuthenticationFinished, //The payment has been successfully authenticated with 3D Secure 2. Returned for 3D Secure 2 authentication-only transactions.	Collect the 3D Secure 2 authentication data that you will need to authorise the payment.
        Authorised, //The payment was successfully authorised. Inform the shopper that the payment was successful.
        Cancelled, //The payment was cancelled (by either the shopper or your own system) before processing was completed. Inform the shopper that their payment was cancelled. Contact the shopper to check whether they want to continue with their order.
        ChallengeShopper, //The issuer requires further shopper interaction before the payment can be authenticated. Returned for 3D Secure 2 transactions.	Present the challenge flow to the shopper and submit the result to Adyen.
        Error, //There was an error when the payment was being processed. Inform the shopper that there was an error processing their payment. You'll receive a refusalReason in the same response, indicating the cause of the error.
        IdentifyShopper, //The issuer requires the shopper's device fingerprint before the payment can be authenticated. Returned for 3D Secure 2 transactions.	Initiate the 3D Secure 2 device fingerprinting process and submit the result to Adyen.
        Pending, //It's not possible to obtain the final status of the payment at this time. This is common for payments with an asynchronous flow, such as Boleto, iDEAL, or Klarna. (we shouldn't see this for credit cards)
        Received, //This is part of the standard payment flow for methods such as SEPA Direct Debit, where it can take some time before the final status of the payment is known. Inform the shopper that you've received their order, and are waiting for the payment to clear.
        RedirectShopper, //The shopper needs to be redirected to an external web page or app to complete the payment. Redirect the shopper to complete the payment.
        Refused //The payment was refused. Inform the shopper that their payment was refused. You'll receive a refusalReason in the same response that indicates why it was refused. We do not recommend disclosing this refusal reason to the shopper. Ask the shopper to try the payment again using a different payment method or card.
    }
}
