﻿namespace iHerb.Payment.Records.Enums
{
    public enum MessageSource
    {
        ACCOUNT,
        TRANSACTION
    }
}
