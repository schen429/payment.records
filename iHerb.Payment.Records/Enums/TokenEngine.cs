﻿using System.Runtime.Serialization;

namespace iHerb.Payment.Records.Enums
{
    public enum TokenEnginePayment : byte
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        iHerb = 1,
        [EnumMember]
        CyberSource = 2,
        [EnumMember]
        WFPG = 3,
        [EnumMember]
        External = 4, // External = No Token Engine
        [EnumMember]
        Adyen = 5,
        [EnumMember]
        Adyen3DS2= 6,
    }
}
