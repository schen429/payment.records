﻿using RabbitMQ.Client;

namespace iHerb.Payment.Records.RabbgitMQ
{
    public interface IRabbitConnectionProvider
    {
        IConnection GetConnection();
    }
}
