﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using iHerb.Payment.Records.Orchestrators;

namespace iHerb.Payment.Records.RabbgitMQ
{
    public class SecuredMessageProcessor : IMessageProcessor
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<SecuredMessageProcessor> _logger;
        private readonly IQueueMessageProcessingOrchestrator _orchestrator;

        public SecuredMessageProcessor(
            IConfiguration configuration,
            ILogger<SecuredMessageProcessor> logger,
            IQueueMessageProcessingOrchestrator orchestrator
        )
        {
            _configuration = configuration;
            _logger = logger;
            _orchestrator = orchestrator;

            ExchangeName = _configuration["Queues:Exchange"];
            QueueName = _configuration["Queues:SecuredMessageQueue"];
            RoutingKeys = new string[] { _configuration["Queues:SecuredMessageQueueRoutingKey"] };
        }

        public string Name { get; } = "SecuredMessageProcessor";
        public string ExchangeName { get; }
        public string QueueName { get; }
        public string[] RoutingKeys { get; }

        public async Task ProcessMessage(string content)
        {
            await _orchestrator.TransferPaymentMessage(content);
        }
    }
}