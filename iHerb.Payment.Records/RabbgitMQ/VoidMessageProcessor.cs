﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Threading.Tasks;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Orchestrators;

namespace iHerb.Payment.Records.RabbgitMQ
{
    public class VoidMessageProcessor : IMessageProcessor
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<VoidMessageProcessor> _logger;
        private readonly IPaymentLegacyOrchestrator _orchestrator;

        public VoidMessageProcessor(
            IConfiguration configuration,
            ILogger<VoidMessageProcessor> logger,
            IPaymentLegacyOrchestrator orchestrator
        )
        {
            _configuration = configuration;
            _logger = logger;
            _orchestrator = orchestrator;

            ExchangeName = _configuration["Queues:Exchange"];
            QueueName = _configuration["Queues:VoidMessageQueue"];
            RoutingKeys = new string[] { _configuration["Queues:VoidMessageQueueRoutingKey"] };
        }

        public string Name { get; } = "VoidMessageProcessor";
        public string ExchangeName { get; }
        public string QueueName { get; }
        public string[] RoutingKeys { get; }

        public async Task ProcessMessage(string content)
        {
            var message = JsonConvert.DeserializeObject<TransactionMessage>(content);

            _logger.LogInformation($"[VoidMessageProcessor] attempting to process auth message for order: {message.ReferenceId}");
            await _orchestrator.ProcessVoidMessage(message);
        }
    }
}