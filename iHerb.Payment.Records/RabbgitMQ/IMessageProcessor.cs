﻿using System.Threading.Tasks;

namespace iHerb.Payment.Records.RabbgitMQ
{
    public interface IMessageProcessor
    {
        string Name { get; }
        string ExchangeName { get; }
        string QueueName { get; }
        string[] RoutingKeys { get; }
        Task ProcessMessage(string content);
    }
}
