﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;
using System.Threading.Tasks;

namespace iHerb.Payment.Records.RabbgitMQ
{
    public class RetryPolicy : IRetryPolicy
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<RetryPolicy> _logger;
        private readonly IConnection _connection;
        private readonly int _maxRetry;
        private readonly int _retryIntervalSeconds;
        private readonly string _retryExchangeName;
        private readonly IModel _channel;

        public RetryPolicy(
            IConfiguration configuration,
            ILogger<RetryPolicy> logger,
            IRabbitConnectionProvider connectionProvider
        )
        {
            _configuration = configuration;
            _logger = logger;
            _connection = connectionProvider.GetConnection();

            _maxRetry = configuration.GetValue<int>("Queues:MaxRetry");
            _retryIntervalSeconds = configuration.GetValue<int>("Queues:RetryIntervalSeconds");
            _retryExchangeName = configuration["Queues:RetryExchange"];

            _channel = _connection.CreateModel();
            _channel.ConfirmSelect();
            _channel.ExchangeDeclare(_retryExchangeName, ExchangeType.Topic, true);
        }

        public string ExchangeName 
        { 
            get { return _retryExchangeName; }
        }

        public async Task<bool> RetryAsync(string fromExchange, string messageId, string routingKey, byte[] body)
        {
            try
            {
                int retryCount;
                if (_retryExchangeName == fromExchange)
                {
                    retryCount = int.Parse(messageId);
                }
                else
                {
                    retryCount = 0;
                }

                var shouldRetry = retryCount < _maxRetry;

                if (shouldRetry)
                {
                    retryCount++;
                    await Task.Delay(TimeSpan.FromSeconds(Math.Min(5, retryCount) * _retryIntervalSeconds));

                    var messageProperties = _channel.CreateBasicProperties();
                    messageProperties.MessageId = retryCount.ToString();

                    _channel.BasicPublish(_retryExchangeName, routingKey, messageProperties, body);
                }

                return shouldRetry;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "[RetryPolicy.RetryAsync] error occurred when handling retry.");
            }

            return false;
        }
    }
}
