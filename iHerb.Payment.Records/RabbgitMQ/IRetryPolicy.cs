﻿using System.Threading.Tasks;

namespace iHerb.Payment.Records.RabbgitMQ
{
    public interface IRetryPolicy
    {
        string ExchangeName { get; }

        Task<bool> RetryAsync(string fromExchange, string messageId, string routingKey, byte[] body);
    }
}
