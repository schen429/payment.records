﻿using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading.Tasks;
using iHerb.Payment.Records.Metrics;

namespace iHerb.Payment.Records.RabbgitMQ
{
    public class RabbitQueueConsumer : IRabbitQueueConsumer
    {
        private readonly ILogger<RabbitQueueConsumer> _logger;
        private readonly IConnection _connection;
        private readonly IRetryPolicy _retryPolicy;
        private readonly IRecordsMetricsCollector _metricsCollector;
        private IMessageProcessor _messageProcessor;
        private IModel _channel;
        private AsyncEventingBasicConsumer _consumer;

        public RabbitQueueConsumer(
            ILogger<RabbitQueueConsumer> logger,
            IRabbitConnectionProvider connectionProvider,
            IRetryPolicy retryPolicy,
            IRecordsMetricsCollector metricsCollector
        )
        {
            _logger = logger;
            _connection = connectionProvider.GetConnection();
            _retryPolicy = retryPolicy;
            _metricsCollector = metricsCollector;
        }

        public void RegisterMessageProcessor(IMessageProcessor messageProcessor)
        {
            if (string.IsNullOrEmpty(messageProcessor.Name))
                throw new ArgumentException(nameof(messageProcessor.Name));

            if (string.IsNullOrEmpty(messageProcessor.ExchangeName))
                throw new ArgumentException(nameof(messageProcessor.ExchangeName));

            if (string.IsNullOrEmpty(messageProcessor.QueueName))
                throw new ArgumentException(nameof(messageProcessor.QueueName));

            if (messageProcessor.RoutingKeys is null || messageProcessor.RoutingKeys.Length == 0)
                throw new ArgumentException(nameof(messageProcessor.RoutingKeys));

            _messageProcessor = messageProcessor;
            SetupConsumer();
        }

        public void Dispose()
        {
            _channel.Close();
            _connection.Close();
        }

        private void SetupConsumer()
        {
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(_messageProcessor.ExchangeName, ExchangeType.Topic, true);
            _channel.QueueDeclare(_messageProcessor.QueueName, true, false, false, null);
            foreach (var routingKey in _messageProcessor.RoutingKeys)
            {
                _channel.QueueBind(_messageProcessor.QueueName, _messageProcessor.ExchangeName, routingKey, null);
                _channel.QueueBind(_messageProcessor.QueueName, _retryPolicy.ExchangeName, routingKey, null);
            }

             _consumer = new AsyncEventingBasicConsumer(_channel);
            _consumer.Received += OnReceived;
            _channel.BasicConsume(_messageProcessor.QueueName, true, _consumer);
        }

        private async Task OnReceived(object sender, BasicDeliverEventArgs eventArg)
        {
            var body = eventArg.Body.ToArray();
            var content = Encoding.UTF8.GetString(body);
            _logger.LogInformation($"[RabbitQueueConsumer-{_messageProcessor.Name}] received message: {content}");

            try
            {
                await _messageProcessor.ProcessMessage(content);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"[RabbitQueueConsumer] {_messageProcessor.Name} threw exception while handling a message from the queue.");
                _metricsCollector.ObserveServiceError(_messageProcessor.Name, RecordsMetricsCollector.Reason_General);
                await _retryPolicy.RetryAsync(eventArg.Exchange, eventArg.BasicProperties.MessageId, eventArg.RoutingKey, body);
            }
        }

    }
}
