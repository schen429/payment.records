﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Threading.Tasks;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Orchestrators;

namespace iHerb.Payment.Records.RabbgitMQ
{
    public class AuthorisationMessageProcessor : IMessageProcessor
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<AuthorisationMessageProcessor> _logger;
        private readonly IPaymentLegacyOrchestrator _orchestrator;

        public AuthorisationMessageProcessor(
            IConfiguration configuration,
            ILogger<AuthorisationMessageProcessor> logger,
            IPaymentLegacyOrchestrator orchestrator
        )
        {
            _configuration = configuration;
            _logger = logger;
            _orchestrator = orchestrator;

            ExchangeName = _configuration["Queues:Exchange"];
            QueueName = _configuration["Queues:AuthorisationMessageQueue"];
            RoutingKeys = new string[] { _configuration["Queues:AuthorisationMessageQueueRoutingKey"] };
        }

        public string Name { get; } = "AuthorisationMessageProcessor";
        public string ExchangeName { get; }
        public string QueueName { get; }
        public string[] RoutingKeys { get; }

        public async Task ProcessMessage(string content)
        {
            var message = JsonConvert.DeserializeObject<TransactionMessage>(content);

            _logger.LogInformation($"[AuthorisationMessageProcessor] attempting to process auth message for order: {message.ReferenceId}");
            await _orchestrator.ProcessAuthorisationMessage(message);
        }
    }
}