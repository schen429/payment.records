﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using iHerb.Payment.Records.Orchestrators;

namespace iHerb.Payment.Records.RabbgitMQ
{
    public class TransactionMessageProcessor : IMessageProcessor
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<TransactionMessageProcessor> _logger;
        private readonly IQueueMessageProcessingOrchestrator _orchestrator;

        public TransactionMessageProcessor(
            IConfiguration configuration,
            ILogger<TransactionMessageProcessor> logger,
            IQueueMessageProcessingOrchestrator orchestrator
        )
        {
            _configuration = configuration;
            _logger = logger;
            _orchestrator = orchestrator;

            ExchangeName = _configuration["Queues:Exchange"];
            QueueName = _configuration["Queues:TransactionMessageQueue"];
            RoutingKeys = new string[] { _configuration["Queues:TransactionMessageQueueRoutingKey"] };
        }

        public string Name { get; } = "TransactionMessageProcessor";
        public string ExchangeName { get; }
        public string QueueName { get; }
        public string[] RoutingKeys { get; }

        public async Task ProcessMessage(string content)
        {
            await _orchestrator.TransferPaymentMessage(content);
        }
    }
}