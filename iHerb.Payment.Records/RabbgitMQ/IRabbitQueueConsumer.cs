﻿using System;

namespace iHerb.Payment.Records.RabbgitMQ
{
    public interface IRabbitQueueConsumer : IDisposable
    {
        void RegisterMessageProcessor(IMessageProcessor messageProcessor);
    }
}
