﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Threading.Tasks;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Orchestrators;

namespace iHerb.Payment.Records.RabbgitMQ
{
    public class CaptureMessageProcessor : IMessageProcessor
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<CaptureMessageProcessor> _logger;
        private readonly IPaymentLegacyOrchestrator _orchestrator;

        public CaptureMessageProcessor(
            IConfiguration configuration,
            ILogger<CaptureMessageProcessor> logger,
            IPaymentLegacyOrchestrator orchestrator
        )
        {
            _configuration = configuration;
            _logger = logger;
            _orchestrator = orchestrator;

            ExchangeName = _configuration["Queues:Exchange"];
            QueueName = _configuration["Queues:CaptureMessageQueue"];
            RoutingKeys = new string[] {
                _configuration["Queues:CaptureMessageQueueRoutingKey"],
                _configuration["Queues:CaptureViaAuthorizationMessageQueueRoutingKey"]
            };
        }

        public string Name { get; } = "CaptureMessageProcessor";
        public string ExchangeName { get; }
        public string QueueName { get; }
        public string[] RoutingKeys { get; }

        public async Task ProcessMessage(string content)
        {
            var message = JsonConvert.DeserializeObject<TransactionMessage>(content);
            _logger.LogInformation($"[CaptureMessageProcessor] attempting to process auth message for order: {message.ReferenceId}");
            await _orchestrator.ProcessCaptureMessage(message);
        }
    }
}
