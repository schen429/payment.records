﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;

namespace iHerb.Payment.Records.RabbgitMQ
{
    public class RabbitConnectionProvider : IRabbitConnectionProvider
    {
        private readonly IConfiguration _configuration;

        public RabbitConnectionProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IConnection GetConnection()
        {
            var connectionFactory = new ConnectionFactory
            {
                HostName = _configuration["Queues:Host"],
                Port = _configuration.GetValue<int>("Queues:Port"),
                VirtualHost = _configuration["Queues:VirtualHost"],
                UserName = _configuration["Queues:UserName"],
                Password = _configuration["Queues:Password"],
                DispatchConsumersAsync = true
            };

            return connectionFactory.CreateConnection();
        }

    }
}
