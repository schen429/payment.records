using System.Threading.Tasks;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Orchestrators;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace iHerb.Payment.Records.RabbgitMQ
{
    public class ChargebackMessageProcessor: IMessageProcessor
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<ChargebackMessageProcessor> _logger;
        private readonly IPaymentLegacyOrchestrator _orchestrator;

        public ChargebackMessageProcessor(
            IConfiguration configuration,
            ILogger<ChargebackMessageProcessor> logger,
            IPaymentLegacyOrchestrator orchestrator
        )
        {
            _configuration = configuration;
            _logger = logger;
            _orchestrator = orchestrator;

            ExchangeName = _configuration["Queues:Exchange"];
            QueueName = _configuration["Queues:ChargebackMessageQueue"];
            RoutingKeys = new string[] { _configuration["Queues:ChargebackMessageQueueRoutingKey"] };
        }
        public string Name { get; } = nameof(ChargebackMessageProcessor);
        public string ExchangeName { get; }
        public string QueueName { get; }
        public string[] RoutingKeys { get; }
        public async Task ProcessMessage(string content)
        {
            var message = JsonConvert.DeserializeObject<TransactionMessage>(content);

            _logger.LogInformation($"[{nameof(ChargebackMessageProcessor)}] attempting to process chargeback message for order: {message.ReferenceId}");
            await _orchestrator.ProcessChargebackMessage(message);
        }
    }
}