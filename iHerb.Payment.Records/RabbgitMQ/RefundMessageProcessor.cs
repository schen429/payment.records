﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Threading.Tasks;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Orchestrators;

namespace iHerb.Payment.Records.RabbgitMQ
{
    public class RefundMessageProcessor : IMessageProcessor
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<RefundMessageProcessor> _logger;
        private readonly IPaymentLegacyOrchestrator _orchestrator;

        public RefundMessageProcessor(
            IConfiguration configuration,
            ILogger<RefundMessageProcessor> logger,
            IPaymentLegacyOrchestrator orchestrator
        )
        {
            _configuration = configuration;
            _logger = logger;
            _orchestrator = orchestrator;

            ExchangeName = _configuration["Queues:Exchange"];
            QueueName = _configuration["Queues:RefundMessageQueue"];
            RoutingKeys = new string[] { _configuration["Queues:RefundMessageQueueRoutingKey"] };
        }

        public string Name { get; } = "RefundMessageProcessor";
        public string ExchangeName { get; }
        public string QueueName { get; }
        public string[] RoutingKeys { get; }

        public async Task ProcessMessage(string content)
        {
            var message = JsonConvert.DeserializeObject<TransactionMessage>(content);

            _logger.LogInformation($"[RefundMessageProcessor] attempting to process refund message for order: {message.ReferenceId}");
            await _orchestrator.ProcessRefundMessage(message);
        }
    }
}