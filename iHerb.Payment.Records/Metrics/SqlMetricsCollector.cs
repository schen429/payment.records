﻿using Prometheus;
using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading.Tasks;

namespace iHerb.Payment.Records.Metrics
{
    public class SqlMetricsCollector : ISqlMetricsCollector
    {
        public const string Database = "database";
        public const string Schema = "schema";
        public const string Command = "command";
        public const string Status = "status";
        public const string Status_Success = "success";
        public const string Status_Failure = "failure";

        private readonly IRecordsMetricsCollector _recordsMetricsCollector;

        public static readonly string[] AllLabels =
        {
            Database,
            Schema,
            Command,
            Status
        };

        public Histogram DurationMetric { get; }
        public Counter TimeoutCounter { get; }

        public SqlMetricsCollector(IRecordsMetricsCollector recordsMetricsCollector)
        {
            _recordsMetricsCollector = recordsMetricsCollector;

            DurationMetric = Prometheus.Metrics.CreateHistogram(
                "payment_api_sql_request_duration_seconds",
                "SQL request duration metric for records",
                new HistogramConfiguration
                {
                    // 2 ms to 64K ms buckets
                    Buckets = Histogram.ExponentialBuckets(0.002, 2, 16),
                    LabelNames = AllLabels
                }
            );

            TimeoutCounter = Prometheus.Metrics.CreateCounter(
                "payment_api_sql_request_timeout_count",
                "SQL request timeout count for Records",
                new CounterConfiguration
                {
                    LabelNames = AllLabels
                }
            );
        }

        public void ObserveDuration(string database, string schema, string command, string status, double seconds)
        {
            var durationMetric = DurationMetric.WithLabels(database, schema, command, status);
            durationMetric.Observe(seconds);
        }

        public void ObserveTimeout(string database, string schema, string command, string status)
        {
            var timeoutMetric = TimeoutCounter.WithLabels(database, schema, command, status);
            timeoutMetric.Inc();
        }

        public async Task ObserveExecuteAsync(string database, string schema, string command, Func<Task> action)
        {
            var status = Status_Failure;
            var stopWatch = Stopwatch.StartNew();
            try
            {
                await action();
                status = Status_Success;
            }
            catch (Exception ex)
            {
                _recordsMetricsCollector.ObserveServiceError(RecordsMetricsCollector.Service_Records, RecordsMetricsCollector.Reason_DBFailure);

                if (ex is SqlException sqlex && sqlex.Number == -2 /*timeout*/)
                {
                    ObserveTimeout(database, schema, command, status);
                }

                throw;
            }
            finally
            {
                stopWatch.Stop();
                ObserveDuration(database, schema, command, status, stopWatch.Elapsed.TotalSeconds);
            }
        }

        public async Task<T> ObserveExecuteAsync<T>(string database, string schema, string command, Func<Task<T>> func)
        {
            T result;
            var stopWatch = Stopwatch.StartNew();

            try
            {
                result = await func();
            }
            catch (Exception ex)
            {
                _recordsMetricsCollector.ObserveServiceError(RecordsMetricsCollector.Service_Records, RecordsMetricsCollector.Reason_DBFailure);

                if (ex is SqlException sqlex && sqlex.Number == -2 /*timeout*/)
                {
                    ObserveTimeout(database, schema, command, Status_Failure);
                }

                stopWatch.Stop();
                ObserveDuration(database, schema, command, Status_Failure, stopWatch.Elapsed.TotalSeconds);

                throw;
            }

            stopWatch.Stop();
            ObserveDuration(database, schema, command, Status_Success, stopWatch.Elapsed.TotalSeconds);

            return result;
        }
    }
}
