﻿using Prometheus;

namespace iHerb.Payment.Records.Metrics
{
    public class RecordsMetricsCollector : IRecordsMetricsCollector
    {
        public const string Label_Service = "service";
        public const string Label_Reason = "reason";

        public const string Service_Records = "records";

        public const string Reason_DBFailure = "db_failure";
        public const string Reason_General = "general";

        public Counter ServiceErrorCounter { get; }

        public RecordsMetricsCollector()
        {
            ServiceErrorCounter = Prometheus.Metrics.CreateCounter(
                "records_service_error_count",
                "Records Service Error Count",
                new CounterConfiguration
                {
                    LabelNames = new string[] { Label_Service, Label_Reason }
                }
            );
        }

        public void ObserveServiceError(string service, string reason)
        {
            var metric = ServiceErrorCounter.WithLabels(service, reason);
            metric.Inc();
        }
    }
}
