﻿using System;
using System.Threading.Tasks;

namespace iHerb.Payment.Records.Metrics
{
    public interface ISqlMetricsCollector
    {
        void ObserveDuration(string database, string schema, string command, string status, double seconds);
        void ObserveTimeout(string database, string schema, string command, string status);
        Task ObserveExecuteAsync(string database, string schema, string command, Func<Task> action);
        Task<T> ObserveExecuteAsync<T>(string database, string schema, string command, Func<Task<T>> func);
    }
}
