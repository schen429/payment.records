﻿namespace iHerb.Payment.Records.Metrics
{
    public interface IRecordsMetricsCollector
    {
        void ObserveServiceError(string service, string reason);
    }
}
