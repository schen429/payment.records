﻿using iHerb.Platform.Authentication.Extensions;
using iHerb.Platform.HealthChecks.Extensions;
using iHerb.Platform.Logging.Extensions;
using iHerb.Platform.Metrics.Extensions;
using iHerb.Platform.WebApi.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NSwag;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using iHerb.Payment.Records.Data;
using iHerb.Payment.Records.Data.Infrastructure;
using iHerb.Payment.Records.Factories;
using iHerb.Payment.Records.Metrics;
using iHerb.Payment.Records.Models.Constants;
using iHerb.Payment.Records.Orchestrators;
using iHerb.Payment.Records.RabbgitMQ;
using iHerb.Payment.Records.Repositories;
using iHerb.Payment.Records.Services;
using iHerb.Payment.Records.Utils;
using SchemaType = NJsonSchema.SchemaType;

namespace iHerb.Payment.Records
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        private ILogger<Startup> Logger { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            
            services.AddLazyCache();

            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });

            // helpers DI services
            services.AddHttpContextAccessor();

            // iHerb DI helper services
            services.AddiHerbApiAuthentication()
                .AddScheme<XApplicationAuthenticationOptions, XApplicationAuthenticationHandler>(
                    PaymentConstants.XApplicationAuthenticatorScheme, c => c.configuration = Configuration)
                .AddPolicyScheme(PaymentConstants.MultiAuthenticatorScheme, "Multi-scheme authenticator", options =>
                {
                    options.ForwardDefaultSelector = context =>
                    {
                        var authHeader = context.Request.Headers["Authorization"].FirstOrDefault();
                        if (authHeader != null)
                        {
                            return JwtBearerDefaults.AuthenticationScheme;
                        }

                        return PaymentConstants.XApplicationAuthenticatorScheme;
                    };
                })
                .AddJwtBearer(PaymentConstants.ChatbotScheme, options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey =
                            new X509SecurityKey(
                                new X509Certificate2(Convert.FromBase64String(Configuration["Ada-JwtValidation:Key"]))),
                        ValidateIssuer = true,
                        ValidIssuer = Configuration["Ada-JwtValidation:Issuer"],
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.FromSeconds(30),
                    };
                    options.Events = new JwtBearerEvents();
                    options.Events.OnAuthenticationFailed += async context =>
                    {
                        Logger.LogDebug(context.ToString());
                        await Task.Delay(10);
                    };
                });

            services.AddSingleton<PaymentLogMapper>();
            services.AddSingleton<IDapperWrapper, DapperWrapper>();
            services.AddSingleton<IPaymentLogsFactory, PaymentLogsFactory>();
            var dbConnectionString = Configuration.GetSection("ConnectionStrings:RecordsConnectionString").Value;
            services.AddTransient<IDbConnection>(_ => new SqlConnection(dbConnectionString));
            services.AddSingleton<IDbConnectionFactory, DbConnectionFactory>();
            services.AddSingleton<ISqlExecutor, SqlExecutor>();
            services.AddSingleton<IContextAccessorService, ContextAccessorService>();
            services.AddSingleton<IPaymentLegacyRepository, PaymentLegacyRepository>();
            services.AddSingleton<IShopRepository, ShopRepository>();
            services.AddSingleton<IRecordsMetricsCollector, RecordsMetricsCollector>();
            services.AddSingleton<ISqlMetricsCollector, SqlMetricsCollector>();

            services.AddSingleton<IPaymentLegacyOrchestrator, PaymentLegacyOrchestrator>();
            services.AddSingleton<IQueueMessageProcessingOrchestrator, QueueMessageProcessingOrchestrator>();
            services.AddSingleton<IFraudOrchestrator, FraudOrchestrator>();
            services.AddSingleton<IRabbitConnectionProvider, RabbitConnectionProvider>();
            services.AddSingleton<IRetryPolicy, RetryPolicy>();
            services.AddTransient<IRabbitQueueConsumer, RabbitQueueConsumer>();
            services.AddSingleton<AuthorisationMessageProcessor>();
            services.AddSingleton<CaptureMessageProcessor>();
            services.AddSingleton<RefundMessageProcessor>();
            services.AddSingleton<SecuredMessageProcessor>();
            services.AddSingleton<TransactionMessageProcessor>();
            services.AddSingleton<VoidMessageProcessor>();

            services.AddiHerbHealthChecks();

            services.AddHostedService<AuthorisationMessageConsumerService>();
            services.AddHostedService<CaptureMessageConsumerService>();
            services.AddHostedService<RefundMessageConsumerService>();
            services.AddHostedService<TransactionMessageConsumerService>();
            services.AddHostedService<SecuredMessageConsumerService>();
            services.AddHostedService<VoidMessageConsumerService>();

            services.AddOpenApiDocument(document =>
            {
                document.DocumentName = AppDomain.CurrentDomain.FriendlyName;
                document.AllowReferencesWithProperties = true;
                document.Title = "iHerb Payment Records API";
                document.Description = Assembly.GetEntryAssembly()?.GetName().Version?.ToString();
                document.SchemaType = SchemaType.OpenApi3;
                document.FlattenInheritanceHierarchy = true;

                OpenApiSecurityScheme val = new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "x-application-x",
                    Description = "The Authorization header is Api Key",
                    In = OpenApiSecurityApiKeyLocation.Header
                };
                document.AddSecurity("ApiKey", Enumerable.Empty<string>(), val);
                document.PostProcess = delegate (OpenApiDocument x)
                {
                    x.Schemes = new[]
                    {
                        OpenApiSchema.Https
                    };
                };
            });

            services.AddiHerbHttpLoggerOptions(options =>
            {
                options.OptOut(new[] { "/healthz/*" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, ILogger<Startup> logger)
        {
            Logger = logger;
            app.RegisterLifetimes();
            app.UseRouting();
            app.UseAuthentication();
            app.UseOpenApi();
            app.UseSwaggerUi3();

            // iHerb custom middleware ORDER MATTERS!
            app.UseiHerbMetrics();
            app.UseiHerbTracingHeaders();
            app.UseiHerbHttpLogging();
            app.UseiHerbHealthChecks();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => endpoints.MapControllers());

            // misc middleware
            app.UseHttpsRedirection();
        }
    }
}
