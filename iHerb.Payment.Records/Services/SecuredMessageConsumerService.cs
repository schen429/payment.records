﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;
using iHerb.Payment.Records.RabbgitMQ;

namespace iHerb.Payment.Records.Services
{
    public class SecuredMessageConsumerService : BackgroundService
    {
        private readonly ILogger<SecuredMessageConsumerService> _logger;
        private readonly IRabbitQueueConsumer _queueConsumer;
        private readonly SecuredMessageProcessor _messageProcessor;

        public SecuredMessageConsumerService(
            ILogger<SecuredMessageConsumerService> logger,
            IRabbitQueueConsumer queueConsumer,
            SecuredMessageProcessor messageProcessor
        )
        {
            _logger = logger;
            _queueConsumer = queueConsumer;
            _messageProcessor = messageProcessor;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("[SecuredMessageConsumerService] on Hosted Service is starting.");

            _queueConsumer.RegisterMessageProcessor(_messageProcessor);

            _logger.LogInformation("[SecuredMessageConsumerService] on Hosted Service has started.");

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _queueConsumer.Dispose();
            base.Dispose();
        }
    }
}

