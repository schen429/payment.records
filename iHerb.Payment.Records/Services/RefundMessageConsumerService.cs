﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;
using iHerb.Payment.Records.RabbgitMQ;

namespace iHerb.Payment.Records.Services
{
    public class RefundMessageConsumerService : BackgroundService
    {
        private readonly ILogger<RefundMessageConsumerService> _logger;
        private readonly IRabbitQueueConsumer _queueConsumer;
        private readonly RefundMessageProcessor _messageProcessor;

        public RefundMessageConsumerService(
            ILogger<RefundMessageConsumerService> logger,
            IRabbitQueueConsumer queueConsumer,
            RefundMessageProcessor messageProcessor
        )
        {
            _logger = logger;
            _queueConsumer = queueConsumer;
            _messageProcessor = messageProcessor;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("[RefundMessageConsumerService] on Hosted Service is starting.");

            _queueConsumer.RegisterMessageProcessor(_messageProcessor);

            _logger.LogInformation("[RefundMessageConsumerService] on Hosted Service has started.");

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _queueConsumer.Dispose();
            base.Dispose();
        }
    }
}

