﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;
using iHerb.Payment.Records.RabbgitMQ;

namespace iHerb.Payment.Records.Services
{
    public class CaptureMessageConsumerService : BackgroundService
    {
        private readonly ILogger<CaptureMessageConsumerService> _logger;
        private readonly IRabbitQueueConsumer _queueConsumer;
        private readonly CaptureMessageProcessor _messageProcessor;

        public CaptureMessageConsumerService(
            ILogger<CaptureMessageConsumerService> logger,
            IRabbitQueueConsumer queueConsumer,
            CaptureMessageProcessor messageProcessor
        )
        {
            _logger = logger;
            _queueConsumer = queueConsumer;
            _messageProcessor = messageProcessor;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("[CaptureMessageConsumerService] on Hosted Service is starting.");

            _queueConsumer.RegisterMessageProcessor(_messageProcessor);

            _logger.LogInformation("[CaptureMessageConsumerService] on Hosted Service has started.");

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _queueConsumer.Dispose();
            base.Dispose();
        }
    }
}

