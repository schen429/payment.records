﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;
using iHerb.Payment.Records.RabbgitMQ;

namespace iHerb.Payment.Records.Services
{
    public class AuthorisationMessageConsumerService : BackgroundService
    {
        private readonly ILogger<AuthorisationMessageConsumerService> _logger;
        private readonly IRabbitQueueConsumer _queueConsumer;
        private readonly AuthorisationMessageProcessor _messageProcessor;

        public AuthorisationMessageConsumerService(
            ILogger<AuthorisationMessageConsumerService> logger,
            IRabbitQueueConsumer queueConsumer,
            AuthorisationMessageProcessor messageProcessor
        )
        {
            _logger = logger;
            _queueConsumer = queueConsumer;
            _messageProcessor = messageProcessor;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("[AuthorisationMessageConsumerService] on Hosted Service is starting.");

            _queueConsumer.RegisterMessageProcessor(_messageProcessor);

            _logger.LogInformation("[AuthorisationMessageConsumerService] on Hosted Service has started.");

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _queueConsumer.Dispose();
            base.Dispose();
        }
    }
}

