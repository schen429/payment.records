﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace iHerb.Payment.Records.Services
{
    internal static class HttpContextAccessorExtensions
    {
        internal static Guid? GetUserId(this IHttpContextAccessor httpContextAccessor)
        {
            return httpContextAccessor.HttpContext.Request.Headers.TryGetValue("UserId", out var userIdValues) &&
                   Guid.TryParse(userIdValues.SingleOrDefault(), out var userId)
                ? userId
                : (Guid?)null;
        }
    }

    public class ContextAccessorService : IContextAccessorService
    {
        private IHttpContextAccessor HttpContextAccessor
        {
            get;
            set;
        }

        public virtual Guid? UserId => HttpContextAccessor.GetUserId();

        public ContextAccessorService(IHttpContextAccessor httpContextAccessor)
        {
            HttpContextAccessor = httpContextAccessor;
        }
    }

    public interface IContextAccessorService
    {
        Guid? UserId { get; }
    }
}
