﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;
using iHerb.Payment.Records.RabbgitMQ;

namespace iHerb.Payment.Records.Services
{
    public class TransactionMessageConsumerService : BackgroundService
    {
        private readonly ILogger<TransactionMessageConsumerService> _logger;
        private readonly IRabbitQueueConsumer _queueConsumer;
        private readonly TransactionMessageProcessor _messageProcessor;

        public TransactionMessageConsumerService(
            ILogger<TransactionMessageConsumerService> logger,
            IRabbitQueueConsumer queueConsumer,
            TransactionMessageProcessor messageProcessor
        )
        {
            _logger = logger;
            _queueConsumer = queueConsumer;
            _messageProcessor = messageProcessor;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("[TransactionMessageConsumerService] on Hosted Service is starting.");

            _queueConsumer.RegisterMessageProcessor(_messageProcessor);

            _logger.LogInformation("[TransactionMessageConsumerService] on Hosted Service has started.");

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _queueConsumer.Dispose();
            base.Dispose();
        }
    }
}

