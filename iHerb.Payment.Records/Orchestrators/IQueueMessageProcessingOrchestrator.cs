﻿using System.Threading.Tasks;

namespace iHerb.Payment.Records.Orchestrators
{
    public interface IQueueMessageProcessingOrchestrator
    {
        Task TransferPaymentMessage(string message);
    }
}
