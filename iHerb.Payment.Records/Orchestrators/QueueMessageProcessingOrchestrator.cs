﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using iHerb.Payment.Records.Models.Messages;

namespace iHerb.Payment.Records.Orchestrators
{
    public class QueueMessageProcessingOrchestrator : IQueueMessageProcessingOrchestrator
    {
        private readonly ILogger<QueueMessageProcessingOrchestrator> _logger;
        private readonly IPaymentLegacyOrchestrator _paymentLegacyOrchestrator;

        public QueueMessageProcessingOrchestrator(
            ILogger<QueueMessageProcessingOrchestrator> logger,
            IPaymentLegacyOrchestrator paymentLegacyOrchestrator
        )
        {
            _logger = logger;
            _paymentLegacyOrchestrator = paymentLegacyOrchestrator;
        }

        public async Task TransferPaymentMessage(string message)
        {
            PaymentMessage paymentMessage;
            try
            {
                paymentMessage = JsonConvert.DeserializeObject<PaymentMessage>(message);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, $"[QueueMessageProcessingOrchestrator][TransferPaymentMessage] Unable to deserialize PaymentMessage from the queue: {exception}");
                throw;
            }

            await _paymentLegacyOrchestrator.ProcessPaymentMessage(paymentMessage, message);
        }
    }
}
