﻿using System;
using System.Threading.Tasks;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Models.Messages;

namespace iHerb.Payment.Records.Orchestrators
{
    public interface IPaymentLegacyOrchestrator
    {
        Task ProcessPaymentMessage(PaymentMessage paymentMessage, string body);
        Task ProcessCaptureMessage(TransactionMessage message);
        Task ProcessRefundMessage(TransactionMessage message);
        Task ProcessChargebackMessage(TransactionMessage message);
        Task ProcessAuthorisationMessage(TransactionMessage message);
        Task ProcessVoidMessage(TransactionMessage message);
        Task<TransactionLogResponse> GetTransactionLog(int transactionId);
        Task<TransactionLogResponse> GetTransactionLogs(int? orderNumber, string sortExpression);
        Task<PaymentTransactionInfoResponse> GetPaymentTransactionInfo(int? orderNumber, byte? orderExt);
        Task<TransactionLogResponse> GetTransactionsByUser(Guid userId, PaymentType? paymentType = null, string paymentSubType = null,
            DateTime? beginTxDate = null, DateTime? endTxDate = null);
        Task<BillingAddressResponse> GetBillingAddress(int orderNumber);
        Task<OrderPaidAmountResponse> GetOrderTotalPaid(int orderNumber, byte? ext);
    }
}
