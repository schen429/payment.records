using System.Threading.Tasks;
using iHerb.Payment.Records.Models.Fraud;

namespace iHerb.Payment.Records.Orchestrators
{
    public interface IFraudOrchestrator
    {
        Task<GetTransactionLogsResponse> GetTransactionLogsAsync(GetTransactionLogsRequest request);
    }
}
