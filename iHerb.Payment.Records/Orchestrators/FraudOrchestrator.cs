using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Models.Fraud;
using iHerb.Payment.Records.Models.PaymentLogs;
using iHerb.Payment.Records.Repositories;
using PaymentLog = iHerb.Payment.Records.Models.Fraud.PaymentLog;

namespace iHerb.Payment.Records.Orchestrators
{
    public class FraudOrchestrator : IFraudOrchestrator
    {
        private readonly IConfiguration _configuration;
        private readonly IPaymentLegacyRepository _paymentLegacyRepository;

        public FraudOrchestrator(IConfiguration configuration, IPaymentLegacyRepository paymentLegacyRepository)
        {
            _configuration = configuration;
            _paymentLegacyRepository = paymentLegacyRepository;
        }

        public async Task<GetTransactionLogsResponse> GetTransactionLogsAsync(GetTransactionLogsRequest request)
        {
            var paymentLogs =
                await _paymentLegacyRepository.GetTransactionLogs(request.OrderNumber, nameof(PaymentLogBase.TxDate));

            var response = new GetTransactionLogsResponse
            {
                PaymentLogs = paymentLogs.Skip(request.Skip.GetValueOrDefault())
                    .Take(request.Top ?? _configuration.GetValue<int>("ApiSettings:DefaultPageSize"))
                    .Select(pl => new Models.Fraud.PaymentLog
                    {
                        AccountName = pl.AccountName,
                        AccountNo = pl.AccountNo,
                        AccountType = pl.AccountType,
                        AccountTypeNew = pl.AccountTypeNew,
                        AdditionalInfo = pl.AdditionalInfo,
                        AddressMatch = pl.AddressMatch,
                        AltReferenceId = pl.AltReferenceId,
                        Amount = pl.Amount,
                        Approved = pl.Approved,
                        AuthCode = pl.AuthCode,
                        BankName = pl.BankName,
                        BillingCountryCode = pl.BillingCountryCode,
                        Captured = pl.Captured,
                        CardClass = pl.CardClass,
                        Comment = pl.Comment,
                        CountryCode = pl.CountryCode,
                        CountryCodeWf = pl.CountryCodeWf,
                        CreditCardAliasId = pl.CreditCardAliasId,
                        CurrencyCode = pl.CurrencyCode,
                        CustomerId = pl.CustomerId,
                        CvcMatch = pl.CvcMatch,
                        DetailCardIndicator = pl.DetailCardIndicator,
                        ExpirationMonth = pl.ExpirationMonth,
                        ExpirationYear = pl.ExpirationYear,
                        FraudResult = CreateFraudResultData(pl),
                        International = pl.International,
                        OrderExt = pl.OrderExt,
                        OrderNumber = pl.OrderNumber,
                        PaymentEngine = pl.PaymentEngine,
                        PaymentId = pl.PaymentId,
                        ProductId = pl.ProductId,
                        ProductSubType = pl.ProductSubType,
                        ReconciliationId = pl.ReconciliationId,
                        ReferenceId = pl.ReferenceId,
                        RequestToken = pl.RequestToken,
                        ResponseCode = pl.ResponseCode,
                        ResponseMessage = pl.ResponseMessage,
                        SingleMerchantI = pl.SingleMerchantI,
                        TokenEngine = pl.TokenEngine,
                        TokenId = pl.TokenId,
                        TxDate = pl.TxDate,
                        TxType = (PaymentTxTypePayment) pl.TxType,
                        Username = pl.Username,
                        Voided = pl.Voided,
                        ZipCodeMatch = pl.ZipCodeMatch
                    })
                    .ToList(),
                Total = paymentLogs.Count
            };

            return response;
        }

        private static AdyenFraudResultData CreateFraudResultData(RawPaymentLog rawPaymentLog)
        {
            if ((PaymentTxTypePayment) rawPaymentLog.TxType != PaymentTxTypePayment.Authorization) return null;

            AdyenFraudResult adyenFraudResult;

            try
            {
                using var document = JsonDocument.Parse(rawPaymentLog.AdditionalInfo);

                if (document.RootElement.ValueKind != JsonValueKind.Object) return null;

                var fraudResultProperty = document.RootElement.EnumerateObject()
                    .SingleOrDefault(p => string.Equals(p.Name, "fraudResult", StringComparison.OrdinalIgnoreCase))
                    .Value;

                if (fraudResultProperty.ValueKind != JsonValueKind.Object) return null;

                adyenFraudResult = JsonSerializer.Deserialize<AdyenFraudResult>(
                    fraudResultProperty.GetRawText(),
                    new JsonSerializerOptions
                    {
                        IgnoreNullValues = true,
                        PropertyNameCaseInsensitive = true
                    });
            }
            catch (JsonException)
            {
                return null;
            }

            return new AdyenFraudResultData
            {
                AccountScore = adyenFraudResult!.accountScore,
                Results = adyenFraudResult!.results?.Select(c => c.FraudCheckResult)
                    .Where(r => r != null && r.accountScore != 0)
                    .Select(r => new AdyenFraudCheckResultData
                    {
                        AccountScore = r.accountScore,
                        CheckId = r.checkId,
                        Name = r.name
                    })
                    .ToList()
            };
        }
    }
}
