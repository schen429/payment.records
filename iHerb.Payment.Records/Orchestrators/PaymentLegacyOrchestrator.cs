﻿using iHerb.Payment.Records.Utils;
using LazyCache;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Factories;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Models.Messages;
using iHerb.Payment.Records.Models.PaymentLogs;
using iHerb.Payment.Records.Repositories;
using Newtonsoft.Json.Linq;

namespace iHerb.Payment.Records.Orchestrators
{
    public class PaymentLegacyOrchestrator : IPaymentLegacyOrchestrator
    {
        private readonly IPaymentLogsFactory _paymentLogsFactory;
        private readonly IPaymentLegacyRepository _paymentLegacyRepository;
        private readonly ILogger<QueueMessageProcessingOrchestrator> _logger;
        private readonly IShopRepository _shopRepository;
        private readonly IAppCache _appCache;
        private readonly double _cacheExpirationTimeHours;

        public PaymentLegacyOrchestrator(
            IPaymentLogsFactory paymentLogsFactory,
            IPaymentLegacyRepository paymentLegacyRepository,
            ILogger<QueueMessageProcessingOrchestrator> logger,
            IShopRepository shopRepository,
            IAppCache appCache,
            IConfiguration configuration)
        {
            _logger = logger;
            _shopRepository = shopRepository;
            _appCache = appCache;
            _paymentLogsFactory = paymentLogsFactory;
            _paymentLegacyRepository = paymentLegacyRepository;
            _cacheExpirationTimeHours = Convert.ToDouble(configuration["CacheSettings:CacheExpirationTimeHours"]);

        }

        public async Task ProcessPaymentMessage(PaymentMessage paymentMessage, string body)
        {
            PaymentLog paymentLogLegacy =
                _paymentLogsFactory.GetPaymentLog(paymentMessage.Source, paymentMessage.Subject, body);


            if (paymentLogLegacy is null)
            {
                _logger.LogError(
                    $"[PaymentLegacyOrchestrator][ProcessPaymentMessage] null paymentLogLegacy.");
                return;
            }

            _logger.LogInformation(
                $"[PaymentLegacyOrchestrator][ProcessPaymentMessage] Processing message: CustomerId = {paymentLogLegacy.CustomerId}, OrderNumber = {paymentLogLegacy.OrderNumber}, OrderExt = {paymentLogLegacy.OrderExt}, AccountType = {paymentLogLegacy.AccountType}, TxType = {paymentLogLegacy.TxType}, Amount = {paymentLogLegacy.Amount}");

            await _paymentLegacyRepository.InsertPaymentLog(paymentLogLegacy);
        }

        public async Task ProcessCaptureMessage(TransactionMessage message)
        {
            PaymentLog paymentLogLegacy = new PaymentLog()
            {
                CustomerId = message.UserId,
                OrderNumber = Convert.ToInt32(message.ReferenceId),
                AccountType = message.GetAccountType(),
                Amount = message.Amount,
                CurrencyCode = message.Currency,
                ReferenceId = message.TransactionId.ToString(),
                TxType = PaymentTxTypePayment.CaptureConfirmation,
                TxDate = DateTime.Now,
                ResponseMessage = message.TransactionStatus.ToString(),
                ResponseCode = ((int) message.TransactionStatus).ToString(),
                Approved = message.TransactionStatus is TransactionStatus.CAPTURED,
                Comment = message.TransactionId.ToString(),
                Username = UserName.PaymentApi,
                Voided = false,
                Captured = message.TransactionStatus is TransactionStatus.CAPTURED,
                AdditionalInfo = JsonConvert.SerializeObject(message.RawResponse),
                TokenId = message.PaymentAccountId,
                PaymentEngine = 0,
                CvcMatch = 'X',
                AddressMatch = 'X',
                ZipCodeMatch = 'X',
                OrderExt = message.Extension?.ToInt()
            };

            _logger.LogInformation(
                $"[PaymentLegacyOrchestrator][ProcessCaptureMessage] Processing message: CustomerId = {paymentLogLegacy.CustomerId}, OrderNumber = {paymentLogLegacy.OrderNumber}, OrderExt = {paymentLogLegacy.OrderExt}, AccountType = {paymentLogLegacy.AccountType}, TxType = {paymentLogLegacy.TxType}, Amount = {paymentLogLegacy.Amount}");

            await _paymentLegacyRepository.InsertPaymentLog(paymentLogLegacy);

            if (message.TransactionStatus == TransactionStatus.CAPTURED)
            {
                await _paymentLegacyRepository.InsertOrderPayment(message.Amount, message.ReferenceId,
                    message.Extension.ToIntOrDefault());
            }

            await UpsertPaymentProperties(message);
        }

        public async Task ProcessRefundMessage(TransactionMessage message)
        {
            PaymentLog paymentLogLegacy = new PaymentLog()
            {
                CustomerId = message.UserId,
                OrderNumber = Convert.ToInt32(message.ReferenceId),
                AccountType = message.GetAccountType(),
                Amount = message.Amount,
                CurrencyCode = message.Currency,
                ReferenceId = message.TransactionId.ToString(),
                TxType = PaymentTxTypePayment.RefundRequest,
                TxDate = DateTime.Now,
                ResponseMessage = message.TransactionStatus.ToString(),
                ResponseCode = ((int) message.TransactionStatus).ToString(),
                Approved = message.TransactionStatus is TransactionStatus.REFUNDED,
                Comment = message.TransactionId.ToString(),
                Username = UserName.PaymentApi,
                Voided = false,
                Captured = false,
                AdditionalInfo = JsonConvert.SerializeObject(message.RawResponse),
                TokenId = message.PaymentAccountId,
                PaymentEngine = 0,
                CvcMatch = 'X',
                AddressMatch = 'X',
                ZipCodeMatch = 'X',
                OrderExt = message.Extension?.ToInt()
            };

            _logger.LogInformation(
                $"[PaymentLegacyOrchestrator][ProcessRefundMessage] Processing message: CustomerId = {paymentLogLegacy.CustomerId}, OrderNumber = {paymentLogLegacy.OrderNumber}, OrderExt = {paymentLogLegacy.OrderExt}, AccountType = {paymentLogLegacy.AccountType}, TxType = {paymentLogLegacy.TxType}, Amount = {paymentLogLegacy.Amount}");

            await _paymentLegacyRepository.InsertPaymentLog(paymentLogLegacy);

            if (message.TransactionStatus is TransactionStatus.REFUNDED)
            {
                await _paymentLegacyRepository.InsertOrderPayment(message.Amount, message.ReferenceId,
                    message.Extension.ToIntOrDefault(), true);
            }

            await UpsertPaymentProperties(message);
        }

        public async Task ProcessChargebackMessage(TransactionMessage message)
        {
            PaymentLog paymentLogLegacy = new PaymentLog()
            {
                CustomerId = message.UserId,
                OrderNumber = Convert.ToInt32(message.ReferenceId),
                AccountType = message.GetAccountType(),
                Amount = message.Amount,
                CurrencyCode = message.Currency,
                ReferenceId = message.TransactionId.ToString(),
                TxType = PaymentTxTypePayment.ChargeBack,
                TxDate = DateTime.Now,
                ResponseMessage = message.TransactionStatus.ToString(),
                ResponseCode = ((int) message.TransactionStatus).ToString(),
                Approved = message.TransactionStatus is TransactionStatus.REFUNDED or TransactionStatus.CAPTURED,
                Comment = message.TransactionId.ToString(),
                Username = UserName.PaymentApi,
                Voided = false,
                Captured = false,
                AdditionalInfo = JsonConvert.SerializeObject(message.RawResponse),
                TokenId = message.PaymentAccountId,
                PaymentEngine = 0,
                CvcMatch = 'X',
                AddressMatch = 'X',
                ZipCodeMatch = 'X',
                OrderExt = message.Extension?.ToInt()
            };

            _logger.LogInformation(
                $"[PaymentLegacyOrchestrator][{nameof(ProcessChargebackMessage)}] Processing message: CustomerId = {paymentLogLegacy.CustomerId}, OrderNumber = {paymentLogLegacy.OrderNumber}, OrderExt = {paymentLogLegacy.OrderExt}, AccountType = {paymentLogLegacy.AccountType}, TxType = {paymentLogLegacy.TxType}, Amount = {paymentLogLegacy.Amount}");

            await _paymentLegacyRepository.InsertPaymentLog(paymentLogLegacy);

            switch (message.TransactionStatus)
            {
                case TransactionStatus.REFUNDED:
                    await _paymentLegacyRepository.InsertOrderPayment(message.Amount, message.ReferenceId,
                        message.Extension.ToIntOrDefault(), true);
                    break;
                case TransactionStatus.CAPTURED:
                    await _paymentLegacyRepository.InsertOrderPayment(message.Amount, message.ReferenceId,
                        message.Extension.ToIntOrDefault());
                    break;
            }

            await UpsertPaymentProperties(message);
        }

        public async Task ProcessAuthorisationMessage(TransactionMessage message)
        {
            if (!(message.PaymentType is PaymentType.CreditCard) &&
                !(message.TransactionStatus is TransactionStatus.CAPTURED))
            {
                PaymentLog paymentLogLegacy = new PaymentLog()
                {
                    CreditCardAliasId = message.Alias,
                    BillingCountryCode = message.BillingAddress?.Country,
                    CustomerId = message.UserId,
                    OrderNumber = Convert.ToInt32(message.ReferenceId),
                    AccountType = message.GetAccountType(),
                    Amount = message.Amount,
                    CurrencyCode = message.Currency,
                    ReferenceId = message.TransactionId.ToString(),
                    TokenEngine = (int) GetTokenEnginePayment(message.PaymentEngine),
                    TxType = PaymentTxTypePayment.Authorization,
                    TxDate = DateTime.Now,
                    ResponseMessage = message.TransactionStatus.ToString(),
                    ResponseCode = ((int) message.TransactionStatus).ToString(),
                    Approved = message.TransactionStatus is TransactionStatus.AUTHORISED,
                    Comment = message.TransactionId.ToString(),
                    Username = UserName.PaymentApi,
                    Voided = false,
                    Captured = false,
                    AdditionalInfo = JsonConvert.SerializeObject(message.RawResponse),
                    TokenId = message.PaymentAccountId,
                    PaymentEngine = 0,
                    CvcMatch = 'X',
                    AddressMatch = 'X',
                    ZipCodeMatch = 'X',
                    OrderExt = message.Extension?.ToInt()
                };

                _logger.LogInformation(
                    $"[PaymentLegacyOrchestrator][ProcessAuthorisationMessage] Processing message: CustomerId = {paymentLogLegacy.CustomerId}, OrderNumber = {paymentLogLegacy.OrderNumber}, OrderExt = {paymentLogLegacy.OrderExt}, AccountType = {paymentLogLegacy.AccountType}, TxType = {paymentLogLegacy.TxType}, Amount = {paymentLogLegacy.Amount}");

                await _paymentLegacyRepository.InsertPaymentLog(paymentLogLegacy);
            }

            if (!(message.TransactionStatus is TransactionStatus.CAPTURED))
            {
                await UpsertPaymentProperties(message);
            }
        }

        public async Task ProcessVoidMessage(TransactionMessage message)
        {
            PaymentLog paymentLogLegacy = new PaymentLog()
            {
                CustomerId = message.UserId,
                OrderNumber = Convert.ToInt32(message.ReferenceId),
                AccountType = message.GetAccountType(),
                Amount = message.Amount,
                CurrencyCode = message.Currency,
                ReferenceId = message.TransactionId.ToString(),
                TxType = PaymentTxTypePayment.Void,
                TxDate = DateTime.Now,
                ResponseMessage = message.TransactionStatus.ToString(),
                ResponseCode = ((int) message.TransactionStatus).ToString(),
                Approved = message.TransactionStatus is TransactionStatus.VOIDED,
                Comment = message.TransactionId.ToString(),
                Username = UserName.PaymentApi,
                Voided = message.TransactionStatus is TransactionStatus.VOIDED,
                Captured = false,
                AdditionalInfo = JsonConvert.SerializeObject(message.RawResponse),
                TokenId = message.PaymentAccountId,
                PaymentEngine = 0,
                CvcMatch = 'X',
                AddressMatch = 'X',
                ZipCodeMatch = 'X',
                OrderExt = message.Extension?.ToInt()
            };

            _logger.LogInformation(
                $"[PaymentLegacyOrchestrator][ProcessVoidMessage] Processing message: CustomerId = {paymentLogLegacy.CustomerId}, OrderNumber = {paymentLogLegacy.OrderNumber}, OrderExt = {paymentLogLegacy.OrderExt}, AccountType = {paymentLogLegacy.AccountType}, TxType = {paymentLogLegacy.TxType}, Amount = {paymentLogLegacy.Amount}");

            await _paymentLegacyRepository.InsertPaymentLog(paymentLogLegacy);

            if (message.TransactionStatus is TransactionStatus.VOIDED)
            {
                await _paymentLegacyRepository.InsertOrderPayment(message.Amount, message.ReferenceId,
                    message.Extension.ToIntOrDefault(), false, true);
            }

            await UpsertPaymentProperties(message);
        }

        public async Task<TransactionLogResponse> GetTransactionLog(int transactionId)
        {
            var originalLogs = await _paymentLegacyRepository.GetTransactionLog(transactionId);
            return new TransactionLogResponse() {PaymentLogs = ConvertToPaymentLogResponse(originalLogs)};
        }

        public async Task<TransactionLogResponse> GetTransactionLogs(int? orderNumber, string sortExpression)
        {
            var originalLogs =
                await _paymentLegacyRepository.GetTransactionLogs(orderNumber, sortExpression ?? string.Empty);
            return new TransactionLogResponse() {PaymentLogs = ConvertToPaymentLogResponse(originalLogs)};
        }

        public async Task<PaymentTransactionInfoResponse> GetPaymentTransactionInfo(int? orderNumber, byte? orderExt)
        {
            var transactionLogs = await GetTransactionLogs(orderNumber, string.Empty);

            PaymentLog transactionLogsResult = null;
            PaymentLog transactionLogsAuthResult = null;

            if (transactionLogs.PaymentLogs.Any(p => !DoesOrderRequireCapture(p.AccountType, p.CurrencyCode)))
            {
                transactionLogsResult = transactionLogs.PaymentLogs.FirstOrDefault(p =>
                    p.TxType == PaymentTxTypePayment.CaptureConfirmation && p.Approved);
                transactionLogsAuthResult =
                    transactionLogs.PaymentLogs.FirstOrDefault(p =>
                        p.TxType is PaymentTxTypePayment.AuthorizationRequest or PaymentTxTypePayment.Authorization &&
                        p.Approved);
            }
            else if (transactionLogs.PaymentLogs.Any(p =>
                p.TxType is PaymentTxTypePayment.DelayedCapture or PaymentTxTypePayment.Sale &&
                p.Approved))
            {
                transactionLogsResult = transactionLogs.PaymentLogs.FirstOrDefault(p =>
                    p.TxType is PaymentTxTypePayment.DelayedCapture or PaymentTxTypePayment.Sale &&
                    p.Approved);
                transactionLogsAuthResult = transactionLogs.PaymentLogs.FirstOrDefault(p =>
                    p.TxType == PaymentTxTypePayment.Authorization && p.Approved);
            }

            if (transactionLogsResult != null)
            {
                return new PaymentTransactionInfoResponse()
                {
                    PaymentMethod = transactionLogsResult.AccountType ?? string.Empty,
                    TransactionDate = transactionLogsResult.TxDate,
                    TransactionId = transactionLogsResult.ReferenceId
                };
            }

            if (transactionLogsAuthResult != null)
            {
                return new PaymentTransactionInfoResponse()
                {
                    PaymentMethod = transactionLogsAuthResult.AccountType,
                    TransactionDate = transactionLogsAuthResult.TxDate,
                    TransactionId = transactionLogsAuthResult.ReferenceId
                };
            }

            return null;
        }

        public async Task<TransactionLogResponse> GetTransactionsByUser(Guid userId, PaymentType? paymentType = null,
            string paymentSubType = null,
            DateTime? beginTxDate = null, DateTime? endTxDate = null)
        {
            var originalLogs =
                await _paymentLegacyRepository.GetTransactionsByUser(userId, paymentType, paymentSubType, beginTxDate,
                    endTxDate);
            return new TransactionLogResponse() {PaymentLogs = ConvertToPaymentLogResponse(originalLogs)};
        }

        public async Task<BillingAddressResponse> GetBillingAddress(int orderNumber)
        {
            var properties = await _paymentLegacyRepository.GetBillingAddressByOrderNumber(orderNumber);
            if (properties == null) return null;

            var billingAddresses = (from prop in properties
                    where prop.BillingAddress != null
                    select new AuthorizationBillingAddress
                    {
                        TransactionId = prop.TransactionId,
                        TransactionStatus = prop.TransactionStatus,
                        BillingAddress = DeserializeBillingAddress(prop)
                    }
                ).ToList();

            return billingAddresses.Count == 0
                ? null
                : new BillingAddressResponse {BillingAddresses = billingAddresses};
        }

        private void DeserializeAdditionalData(PaymentLog paymentLog)
        {
            try
            {
                if (!paymentLog.AdditionalInfo.Contains("additionalData", StringComparison.InvariantCultureIgnoreCase))
                    return;

                var addnlData = JObject.Parse(paymentLog.AdditionalInfo);
                paymentLog.CreditCardAliasId = addnlData["additionalData"]["alias"].ToString();
                paymentLog.ExpirationMonth = addnlData["additionalData"]["expiryDate"].ToString().Contains("/")
                    ? Convert.ToInt16(addnlData["additionalData"]["expiryDate"].ToString().Split("/")[0])
                    : 0;
                paymentLog.ExpirationYear = addnlData["additionalData"]["expiryDate"].ToString().Contains("/")
                    ? Convert.ToInt16(addnlData["additionalData"]["expiryDate"].ToString().Split("/")[1])
                    : 0;
                paymentLog.AccountName = addnlData["additionalData"]["cardHolderName"].ToString();
            }
            catch (Exception ex)
            {
                _logger.LogWarning(
                    $"{nameof(DeserializeAdditionalData)} failed. {ex}");
            }
        }

        private BillingAddress DeserializeBillingAddress(PaymentProperties prop)
        {
            var billingAddress = JsonConvert.DeserializeObject<BillingAddress>(prop.BillingAddress);
            try
            {
                var addnlData = JObject.Parse(prop.AdditionalInfo);
                if (prop.AdditionalInfo.Contains("additionalData",
                    StringComparison.InvariantCultureIgnoreCase))
                {
                    billingAddress.AccountHolderName = addnlData["additionalData"]["cardHolderName"].ToString();
                    if (billingAddress.AccountHolderName.Contains(" "))
                    {
                        billingAddress.FirstName = billingAddress.AccountHolderName.Split(' ')[0];
                        billingAddress.LastName = billingAddress.AccountHolderName.Split(' ')[1];
                    }
                }
                else if (prop.AdditionalInfo.Contains("ShippingAddress",
                    StringComparison.InvariantCultureIgnoreCase))
                {
                    billingAddress.FirstName = addnlData["Target"]["ShippingAddress"]["FirstName"].ToString();
                    billingAddress.LastName = addnlData["Target"]["ShippingAddress"]["LastName"].ToString();
                    billingAddress.AccountHolderName = billingAddress.FirstName + " " + billingAddress.LastName;
                }
            }
            catch (Exception ex)
            {
                _logger.LogWarning(
                    $"{nameof(DeserializeBillingAddress)} failed. {ex}");
            }

            return billingAddress;
        }

        public async Task<OrderPaidAmountResponse> GetOrderTotalPaid(int orderNumber, byte? ext)
        {
            var amount = await _paymentLegacyRepository.GetOrderTotalPaid(orderNumber, ext);
            if (amount == 0) return null;
            return new OrderPaidAmountResponse
            {
                AmountTotalPaid = amount
            };
        }

        private TokenEnginePayment GetTokenEnginePayment(TokenEngineTransactions paymentEngine)
        {
            switch (paymentEngine)
            {
                case TokenEngineTransactions.ADYEN3DS2:
                    return TokenEnginePayment.Adyen3DS2;
                case TokenEngineTransactions.IHERB:
                    return TokenEnginePayment.iHerb;
                default:
                    return TokenEnginePayment.iHerb;
            }
        }

        private List<PaymentLog> ConvertToPaymentLogResponse(IList<RawPaymentLog> originalLogs)
        {
            var logs = new List<PaymentLog>();
            foreach (var originalLog in originalLogs)
            {
                var log = new PaymentLog();
                foreach (var prop in typeof(RawPaymentLog).GetProperties())
                {
                    if (prop.Name == "TxType")
                    {
                        log.TxType = (PaymentTxTypePayment) ((byte) originalLog.TxType);
                        continue;
                    }

                    log.GetType().GetProperty(prop.Name).SetValue(log,
                        originalLog.GetType().GetProperty(prop.Name).GetValue(originalLog, null));
                }

                DeserializeAdditionalData(log);
                logs.Add(log);
            }

            return logs;
        }
        
        private bool DoesOrderRequireCapture(string accountType, string currencyCode)
        {
            var localCurrencyPaymentTypeList = GetLocalCurrencyPaymentTypes().Result;

            var localCurrencyPaymentType = localCurrencyPaymentTypeList.FirstOrDefault(l =>
                l.CreditCardType == accountType && l.CurrencyCode == currencyCode);
            var externalPaymentType = localCurrencyPaymentType == null
                ? ExternalPaymentType.None
                : localCurrencyPaymentType
                    .ExternalPaymentType;
            return (externalPaymentType == ExternalPaymentType.None ||
                    externalPaymentType == ExternalPaymentType.PayPal);
        }

        private async Task<List<LocalCurrencyPaymentType>> GetLocalCurrencyPaymentTypes()
        {
            byte deviceStore = 0;
            var includeDisabled = false;
            var cacheKey = $"LocalCurrencyPaymentTypes_{deviceStore}_{includeDisabled}";
            var localCurrencyPaymentTypes = await _appCache.GetAsync<List<LocalCurrencyPaymentType>>(cacheKey);
            if (localCurrencyPaymentTypes != null) return localCurrencyPaymentTypes;

            localCurrencyPaymentTypes = await _shopRepository.GetLocalCurrencyPaymentTypesAsync(deviceStore, false);

            _appCache.Add(cacheKey,
                localCurrencyPaymentTypes, TimeSpan.FromHours(_cacheExpirationTimeHours));
            return localCurrencyPaymentTypes;
        }

        private async Task UpsertPaymentProperties(TransactionMessage message)
        {
            var paymentProperties = new PaymentProperties
            {
                TransactionId = message.TransactionId,
                CustomerId = message.UserId,
                OrderNumber = Convert.ToInt32(message.ReferenceId),
                TransactionDate = DateTime.Now,
                TransactionStatus = message.TransactionStatus.ToString(),
                BillingAddress = (message.BillingAddress is null) ? null : JsonConvert.SerializeObject(message.BillingAddress),
                LastUpdatedBy = "PaymentApi"
            };

            await _paymentLegacyRepository.UpsertPaymentProperties(paymentProperties);
        }
    }
}