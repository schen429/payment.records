﻿using System;
using System.Linq;
using System.Threading.Tasks;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Models.Constants;
using iHerb.Payment.Records.Orchestrators;
using iHerb.Payment.Records.Services;
using iHerb.Platform.Authentication.DependencyInjections;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace iHerb.Payment.Records.Controllers
{
    [Route("api/records")]
    [ApiController]
    public class RecordsController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IContextAccessorService _contextAccessorService;
        private readonly IPaymentLegacyOrchestrator _paymentLegacyOrchestrator;

        public RecordsController(IPaymentLegacyOrchestrator paymentLegacyOrchestrator,
            IAuthenticationService authenticationService,
            IContextAccessorService contextAccessorService)
        {
            _paymentLegacyOrchestrator = paymentLegacyOrchestrator;
            _authenticationService = authenticationService;
            _contextAccessorService = contextAccessorService;
        }

        /// <summary>
        ///     Get the transaction logs of an order
        /// </summary>
        /// <remarks>
        ///     Returns transaction logs of a given order number
        /// </remarks>
        [Authorize(AuthenticationSchemes = PaymentConstants.XApplicationAuthenticatorScheme)]
        [HttpGet("TransactionLogs")]
        public async Task<ActionResult<TransactionLogResponse>> GetTransactionLogs(string orderNumber,
            string sortExpression = "")
        {
            if (sortExpression != null)
            {
                var containsSpecialCharacter =
                    sortExpression.Replace(" ", string.Empty).Any(x =>
                        !char.IsLetterOrDigit(x));
                if (containsSpecialCharacter) return BadRequest("SortExpression contains unsupported characters.");
            }

            if (string.IsNullOrEmpty(orderNumber)) return NotFound();
            var result =
                await _paymentLegacyOrchestrator.GetTransactionLogs(Convert.ToInt32(orderNumber), sortExpression);
            if (result.PaymentLogs.Count == 0) return NotFound();

            return Ok(result);
        }

        /// <summary>
        ///     Get the payment transaction info to WMS
        /// </summary>
        /// <remarks>
        ///     Returns transaction info of a given order number
        /// </remarks>
        [Authorize(AuthenticationSchemes = PaymentConstants.XApplicationAuthenticatorScheme)]
        [HttpGet("PaymentTransactionInfo")]
        public async Task<ActionResult<PaymentTransactionInfoResponse>> GetPaymentTransactionInfo(int orderNumber,
            byte? orderExt)
        {
            var result = await _paymentLegacyOrchestrator.GetPaymentTransactionInfo(orderNumber, orderExt);
            if (result == null) return NotFound();

            return Ok(result);
        }

        /// <summary>
        ///     Get the transaction logs of a user
        /// </summary>
        /// <remarks>
        ///     Returns transaction logs of a given user
        /// </remarks>
        [Authorize(AuthenticationSchemes = PaymentConstants.MultiAuthenticatorScheme)]
        [HttpGet("TransactionLogs/{userId}")]
        public async Task<ActionResult<TransactionLogResponse>> GetUserTransactionLogs(Guid userId,
            [FromQuery] PaymentType? paymentType = null,
            [FromQuery] string paymentSubType = null,
            [FromQuery] DateTime? beginTxDate = null,
            [FromQuery] DateTime? endTxDate = null)
        {
            if (HttpContext?.User?.Identity?.AuthenticationType == PaymentConstants.XApplicationAuthenticatorScheme)
            {
                if (userId == Guid.Empty) return new ForbidResult();
            }
            else
            {
                var authUserId = _authenticationService.UserId == Guid.Empty
                    ? _contextAccessorService.UserId ?? Guid.Empty
                    : _authenticationService.UserId;

                if (userId == Guid.Empty || authUserId == Guid.Empty || userId != authUserId) return new ForbidResult();
            }

            var result = await _paymentLegacyOrchestrator.GetTransactionsByUser(
                userId,
                paymentType,
                paymentSubType,
                beginTxDate,
                endTxDate);

            return Ok(result);
        }

        /// <summary>
        ///     Get the billing address of an order
        /// </summary>
        /// <remarks>
        ///     Returns billing address of a given order number
        /// </remarks>
        [Authorize(AuthenticationSchemes = PaymentConstants.XApplicationAuthenticatorScheme)]
        [HttpGet("BillingAddresses")]
        public async Task<ActionResult<BillingAddressResponse>> GetBillingAddresses(int orderNumber)
        {
            var result = await _paymentLegacyOrchestrator.GetBillingAddress(orderNumber);
            if (result == null) return NotFound();

            return Ok(result);
        }
        
        /// <summary>
        ///     Get the order total paid amount for a given order number
        /// </summary>
        /// <remarks>
        ///     Returns the order total paid amount for a given order number
        /// </remarks>
        [Authorize(AuthenticationSchemes = PaymentConstants.MultiAuthenticatorScheme)]
        [HttpGet("OrderTotalPaid")]
        public async Task<ActionResult<OrderPaidAmountResponse>> GetOrderAmount(int orderNumber, byte? ext)
        {
            var response = await _paymentLegacyOrchestrator.GetOrderTotalPaid(orderNumber, ext);
            if (response is null) return NotFound();
            return Ok(response);
        }
    }
}