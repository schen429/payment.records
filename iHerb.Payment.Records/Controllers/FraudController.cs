using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using iHerb.Payment.Records.Models.Constants;
using iHerb.Payment.Records.Models.Fraud;
using iHerb.Payment.Records.Orchestrators;

namespace iHerb.Payment.Records.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FraudController : ControllerBase
    {
        private readonly IFraudOrchestrator _fraudOrchestrator;

        public FraudController(IFraudOrchestrator fraudOrchestrator)
        {
            _fraudOrchestrator = fraudOrchestrator;
        }

        /// <summary>
        /// Get the transaction logs of an order.
        /// </summary>
        /// <response code="200">
        /// Transaction logs for a given order number, sorted by ascending TxDate.
        /// </response>
        [Authorize(AuthenticationSchemes = PaymentConstants.XApplicationAuthenticatorScheme)]
        [HttpGet("TransactionLogs")]
        public async Task<ActionResult<GetTransactionLogsResponse>> GetTransactionLogs(
            [FromQuery] GetTransactionLogsRequest request)
        {
            var response = await _fraudOrchestrator.GetTransactionLogsAsync(request);
            if (response.Total == 0) return NotFound();

            return Ok(response);
        }
    }
}
