﻿using System.Collections.Generic;
using iHerb.Payment.Records.Models.PaymentLogs;

namespace iHerb.Payment.Records.Models
{
    public class TransactionLogResponse
    {
        public IList<PaymentLog> PaymentLogs { get; set; }
    }
}
