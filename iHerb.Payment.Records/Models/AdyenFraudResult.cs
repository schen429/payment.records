using System.Collections.Generic;

namespace iHerb.Payment.Records.Models
{
    public class AdyenFraudResult
    {
        public int accountScore { get; set; }
        public IList<AdyenFraudCheckResultContainer> results { get; set; }
    }
}
