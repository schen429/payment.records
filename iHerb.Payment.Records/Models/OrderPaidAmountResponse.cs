namespace iHerb.Payment.Records.Models
{
    public class OrderPaidAmountResponse
    {
        public decimal AmountTotalPaid { get; set; }
    }
}