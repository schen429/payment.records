namespace iHerb.Payment.Records.Models
{
    public class AdyenFraudCheckResultContainer
    {
        public AdyenFraudCheckResult FraudCheckResult { get; set; }
    }
}
