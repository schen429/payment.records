﻿namespace iHerb.Payment.Records.Models.Constants
{
    public static class PaymentConstants
    {
        public const string PaymentConnectionName = "Payment";
        public const string SecureConnectionName = "Secure";
        public const string MultiAuthenticatorScheme = "multi-authenticator";
        public const string ChatbotScheme = "chatbot-authenticator";
        public const string XApplicationAuthenticatorScheme = "x-application-authenticator";
    }
}
