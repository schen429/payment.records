using System;

namespace iHerb.Payment.Records.Models
{
    public class OriginalOrderPayment
    {
        public int PaymentID { get; set; }
        public int OrderNumber { get; set; }
        public string ReferenceID { get; set; }
        public byte OrderExt { get; set; }
        public decimal Amount { get; set; }
        public string AuthCode { get; set; }
        public DateTime PaymentDate { get; set; }
        public bool Voided { get; set; }
        public byte OrderPaymentType { get; set; }
    }

    public class OrderPayment
    {
        public string ReferenceId { get; set; }
        public byte OrderExt { get; set; }
        public decimal Amount { get; set; }
        public string AuthCode { get; set; }
        public DateTime PaymentDate { get; set; }
        public bool Voided { get; set; }
        public OrderPaymentTypePayment? OrderPaymentType { get; set; }
    }

    public enum OrderPaymentTypePayment : byte
    {
        Rewards = 1,
        Chargeback = 2,
        Charge = 3,
        CODCharge = 4,
        External = 5,
        Refund = 6,
        ChargebackReversal = 7
    }
}