namespace iHerb.Payment.Records.Models
{
    public class AdyenFraudCheckResult
    {
        public int accountScore { get; set; }
        public int checkId { get; set; }
        public string name { get; set; }
    }
}
