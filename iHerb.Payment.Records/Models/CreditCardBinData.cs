﻿namespace iHerb.Payment.Records.Models
{
    public class CreditCardBinData
    {
        public string CountryCode { get; set; } = null;
        public string BankName { get; set; } = "?";
        public string ProductId { get; set; } = "?";
        public string ProductSubType { get; set; } = "?";
        public string DetailCardIndicator { get; set; } = "?";
        public string CardClass { get; set; } = "?";
    }
}
