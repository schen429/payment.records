﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using iHerb.Payment.Records.Enums;

namespace iHerb.Payment.Records.Models.Consumed.Authorization
{
    public class AuthorizationExternalRequest
    {
        private object _rawResponse;

        public Guid TransactionId { get; set; }
        public PaymentType PaymentType { get; set; }
        public PaymentSubType PaymentSubType { get; set; } 
        public string ReferenceId { get; set; }
        public Guid UserId { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string AccountHolderName { get; set; }
        public string ExpirationMonth { get; set; } 
        public string ExpirationYear { get; set; } 
        public string Last4 { get; set; }
        public string ExternalPaymentAccountId { get; set; }
        public string Country { get; set; }

        public object RawResponse
        {
            get => _rawResponse;
            set
            {
                _rawResponse = value;

                if (value is JObject jObject)
                {
                    AuthorizationExternalResponse = jObject.ToObject<AuthorizationExternalResponse>();
                }
            }
        }

        public string PaymentAccountId { get; set; }
        public TokenEngineTransactions PaymentEngine { get; set; }
        public ActionType ActionType { get; set; }
        public string ActionUrl { get; set; }
        public string CreditCardAliasId { get; set; }
        public string Extension { get; set; }

        [JsonIgnore]
        public AuthorizationExternalResponse AuthorizationExternalResponse { get; set; }

        public string GetAccountType() =>
            (PaymentType is PaymentType.CreditCard) ? PaymentSubType.ToString()  : PaymentType.ToString();
    }
}
