﻿using iHerb.Payment.Records.Enums;
using Newtonsoft.Json;

namespace iHerb.Payment.Records.Models.Consumed.Authorization
{
    public class AuthorizationExternalResponse
    {
        public AdyenResultCode resultCode { get; set; }
        public string details { get; set; }
        public string redirect { get; set; }
        public AdyenAdditionalData additionalData { get; set; }
        public string pspReference { get; set; }
        public string paymentData { get; set; }
        public string message { get; set; }
        public string refusalReason { get; set; }
    }
}
