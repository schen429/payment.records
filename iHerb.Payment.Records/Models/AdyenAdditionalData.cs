﻿using Newtonsoft.Json;

namespace iHerb.Payment.Records.Models
{
    public class AdyenAdditionalData
    {
        [JsonProperty("authCode")]
        public string authCode { get; set; }

        [JsonProperty("recurring.recurringDetailReference")]
        public string recurringDetailReference { get; set; }

        [JsonProperty("recurringProcessingModel")]
        public string recurringProcessingModel { get; set; }

        [JsonProperty("recurring.shopperReference")]
        public string shopperReference { get; set; }

        [JsonProperty("avsResult")]
        public string avsResult { get; set; }

        [JsonProperty("refusalReasonRaw")]
        public string refusalReasonRaw { get; set; }

        [JsonProperty("issuerCountry")]
        public string issuerCountry { get; set; }

        [JsonProperty("cvcResult")]
        public string cvcResult { get; set; }

        [JsonProperty("cardBin")]
        public string cardBin { get; set; }

        [JsonProperty("aliasType")]
        public string aliasType { get; set; }

        [JsonProperty("avsResultRaw")]
        public string avsResultRaw { get; set; }

        [JsonProperty("alias")]
        public string alias { get; set; }

        [JsonProperty("cvcResultRaw")]
        public string cvcResultRaw { get; set; }

        [JsonProperty("paymentMethodVariant")]
        public string paymentMethodVariant { get; set; }

        [JsonProperty("acquirerCode")]
        public string acquirerCode { get; set; }

        [JsonProperty("acquirerReference")]
        public string acquirerReference { get; set; }

        [JsonProperty("cardIssuingCountry")]
        public string cardIssuingCountry { get; set; }

        [JsonProperty("fundingSource")]
        public string fundingSource { get; set; }

        [JsonProperty("cardPaymentMethod")]
        public string cardPaymentMethod { get; set; }

        [JsonProperty("cardIssuingBank")]
        public string cardIssuingBank { get; set; }
        public string expiryDate { get; set; }
    }

}
