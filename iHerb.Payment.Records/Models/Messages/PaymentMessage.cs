﻿using iHerb.Payment.Records.Enums;

namespace iHerb.Payment.Records.Models.Messages
{
    public class PaymentMessage<T> : PaymentMessage, IPaymentMessage<T>
    {
        public T Body { get; set; }
    }
    public class PaymentMessage
    {
        public MessageSource Source { get; set; }
        public MessageSubject Subject { get; set; }
    }
}
