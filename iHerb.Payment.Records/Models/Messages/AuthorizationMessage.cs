﻿using iHerb.Payment.Records.Models.Consumed.Authorization;

namespace iHerb.Payment.Records.Models.Messages
{
    public class AuthorizationMessage : PaymentMessage<AuthorizationExternalRequest>
    {
    }
}
