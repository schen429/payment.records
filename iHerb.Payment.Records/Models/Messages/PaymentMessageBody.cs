﻿namespace iHerb.Payment.Records.Models.Messages
{
    public class PaymentMessageBody
    {
        public string ExternalRequest { get; set; }
        public string ExternalResponse { get; set; }
        public string CreditCardBinData { get; set; }
    }
}
