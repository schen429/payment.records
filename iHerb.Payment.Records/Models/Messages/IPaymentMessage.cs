﻿namespace iHerb.Payment.Records.Models.Messages
{
    public interface IPaymentMessage
    {

    }
    public interface IPaymentMessage<T> : IPaymentMessage
    {
        T Body { get; set; }
    }
}
