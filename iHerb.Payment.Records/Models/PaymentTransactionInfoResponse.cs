﻿using System;

namespace iHerb.Payment.Records.Models
{
    public class PaymentTransactionInfoResponse
    {
        public string TransactionId { get; set; }
        public string PaymentMethod { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}