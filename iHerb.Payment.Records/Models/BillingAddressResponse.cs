using System;
using System.Collections.Generic;

namespace iHerb.Payment.Records.Models
{
    public class BillingAddressResponse
    {
        public IList<AuthorizationBillingAddress> BillingAddresses { get; set; }
    }

    public class AuthorizationBillingAddress
    {
        public Guid TransactionId { get; set; }
        public BillingAddress BillingAddress { get; set; }
        public string TransactionStatus { get; set; }
    }
}