﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using iHerb.Payment.Records.Enums;

namespace iHerb.Payment.Records.Models
{
    public class TransactionMessage
    {
        public Guid TransactionId { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public PaymentType PaymentType { get; set; }
        public string PaymentSubType { get; set; }
        public string PaymentAccountId { get; set; }
        public string ReferenceId { get; set; }
        public Guid UserId { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public object RawResponse { get; set; }
        public string Country { get; set; }
        public BillingAddress BillingAddress { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public TokenEngineTransactions PaymentEngine { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ActionType? ActionType { get; set; }
        public string ActionUrl { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public TransactionStatus TransactionStatus { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public virtual TransactionType TransactionType { get; set; }
        public string Alias { get; set; }
        public string Extension { get; set; }

        public string GetAccountType() =>
            (PaymentType is PaymentType.CreditCard) ? PaymentSubType ?? PaymentType.ToString() : PaymentType.ToString();
    }
}
