﻿using iHerb.Payment.Records.Utils;
using System;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models.Consumed.Authorization;

namespace iHerb.Payment.Records.Models.PaymentLogs
{
    public class AlternativePaymentAuthorizationRequestPaymentLog : PaymentLog
    {
        public AlternativePaymentAuthorizationRequestPaymentLog(AuthorizationExternalRequest request)
        {
            CustomerId = request.UserId;
            OrderNumber = Convert.ToInt32(request.ReferenceId);
            AccountNo = "N/A";
            AccountName = null;
            AccountType = request.GetAccountType();
            Amount = request.Amount;
            CurrencyCode = request.Currency;
            AuthCode = null;
            ReferenceId = null;
            TxType = PaymentTxTypePayment.AuthorizationRequest;
            TxDate = DateTime.Now;
            ResponseMessage = PaymentTxTypePayment.AuthorizationRequest.ToString();
            ResponseCode = "0";
            Approved = false;
            Comment = null;
            Username = UserName.PaymentApi;
            PaymentId = null;
            Voided = false;
            Captured = false;
            CvcMatch = (char) MatchStatusPayment.Unknown;
            AddressMatch = (char) MatchStatusPayment.Unknown;
            ZipCodeMatch = (char) MatchStatusPayment.Unknown;
            International = false;
            RequestToken = request.ReferenceId;
            ExpirationYear = null;
            ExpirationMonth = null;
            CountryCode = request.Country;
            BankName = null;
            AdditionalInfo = request.ActionUrl;
            PaymentEngine = (int) Enums.PaymentEngine.Adyen;
            TokenEngine = 0;
            TokenId = null;
            ReconciliationId = null;
            OrderExt = request.Extension?.ToInt();
            DetailCardIndicator = null;
            ProductId = null;
            CardClass = null;
            ProductSubType = null;
            CountryCodeWf = null;
            BillingCountryCode = null;
            SingleMerchantI = null;
            AccountTypeNew = null;
            CreditCardAliasId = request.CreditCardAliasId;
        }
    }
}
