﻿using iHerb.Payment.Records.Utils;
using Newtonsoft.Json;
using System;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models.Consumed.Authorization;

namespace iHerb.Payment.Records.Models.PaymentLogs
{
    public class CreditCardAuthorizationPaymentLog : PaymentLog
    {
        public CreditCardAuthorizationPaymentLog(AuthorizationExternalRequest request)
        {
            CustomerId = request.UserId;
            OrderNumber = Convert.ToInt32(request.ReferenceId);
            AccountNo = request.Last4;
            AccountName = request.AccountHolderName;
            AccountType = request.PaymentSubType.ToString();
            Amount = request.Amount;
            CurrencyCode = request.Currency;
            AuthCode = request.AuthorizationExternalResponse.additionalData.authCode;
            ReferenceId = request.AuthorizationExternalResponse.pspReference;
            TxType = PaymentTxTypePayment.Authorization;
            TxDate = DateTime.Now;
            ResponseMessage = request.AuthorizationExternalResponse.resultCode.ToString();
            ResponseCode = GetResultCode(request.AuthorizationExternalResponse.resultCode.ToString());
            Approved = GetResultCode(request.AuthorizationExternalResponse.resultCode.ToString()) ==
                       ResultCodes.Authorized;
            Comment = request.TransactionId.ToString();
            Username = UserName.PaymentApi;
            PaymentId = null;
            Voided = false;
            Captured = false;
            CvcMatch = (char) CVCMatch(request.AuthorizationExternalResponse.additionalData.cvcResult);
            AddressMatch = (char) AvsAddressMatch(request.AuthorizationExternalResponse.additionalData.avsResult);
            ZipCodeMatch = (char) AvsZipCodeMatch(request.AuthorizationExternalResponse.additionalData.avsResult);
            International = IsInternational(request.AuthorizationExternalResponse.additionalData.issuerCountry);
            RequestToken = null;
            ExpirationYear = Convert.ToInt32(request.ExpirationYear);
            ExpirationMonth = Convert.ToInt32(request.ExpirationMonth);
            CountryCode = null;
            BankName = null;
            AdditionalInfo = JsonConvert.SerializeObject(request.RawResponse);
            PaymentEngine = (int) Enums.PaymentEngine.AYPG;
            TokenEngine = (int) GetTokenEnginePayment(request.PaymentEngine);
            TokenId = request.PaymentAccountId;
            ReconciliationId = null;
            OrderExt = request.Extension?.ToInt();
            DetailCardIndicator = null;
            ProductId = null;
            CardClass = null;
            ProductSubType = null;
            CountryCodeWf = null;
            BillingCountryCode = request.Country;
            SingleMerchantI = null;
            AccountTypeNew = null;
            CreditCardAliasId = request.CreditCardAliasId;
        }

        private TokenEnginePayment GetTokenEnginePayment(TokenEngineTransactions paymentEngine)
        {
            switch (paymentEngine)
            {
                    case TokenEngineTransactions.ADYEN3DS2:
                        return TokenEnginePayment.Adyen3DS2;
                    case TokenEngineTransactions.IHERB:
                        return TokenEnginePayment.iHerb;
                    default:
                        return TokenEnginePayment.iHerb;
            }
        }
    }
}
