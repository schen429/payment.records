﻿using System;
using iHerb.Payment.Records.Enums;

namespace iHerb.Payment.Records.Models.PaymentLogs
{
    public class LocalCurrencyPaymentType
    {
        public int Id;
        public string CurrencyCode;
        public string CreditCardType;
        public bool IsEnabled;
        public    ExternalPaymentType ExternalPaymentType;
        public Decimal MinimumCharge;
        public Decimal MaximumCharge;
        public string CssClass;
        public string CountryCodes;
        public short? DisplayPriority;
        public  PaymentEngine PaymentEngine;
        public PaymentEngine? PaymentEngineAlt;
        public short HoursUntilPaymentReminder;
        public short HoursUntilPaymentCancellation;
        public PaymentOrderStatus InitialPaymentOrderStatus;
        public bool RequiresCapture;
    }
}