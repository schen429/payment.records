﻿using Newtonsoft.Json;
using System;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models.Consumed.Tokenization;

namespace iHerb.Payment.Records.Models.PaymentLogs
{
    public class CreditCardTokenizationPaymentLog : PaymentLog
    {
        public CreditCardTokenizationPaymentLog(TokenizationExternalRequest request, TokenizationExternalResponse response, CreditCardBinData creditCardBinData)
        {
            switch (response)
            {
                case TokenizationSuccessExternalResponse successExternalResponse:
                    CustomerId = request.UserId;
                    OrderNumber = null;
                    AccountNo = request.AccountData.CardData.Last4;
                    AccountName = request.AccountData.AccountHolderName;
                    AccountType = request.AccountData.CardData.SubType.ToString();
                    Amount = 0;
                    CurrencyCode = request.CurrencyCode ?? request.Currency;
                    AuthCode = successExternalResponse.additionalData.authCode;
                    ReferenceId = successExternalResponse.pspReference;
                    TxType = PaymentTxTypePayment.CreateSubscription;
                    TxDate = DateTime.Now;
                    ResponseMessage = successExternalResponse.resultCode;
                    ResponseCode = GetResultCode(successExternalResponse.resultCode);
                    Approved = false;
                    Comment = UserName.PaymentSecured;
                    Username = UserName.PaymentSecured;
                    PaymentId = null;
                    Voided = false;
                    Captured = false;
                    CvcMatch = (char)CVCMatch(successExternalResponse.additionalData.cvcResult);
                    AddressMatch = (char)AvsAddressMatch(successExternalResponse.additionalData.avsResult);
                    ZipCodeMatch = (char)AvsZipCodeMatch(successExternalResponse.additionalData.avsResult);
                    International = IsInternational(successExternalResponse.additionalData.issuerCountry);
                    RequestToken = null;
                    var month = successExternalResponse.additionalData.expiryDate?.Split('/')[0];
                    var year = successExternalResponse.additionalData.expiryDate?.Split('/')[1];
                    ExpirationMonth = month is null ? 0 : Convert.ToInt32(month);//10/2020 => 10     
                    ExpirationYear = year is null ? 0 : Convert.ToInt32(year);//10/2020 => 2020
                    CountryCode = creditCardBinData.CountryCode;
                    BankName = creditCardBinData.BankName;
                    AdditionalInfo = JsonConvert.SerializeObject(successExternalResponse);
                    PaymentEngine = (int)Enums.PaymentEngine.AYPG;
                    TokenEngine = (int)TokenEnginePayment.iHerb;
                    TokenId = request.PaymentAccountId;
                    ReconciliationId = null;
                    OrderExt = null;
                    DetailCardIndicator = creditCardBinData.DetailCardIndicator;
                    ProductId = creditCardBinData.ProductId;
                    CardClass = creditCardBinData.CardClass;
                    ProductSubType = creditCardBinData.ProductSubType;
                    CountryCodeWf = null;
                    BillingCountryCode = request.AccountData.CardData.BillingAddress.Country;
                    SingleMerchantI = null;
                    AccountTypeNew = null;
                    CreditCardAliasId = successExternalResponse.additionalData.alias;
                    break;

                case TokenizationErrorExternalResponse errorExternalResponse:
                    CustomerId = request.UserId;
                    OrderNumber = null;
                    AccountNo = request.AccountData.CardData.Last4;
                    AccountName = request.AccountData.AccountHolderName;
                    AccountType = request.AccountData.CardData.SubType.ToString();
                    Amount = 0;
                    CurrencyCode = request.CurrencyCode ?? request.Currency;
                    AuthCode = null;
                    ReferenceId = null;
                    TxType = PaymentTxTypePayment.CreateSubscription;
                    TxDate = DateTime.Now;
                    ResponseMessage = JsonConvert.SerializeObject(errorExternalResponse);
                    ResponseCode = errorExternalResponse.errorCode is null ? ResultCodes.UnknownError : errorExternalResponse.errorCode;
                    Approved = false;
                    Comment = null;
                    Username = UserName.PaymentSecured;
                    PaymentId = null;
                    Voided = false;
                    Captured = false;
                    CvcMatch = (char)MatchStatusPayment.Unknown;
                    AddressMatch = (char)MatchStatusPayment.Unknown;
                    ZipCodeMatch = (char)MatchStatusPayment.Unknown;
                    International = false;
                    RequestToken = null;
                    CountryCode = creditCardBinData.CountryCode;
                    BankName = creditCardBinData.BankName;
                    AdditionalInfo = JsonConvert.SerializeObject(response);
                    PaymentEngine = (int)Enums.PaymentEngine.AYPG;
                    TokenEngine = (int)TokenEnginePayment.iHerb;
                    TokenId = null;
                    ReconciliationId = null;
                    OrderExt = null;
                    DetailCardIndicator = creditCardBinData.DetailCardIndicator;
                    ProductId = creditCardBinData.ProductId;
                    CardClass = creditCardBinData.CardClass;
                    ProductSubType = creditCardBinData.ProductSubType;
                    CountryCodeWf = null;
                    BillingCountryCode = request.AccountData.CardData.BillingAddress.Country;
                    SingleMerchantI = null;
                    AccountTypeNew = null;
                    break;
            }
        }
    }
}
