﻿using iHerb.Platform.Constants;
using System;
using iHerb.Payment.Records.Enums;

namespace iHerb.Payment.Records.Models.PaymentLogs
{
    public class PaymentLogBase
    {
        public Guid CustomerId { get; set; }
        public int? OrderNumber { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string AccountType { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public string AuthCode { get; set; }
        public string ReferenceId { get; set; }

        public DateTime TxDate { get; set; }
        public string ResponseMessage { get; set; }
        public string ResponseCode { get; set; }
        public bool Approved { get; set; }
        public string Comment { get; set; }
        public string Username { get; set; }
        public int? PaymentId { get; set; }
        public bool Voided { get; set; }
        public bool Captured { get; set; }
        public char CvcMatch { get; set; }
        public char AddressMatch { get; set; }
        public char ZipCodeMatch { get; set; }
        public bool International { get; set; }
        public string RequestToken { get; set; }
        public int? ExpirationYear { get; set; }
        public int? ExpirationMonth { get; set; }
        public string CountryCode { get; set; }
        public string BankName { get; set; }
        public string AdditionalInfo { get; set; }
        public int? PaymentEngine { get; set; }
        public int? TokenEngine { get; set; }
        public string TokenId { get; set; }
        public string ReconciliationId { get; set; }
        public int? OrderExt { get; set; }
        public string DetailCardIndicator { get; set; }
        public string ProductId { get; set; }
        public string CardClass { get; set; }
        public string ProductSubType { get; set; }
        public string CountryCodeWf { get; set; }
        public string AltReferenceId { get; set; }
        public string BillingCountryCode { get; set; }
        public int? SingleMerchantI { get; set; }
        public string AccountTypeNew { get; set; }
        public string CreditCardAliasId { get; set; }

        public MatchStatusPayment AvsAddressMatch(string avsResult)
        {
            if (string.IsNullOrWhiteSpace(avsResult))
            {
                return MatchStatusPayment.Error;
            }

            string avsResultCode = avsResult.Split(' ')[0];

            switch (avsResultCode)
            {
                case "1":
                case "7":
                case "9":
                case "12":
                case "20":
                case "21":
                case "24":
                case "25":
                    return MatchStatusPayment.Matched;

                case "2":
                case "6":
                case "10":
                case "13":
                case "26":
                    return MatchStatusPayment.NotMatched;

                case "4":
                    return MatchStatusPayment.NotSupported;

                case "3":
                case "5":
                case "8":
                case "11":
                case "14":
                case "15":
                case "16":
                case "17":
                case "18":
                case "19":
                case "22":
                case "23":
                    return MatchStatusPayment.Unknown;

                default:
                    return MatchStatusPayment.Error;
            }
        }

        public MatchStatusPayment AvsZipCodeMatch(string avsResult)
        {
            if (string.IsNullOrWhiteSpace(avsResult))
            {
                return MatchStatusPayment.Error;
            }

            string avsResultCode = avsResult.Split(' ')[0];

            switch (avsResultCode)
            {
                case "6":
                case "7":
                case "14":
                case "15":
                case "19":
                case "20":
                case "23":
                case "24":
                    return MatchStatusPayment.Matched;

                case "1":
                case "2":
                case "16":
                case "17":
                case "26":
                    return MatchStatusPayment.NotMatched;

                case "4":
                    return MatchStatusPayment.NotSupported;


                case "3":
                case "5":
                case "8":
                case "9":
                case "10":
                case "11":
                case "12":
                case "13":
                case "18":
                case "21":
                case "22":
                case "25":
                    return MatchStatusPayment.Unknown;

                default:
                    return MatchStatusPayment.Error;
            }
        }

        public MatchStatusPayment CVCMatch(string cvcResult)
        {
            if (cvcResult != null)
            {
                string cvResultCode = cvcResult.Split(' ')[0];

                switch (cvResultCode)
                {
                    case "0":
                        return MatchStatusPayment.Unknown;
                    case "1":
                        return MatchStatusPayment.Matched;
                    case "2":
                    case "4":
                        return MatchStatusPayment.NotMatched;
                    case "6":
                    case "3":
                        return MatchStatusPayment.Error;
                    default:
                        return MatchStatusPayment.NotSupported;
                }
            }

            return MatchStatusPayment.Unknown;
        }

        public string GetResultCode(string resultCode)
        {
            var adyenResultCode = (AdyenResultCode)Enum.Parse(typeof(AdyenResultCode), resultCode);

            switch (adyenResultCode)
            {
                case AdyenResultCode.Authorised:
                    return ResultCodes.Authorized;
                case AdyenResultCode.Refused:
                    return ResultCodes.Refused;
                case AdyenResultCode.Error:
                    return ResultCodes.Error;
                case AdyenResultCode.Pending:
                    return ResultCodes.Pending;
                case AdyenResultCode.AuthenticationFinished:
                    return ResultCodes.AuthenticationFinished;
                case AdyenResultCode.Cancelled:
                    return ResultCodes.Cancelled;
                case AdyenResultCode.ChallengeShopper:
                    return ResultCodes.ChallengeShopper;
                case AdyenResultCode.Received:
                    return ResultCodes.Received;
                case AdyenResultCode.IdentifyShopper:
                    return ResultCodes.IdentifyShopper;
                case AdyenResultCode.RedirectShopper:
                    return ResultCodes.RedirectShopper;
                default:
                    return ResultCodes.Error;
            }
        }

        public bool IsInternational(string issuerCountry)
        {
            if (!string.IsNullOrEmpty(issuerCountry))
            {
                return issuerCountry != CountryCodes.UNITED_STATES;
            }

            return false;
        }
    }
    public class PaymentLog : PaymentLogBase
    {
        public PaymentTxTypePayment TxType { get; set; }
    }

    public class RawPaymentLog : PaymentLogBase
    {
        public char TxType { get; set; }
    }
}
