﻿using System;

namespace iHerb.Payment.Records.Models.PaymentLogs
{
    public class PaymentProperties
    {
		public Guid TransactionId { get; set; }
		public Guid? CustomerId { get; set; }
		public int? OrderNumber { get; set; }
		public DateTime? TransactionDate { get; set; }
		public string TransactionStatus { get; set; }
		public string BillingAddress { get; set; }
		public DateTime? LastUpdatedAt { get; set; }
		public string LastUpdatedBy { get; set; }
		public string AdditionalInfo { get; set; }
	}
}
