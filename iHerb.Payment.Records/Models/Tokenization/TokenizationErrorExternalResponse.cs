﻿namespace iHerb.Payment.Records.Models.Consumed.Tokenization
{
    public class TokenizationErrorExternalResponse : TokenizationExternalResponse
    {
        public string status { get; set; }
        public string errorCode { get; set; }
        public string message { get; set; }   
        public string errorType { get; set; }
    }
}
