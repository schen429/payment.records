﻿using Newtonsoft.Json;

namespace iHerb.Payment.Records.Models.Consumed.Tokenization
{
    public class TokenizationSuccessExternalResponse : TokenizationExternalResponse
    {
        public AdyenAdditionalData additionalData { get; set; }

        public string pspReference { get; set; }

        public string resultCode { get; set; }
    }
}
