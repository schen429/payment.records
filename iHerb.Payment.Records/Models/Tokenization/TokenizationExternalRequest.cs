﻿using System;
using iHerb.Payment.Records.Enums;

namespace iHerb.Payment.Records.Models.Consumed.Tokenization
{
    public class TokenizationExternalRequest
    {
        public Guid UserId { get; set; }
        public string CurrencyCode { get; set; }
        public string Currency { get; set; }
        public string ExternalPaymentAccountId { get; set; }
        public CreditCardRawAccountDataRequest AccountData { get; set; }

        public string PaymentAccountId { get; set; }
    }

    public class CreditCardRawAccountDataRequest
    {
        public string AccountHolderName { get; set; }
        public CreditCardDataRequest CardData { get; set; }
        public string NationalIdentificationNumber { get; set; }
    }

    public class CreditCardDataRequest
    {
        public CardDataFormat CardDataFormat { get; set; } 
        public PaymentSubType SubType { get; set; }
        public string FullCardNumber { get; set; }
        public string SecurityCode { get; set; }
        public CreditCardPaymentBillingAddressRequest BillingAddress { get; set; }
        public string ExpirationMonth { get; set; }
        public string ExpirationYear { get; set; }
        public string Last4 { get; set; }
    }

    public class CreditCardPaymentBillingAddressRequest
    {
        public string Country { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string HouseNumberOrName { get; set; }
        public string StateOrProvince { get; set; }
        public string PostalCode { get; set; }
    }
}
