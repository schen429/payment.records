﻿namespace iHerb.Payment.Records.Models
{
    public class BillingAddress
    {
        public string Country { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string HouseNumberOrName { get; set; } = string.Empty;
        public string StateOrProvince { get; set; }
        public string PostalCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AccountHolderName { get; set; }
    }
}
