using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace iHerb.Payment.Records.Models.Fraud
{
    public class GetTransactionLogsRequest : IValidatableObject
    {
        [Required]
        [Range(1, int.MaxValue)]
        public int? OrderNumber { get; set; }

        [Range(0, int.MaxValue)]
        public int? Skip { get; set; }

        public int? Top { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var configuration = validationContext.GetService<IConfiguration>();
            var defaultPageSizeMax = configuration.GetValue<int>("ApiSettings:DefaultPageSizeMax");

            if (Top != null && (Top < 1 || Top > defaultPageSizeMax))
            {
                yield return new ValidationResult(
                    $"The field {nameof(Top)} must be between 1 and {defaultPageSizeMax}.",
                    new[] { nameof(Top) });
            }
        }
    }
}
