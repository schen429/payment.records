namespace iHerb.Payment.Records.Models.Fraud
{
    public class AdyenFraudCheckResultData
    {
        public int AccountScore { get; set; }
        public int CheckId { get; set; }
        public string Name { get; set; }
    }
}
