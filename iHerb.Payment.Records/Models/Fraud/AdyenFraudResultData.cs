using System.Collections.Generic;

namespace iHerb.Payment.Records.Models.Fraud
{
    public class AdyenFraudResultData
    {
        public int AccountScore { get; set; }
        public IList<AdyenFraudCheckResultData> Results { get; set; }
    }
}
