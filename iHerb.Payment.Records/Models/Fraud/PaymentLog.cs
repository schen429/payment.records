using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models.PaymentLogs;

namespace iHerb.Payment.Records.Models.Fraud
{
    public class PaymentLog : PaymentLogBase
    {
        public AdyenFraudResultData FraudResult { get; set; }
        public PaymentTxTypePayment TxType { get; set; }
    }
}
