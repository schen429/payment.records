using System.Collections.Generic;

namespace iHerb.Payment.Records.Models.Fraud
{
    public class GetTransactionLogsResponse
    {
        public IList<PaymentLog> PaymentLogs { get; set; }
        public int Count => PaymentLogs?.Count ?? 0;
        public int Total { get; set; }
    }
}
