﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace iHerb.Payment.Records.Repositories
{
    public class SqlExecutor: ISqlExecutor
    {
        public Task<int> ExecuteNonQueryAsync(DbCommand command)
        {
            return command.ExecuteNonQueryAsync();
        }

        public Task<int> ExecuteAsync(IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            return SqlMapper.ExecuteAsync(cnn, sql, param, transaction, commandTimeout, commandType);
        }

        public Task<IEnumerable<T>> QueryAsync<T>(IDbConnection cnn, string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            return SqlMapper.QueryAsync<T>(cnn, sql, param, transaction, commandTimeout, commandType);
        }
    }
}
