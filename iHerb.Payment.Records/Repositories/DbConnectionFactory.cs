﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace iHerb.Payment.Records.Repositories
{
    public class DbConnectionFactory : IDbConnectionFactory
    {
        private readonly IDbConnection _paymentConnection;

        public DbConnectionFactory(IEnumerable<IDbConnection> connections)
        {
            _paymentConnection = connections.First();
        }
        public SqlConnection GetPaymentConnection()
        {
            return new SqlConnection(_paymentConnection.ConnectionString);
        }
    }
}
