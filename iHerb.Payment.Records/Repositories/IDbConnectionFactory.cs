﻿using System.Data.SqlClient;

namespace iHerb.Payment.Records.Repositories
{
    public interface IDbConnectionFactory
    {
        SqlConnection GetPaymentConnection();
    }

}
