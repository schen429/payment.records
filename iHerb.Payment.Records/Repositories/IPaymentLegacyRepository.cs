﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Models.PaymentLogs;

namespace iHerb.Payment.Records.Repositories
{
    public interface IPaymentLegacyRepository
    {
        Task InsertPaymentLog(PaymentLog paymentLog);
        Task InsertOrderPayment(decimal amount, string ordernumber, int orderExt, bool refund = false, bool voided = false);
        Task<IList<RawPaymentLog>> GetTransactionLog(int txID);
        Task<IList<RawPaymentLog>> GetTransactionLogs(int? orderNumber, string sortExperssion);
        Task<IList<RawPaymentLog>> GetTransactionsByUser(Guid userId, PaymentType? paymentType = null, string paymentSubType = null,
            DateTime? beginTxDate = null, DateTime? endTxDate = null);
        Task UpsertPaymentProperties(PaymentProperties authorizationProperties);
        Task<IList<PaymentProperties>> GetBillingAddressByOrderNumber(int orderNumber);
        Task<decimal> GetOrderTotalPaid(int orderNumber, byte? orderExt);
    }
}
