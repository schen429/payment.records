﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using iHerb.Payment.Records.Metrics;
using iHerb.Payment.Records.Models.PaymentLogs;

namespace iHerb.Payment.Records.Repositories
{
    [ExcludeFromCodeCoverage]
    public class ShopRepository : IShopRepository
    {
        private const string Database = "shop";      // this is not exact DB name, for logging only

        private readonly ISqlMetricsCollector _sqlMetricsCollector;
        private readonly IConfiguration _configuration;

        public ShopRepository(
            IConfiguration configuration,
            ISqlMetricsCollector metricsCollector)
        {
            _configuration = configuration;
            _sqlMetricsCollector = metricsCollector;
        }

        public async Task<List<LocalCurrencyPaymentType>> GetLocalCurrencyPaymentTypesAsync(byte deviceStore,
            bool includeDisabled = false)
        {
            List<LocalCurrencyPaymentType> result = null;
            using (IDbConnection dbConnection =
                new SqlConnection(_configuration["ConnectionStrings:ShopConnectionString"]))
            {
                await _sqlMetricsCollector.ObserveExecuteAsync(Database, "shopservice",
                    "spx_configuration_GetLocalCurrencyPaymentType_DeviceStoreExcluded",
                    async () =>
                    {
                        result = (await dbConnection.QueryAsync<LocalCurrencyPaymentType>(
                            "[shopservice].[spx_configuration_GetLocalCurrencyPaymentType_DeviceStoreExcluded]",
                            new { DeviceStore = deviceStore, IncludeDisabled = includeDisabled },
                            commandType: CommandType.StoredProcedure)).ToList();
                    });
            }

            return result;
        }
    }

    public interface IShopRepository
    {
        Task<List<LocalCurrencyPaymentType>> GetLocalCurrencyPaymentTypesAsync(byte deviceStore,
           bool includeDisabled);
    }
}