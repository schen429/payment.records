﻿using Dapper;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using iHerb.Payment.Records.Enums;
using iHerb.Payment.Records.Metrics;
using iHerb.Payment.Records.Models;
using iHerb.Payment.Records.Models.PaymentLogs;
using iHerb.Payment.Records.Orchestrators;

namespace iHerb.Payment.Records.Repositories
{
    [ExcludeFromCodeCoverage]
    public class PaymentLegacyRepository : IPaymentLegacyRepository
    {
        private const string _database = "Reporting_Payment"; // this is not exact DB name, for logging only

        private readonly ILogger<QueueMessageProcessingOrchestrator> _logger;
        private readonly IDbConnectionFactory _connectionFactory;
        private readonly ISqlMetricsCollector _sqlMetricsCollector;
        private readonly ISqlExecutor _sqlExecutor;

        public PaymentLegacyRepository(
            IDbConnectionFactory connectionFactory,
            ILogger<QueueMessageProcessingOrchestrator> logger,
            ISqlMetricsCollector sqlMetricsCollector,
            ISqlExecutor sqlExecutor)
        {
            _logger = logger;
            _connectionFactory = connectionFactory;
            _sqlMetricsCollector = sqlMetricsCollector;
            _sqlExecutor = sqlExecutor;
        }

        public async Task InsertPaymentLog(PaymentLog paymentLog)
        {
            _logger.LogInformation(
                $"[PaymentLegacyRepository][InsertPaymentLog] Inserting paymentlog {JsonConvert.SerializeObject(paymentLog)}");

            using (var connection = _connectionFactory.GetPaymentConnection())
            {
                if (connection is not null)
                    connection.Open();
                var command = new SqlCommand();
                command.CommandText = RepositoryConstants.StoredProcedure.AddTransactionLog;
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = connection;

                if (paymentLog.OrderNumber != null)
                {
                    command.Parameters.Add("@OrderNumber", SqlDbType.Int).Value = paymentLog.OrderNumber;
                }
                else
                {
                    command.Parameters.Add("@OrderNumber", SqlDbType.Int).Value = DBNull.Value;
                }

                if (paymentLog.OrderExt != null)
                {
                    command.Parameters.Add("@OrderExt", SqlDbType.TinyInt).Value = paymentLog.OrderExt;
                }
                else
                {
                    command.Parameters.Add("@OrderExt", SqlDbType.TinyInt).Value = DBNull.Value;
                }

                command.Parameters.Add("@CustomerID", SqlDbType.UniqueIdentifier).Value = paymentLog.CustomerId;

                if (paymentLog.AccountNo != null)
                {
                    command.Parameters.Add("@AccountNo", SqlDbType.Char).Value = paymentLog.AccountNo;
                }
                else
                {
                    command.Parameters.Add("@AccountNo", SqlDbType.Char).Value = DBNull.Value;
                }

                if (paymentLog.AccountType != null)
                {
                    command.Parameters.Add("@AccountType", SqlDbType.VarChar).Value = paymentLog.AccountType;
                }
                else
                {
                    command.Parameters.Add("@AccountType", SqlDbType.VarChar).Value = DBNull.Value;
                }

                command.Parameters.Add("@Amount", SqlDbType.Money).Value = paymentLog.Amount;

                if (paymentLog.CurrencyCode != null)
                {
                    command.Parameters.Add("@CurrencyCode", SqlDbType.Char).Value = paymentLog.CurrencyCode;
                }
                else
                {
                    command.Parameters.Add("@CurrencyCode", SqlDbType.Char).Value = string.Empty;
                }

                if (paymentLog.AuthCode != null)
                {
                    command.Parameters.Add("@AuthCode", SqlDbType.Char).Value = paymentLog.AuthCode;
                }
                else
                {
                    command.Parameters.Add("@AuthCode", SqlDbType.Char).Value = DBNull.Value;
                }

                if (paymentLog.ReferenceId != null)
                {
                    command.Parameters.Add("@ReferenceID", SqlDbType.VarChar).Value = paymentLog.ReferenceId;
                }
                else
                {
                    command.Parameters.Add("@ReferenceID", SqlDbType.VarChar).Value = DBNull.Value;
                }

                command.Parameters.Add("@TxType", SqlDbType.Char).Value = (char) paymentLog.TxType;

                command.Parameters.Add("@TxDate", SqlDbType.DateTime).Value = paymentLog.TxDate;

                if (paymentLog.ResponseMessage != null)
                {
                    command.Parameters.Add("@ResponseMessage", SqlDbType.NVarChar).Value = paymentLog.ResponseMessage;
                }
                else
                {
                    command.Parameters.Add("@ResponseMessage", SqlDbType.NVarChar).Value = DBNull.Value;
                }

                if (paymentLog.ResponseCode != null)
                {
                    command.Parameters.Add("@ResponseCode", SqlDbType.Int).Value = paymentLog.ResponseCode;
                }
                else
                {
                    command.Parameters.Add("@ResponseCode", SqlDbType.Int).Value = DBNull.Value;
                }

                command.Parameters.Add("@Approved", SqlDbType.Bit).Value = paymentLog.Approved;

                if (paymentLog.Comment != null)
                {
                    command.Parameters.Add("@Comment", SqlDbType.NVarChar).Value = paymentLog.Comment;
                }
                else
                {
                    command.Parameters.Add("@Comment", SqlDbType.NVarChar).Value = DBNull.Value;
                }

                if (paymentLog.Username != null)
                {
                    command.Parameters.Add("@Username", SqlDbType.VarChar).Value = paymentLog.Username;
                }
                else
                {
                    command.Parameters.Add("@Username", SqlDbType.VarChar).Value = DBNull.Value;
                }

                command.Parameters.Add("@Voided", SqlDbType.Bit).Value = paymentLog.Voided;

                command.Parameters.Add("@Captured", SqlDbType.Bit).Value = paymentLog.Captured;

                if (paymentLog.CountryCode != null)
                {
                    command.Parameters.Add("@CountryCode", SqlDbType.Char).Value = paymentLog.CountryCode;
                }
                else
                {
                    command.Parameters.Add("@CountryCode", SqlDbType.Char).Value = DBNull.Value;
                }

                if (paymentLog.BankName != null)
                {
                    command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = paymentLog.BankName;
                }
                else
                {
                    command.Parameters.Add("@BankName", SqlDbType.VarChar).Value = DBNull.Value;
                }

                if (paymentLog.AdditionalInfo != null)
                {
                    command.Parameters.Add("@AdditionalInfo", SqlDbType.NVarChar).Value = paymentLog.AdditionalInfo;
                }
                else
                {
                    command.Parameters.Add("@AdditionalInfo", SqlDbType.NVarChar).Value = DBNull.Value;
                }

                if (paymentLog.PaymentEngine != null)
                {
                    command.Parameters.Add("@PaymentEngine", SqlDbType.TinyInt).Value = paymentLog.PaymentEngine;
                }
                else
                {
                    command.Parameters.Add("@PaymentEngine", SqlDbType.TinyInt).Value = DBNull.Value;
                }

                if (paymentLog.ReconciliationId != null)
                {
                    command.Parameters.Add("@ReconciliationID", SqlDbType.VarChar).Value = paymentLog.ReconciliationId;
                }
                else
                {
                    command.Parameters.Add("@ReconciliationID", SqlDbType.VarChar).Value = DBNull.Value;
                }

                if (paymentLog.DetailCardIndicator != null)
                {
                    command.Parameters.Add("@DetailCardIndicator", SqlDbType.Char).Value =
                        paymentLog.DetailCardIndicator;
                }
                else
                {
                    command.Parameters.Add("@DetailCardIndicator", SqlDbType.Char).Value = DBNull.Value;
                }

                if (paymentLog.ProductId != null)
                {
                    command.Parameters.Add("@ProductID", SqlDbType.VarChar).Value = paymentLog.ProductId;
                }
                else
                {
                    command.Parameters.Add("@ProductID", SqlDbType.VarChar).Value = DBNull.Value;
                }

                if (paymentLog.CardClass != null)
                {
                    command.Parameters.Add("@CardClass", SqlDbType.Char).Value = paymentLog.CardClass;
                }
                else
                {
                    command.Parameters.Add("@CardClass", SqlDbType.Char).Value = DBNull.Value;
                }

                if (paymentLog.ProductSubType != null)
                {
                    command.Parameters.Add("@ProductSubType", SqlDbType.VarChar).Value = paymentLog.ProductSubType;
                }
                else
                {
                    command.Parameters.Add("@ProductSubType", SqlDbType.VarChar).Value = DBNull.Value;
                }

                command.Parameters.Add("@CVCMatch", SqlDbType.Char).Value = paymentLog.CvcMatch;
                command.Parameters.Add("@AddressMatch", SqlDbType.Char).Value = paymentLog.AddressMatch;
                command.Parameters.Add("@ZipCodeMatch", SqlDbType.Char).Value = paymentLog.ZipCodeMatch;

                if (!string.IsNullOrEmpty(paymentLog.TokenId))
                {
                    command.Parameters.Add("@TokenID", SqlDbType.VarChar).Value = paymentLog.TokenId;
                }
                else
                {
                    command.Parameters.Add("@TokenID", SqlDbType.VarChar).Value = DBNull.Value;
                }

                if (paymentLog.TokenEngine != null)
                {
                    command.Parameters.Add("@TokenEngine", SqlDbType.TinyInt).Value = paymentLog.TokenEngine;
                }
                else
                {
                    command.Parameters.Add("@TokenEngine", SqlDbType.TinyInt).Value = DBNull.Value;
                }

                try
                {
                    await _sqlMetricsCollector.ObserveExecuteAsync(_database, "payment",
                        "spx_Payment_AddTransactionLogSOA",
                        async () => { await _sqlExecutor.ExecuteNonQueryAsync(command); }
                    );
                }
                catch (Exception exception)
                {
                    _logger.LogError(
                        $"[PaymentLegacyRepository][InsertPaymentLog] Unable to execute the stored procedure {RepositoryConstants.StoredProcedure.AddTransactionLog} with Command: {JsonConvert.SerializeObject(command)}. Exception: {exception}");
                    throw;
                }
            }
        }

        public async Task InsertOrderPayment(decimal amount, string ordernumber, int orderExt, bool refund = false,
            bool voided = false)
        {
            var orderPayment = new
            {
                OrderNumber = ordernumber,
                OrderExt = (byte) orderExt,
                ReferenceID = ordernumber,
                //negative if refund
                Amount = refund ? -amount : amount,
                AuthCode = string.Empty,
                PaymentDate = DateTime.Now,
                //legacy payment enums for reufnd/charge
                PaymentType = refund ? 6 : 3,
                Voided = voided
            };

            _logger.LogInformation(
                $"[PaymentLegacyRepository][InsertOrderPayment] Inserting order payment {JsonConvert.SerializeObject(orderPayment)}");

            using (var connection = _connectionFactory.GetPaymentConnection())
            {
                await _sqlMetricsCollector.ObserveExecuteAsync(_database, "payment", "spx_Payment_AddOrderPayment2",
                    async () =>
                    {
                        await _sqlExecutor.ExecuteAsync(
                            connection,
                            RepositoryConstants.StoredProcedure.AddOrderPayment,
                            orderPayment,
                            commandType: CommandType.StoredProcedure
                        );
                    }
                );
            }
        }

        public async Task<IList<RawPaymentLog>> GetTransactionLogs(int? orderNumber, string sortExperssion)
        {
            var result = new List<RawPaymentLog>();
            sortExperssion = string.IsNullOrWhiteSpace(sortExperssion) ? null : sortExperssion;
            using (var dbConnection = _connectionFactory.GetPaymentConnection())
            {
                await _sqlMetricsCollector.ObserveExecuteAsync(_database, "payment",
                    "spx_Payment_GetTransactionLogsSOA",
                    async () =>
                    {
                        result = (await _sqlExecutor.QueryAsync<RawPaymentLog>(
                            dbConnection,
                            "[Payment].[spx_Payment_GetTransactionLogsSOA]",
                            new
                            {
                                orderNumber,
                                sortExperssion
                            },
                            commandType: CommandType.StoredProcedure)).ToList();
                    });

                return result;
            }
        }

        public async Task<IList<RawPaymentLog>> GetTransactionLog(int txID)
        {
            var result = new List<RawPaymentLog>();

            using (var dbConnection = _connectionFactory.GetPaymentConnection())
            {
                await _sqlMetricsCollector.ObserveExecuteAsync(_database, "payment",
                    "spx_Payment_GetTransactionLogSOA",
                    async () =>
                    {
                        result = (await _sqlExecutor.QueryAsync<RawPaymentLog>(
                            dbConnection,
                            "[Payment].[spx_Payment_GetTransactionLogSOA]",
                            new
                            {
                                txID
                            },
                            commandType: CommandType.StoredProcedure)).ToList();
                    });

                return result;
            }
        }

        public async Task<IList<RawPaymentLog>> GetTransactionsByUser(Guid userId, PaymentType? paymentType = null,
            string paymentSubType = null,
            DateTime? beginTxDate = null, DateTime? endTxDate = null)
        {
            var result = new List<RawPaymentLog>();

            using (var dbConnection = _connectionFactory.GetPaymentConnection())
            {
                await _sqlMetricsCollector.ObserveExecuteAsync(_database, "payment",
                    "spx_get_transactions_by_user",
                    async () =>
                    {
                        result = (await _sqlExecutor.QueryAsync<RawPaymentLog>(
                            dbConnection,
                            "[Payment].[spx_get_transactions_by_user]",
                            new
                            {
                                userId,
                                paymentType,
                                paymentSubType,
                                beginTxDate,
                                endTxDate
                            },
                            commandType: CommandType.StoredProcedure)).ToList();
                    });

                return result;
            }
        }

        public async Task UpsertPaymentProperties(PaymentProperties authorizationProperties)
        {
            if (authorizationProperties is null)
                throw new ArgumentNullException(nameof(authorizationProperties));

            _logger.LogInformation(
                $"[PaymentLegacyRepository][InsertOrderPayment] Upserting authorization properties {JsonConvert.SerializeObject(authorizationProperties)}");

            using (var connection = _connectionFactory.GetPaymentConnection())
            {
                await _sqlMetricsCollector.ObserveExecuteAsync(_database, "payment",
                    "spx_Payment_UpsertPaymentProperties",
                    async () =>
                    {
                        await _sqlExecutor.ExecuteAsync(
                            connection,
                            RepositoryConstants.StoredProcedure.UpsertPaymentProperties,
                            new
                            {
                                authorizationProperties.TransactionId,
                                authorizationProperties.CustomerId,
                                authorizationProperties.OrderNumber,
                                authorizationProperties.TransactionDate,
                                authorizationProperties.TransactionStatus,
                                authorizationProperties.BillingAddress,
                                authorizationProperties.LastUpdatedBy
                            },
                            commandType: CommandType.StoredProcedure
                        );
                    }
                );
            }
        }

        public async Task<IList<PaymentProperties>> GetBillingAddressByOrderNumber(int orderNumber)
        {
            var result = new List<PaymentProperties>();

            using (var dbConnection = _connectionFactory.GetPaymentConnection())
            {
                await _sqlMetricsCollector.ObserveExecuteAsync(_database, "payment",
                    "spx_Payment_GetOrderBillingAddress",
                    async () =>
                    {
                        result = (await _sqlExecutor.QueryAsync<PaymentProperties>(
                            dbConnection,
                            "[Payment].[spx_Payment_GetOrderBillingAddress]",
                            new
                            {
                                orderNumber
                            },
                            commandType: CommandType.StoredProcedure)).ToList();
                    });

                return result;
            }
        }

        public async Task<decimal> GetOrderTotalPaid(int orderNumber, byte? orderExt)
        {
            var result = 0M;
            using (var dbConnection = _connectionFactory.GetPaymentConnection())
            {
                await _sqlMetricsCollector.ObserveExecuteAsync(_database, "payment",
                    "spx_Payment_GetOrderTotalPaid",
                    async () =>
                    {
                        result = (await _sqlExecutor.QueryAsync<decimal>(
                            dbConnection,
                            "[payment].[spx_Payment_GetOrderTotalPaid]",
                            new
                            {
                                orderNumber,
                                orderExt
                            },
                            commandType: CommandType.StoredProcedure)).FirstOrDefault();
                    });

                return result;
            }
        }
    }
}